import libnn as nn

from matplotlib import patches
import matplotlib as mpl

from libnn import colors2 as colors
import numpy as np

from libnn import closest_index

#from  nn_model import load_data


if __name__ == "__main__":
    import matplotlib.pylab as plt


    
    filename = 'data/nn_training_sorted_stft.pickle'
    #filename = 'data/smallsubset1.pickle'
    filename = 'data/smallsubset1_nondict.pickle'

   

    dataset = nn.DataSet(filename=filename)
    dataset.sort(np.argsort(dataset.data.ff),source=True)
    dataset.reset_filter()

    if 0:
        antenna = 4
        #dataset.reset_filter()
        datafilt = dataset.data.antenna==antenna & dataset.data.flattop & ~dataset.data.lowercutoff
        #dataset.reset_filter()
        datafiltlc = dataset.data.antenna==antenna & dataset.data.flattop & dataset.data.lowercutoff
        #dataset.reset_filter()
    if 1:
        #dataset.reset_filter()
        datafilt =  dataset.data.flattop & ~dataset.data.lowercutoff
        #dataset.reset_filter()
        datafiltlc = dataset.data.flattop & dataset.data.lowercutoff
        #dataset.reset_filter()
    

    ffvals = np.array([  45.5, 47, 51.6, 53.1, 54.5])*1e9
    indexes = []
    indexeslc = []
    for ff in ffvals:
        print 'test for normal',ff
        dataset.reset_filter()
        dataset.filter(datafilt)
        idx = nn.closest_index(dataset.data.ff, ff)
        indexes.append(idx)
        print 'test for lc',ff
        dataset.reset_filter()
        dataset.filter(datafiltlc)
        idx = nn.closest_index(dataset.data.ff, ff)
        indexeslc.append(idx)





    figdpi = 300
    fi = 0
    filenamepng = 'images/sofe2017_roi_details_%d.png'
    filenamepdf = 'images/sofe2017_roi_details_%d.pdf'

    print indexes,indexeslc




    corroi = colors[2]
    corff= colors[1]

    corfce = 'k'
    corsep = colors[7]
    alphaval = 0.8
    lw = 1

    rows = 1
    cols = 1
    f,axarr = plt.subplots(rows,cols, figsize=(5,3))
    #sharex=True,sharey=True)
 
    f.add_subplot(111, frameon=False)
# hide tick and tick label of the big axes
    plt.tick_params(labelcolor='none', top='off', bottom='off', left='off', right='off')
    plt.ylabel("Beat frequency [MHz]")
    plt.xlabel("Probing frequency [GHz]")

    #plt.xlabel('Probing frequency [GHz]')
    plt.subplots_adjust(left=None, bottom=0.16, right=None, top=None, wspace=0.05, hspace=0)

    #plt.suptitle('X-mode response')

    parseddata = dataset.parse(kind='2d',shifted=False)
    fblims = np.array(nn.vecx_fblims)/1e6
    i = 13

    stft_pf = dataset.data.stft_pf[i]/1e9
    stft_fb = dataset.data.stft_fb[i]/1e6
    stft = dataset.data.stft[i]
    
    pk_pf = dataset.data.pk_pf[i]/1e9
    pk_fb = dataset.data.pk_fb[i]/1e6
    
    ff = dataset.data.ff[i]/1e9
    fce_antenna = dataset.data.fce_antenna[i]/1e9
    fce_sep = dataset.data.fce_sep[i]/1e9
    ff_algorithm = dataset.data.ff_algorithm[i]/1e9

    stftroi =  parseddata.x[i]
    stftroi = stftroi.reshape(stftroi.shape[0:2])

    stftroi_pf = stft_pf
    minidx = nn.closest_index(stft_fb,fblims[0])
    maxidx = nn.closest_index(stft_fb,fblims[1])
    stftroi_fb = stft_fb[minidx:maxidx]



    norm = mpl.colors.Normalize(vmin=0.,vmax=stft.max())
    
    ax = axarr
    
    yasp = 40 #fblims[1]-fblims[0]
    xasp = stft_pf[-1]-stft_pf[0]
    aspratio = 5
    #ax.set_aspect(yasp/xasp/aspratio)

    ax.set_ylim([-5,35])
    hplot = ax.pcolormesh(stft_pf,stft_fb,stft,cmap='Blues',rasterized=True,zorder=1,norm=norm)

    fi += 1;f.savefig(filenamepng%(fi),dpi=figdpi);f.savefig(filenamepdf%(fi),dpi=figdpi);print filenamepng%(fi)


    hlist = []
    h = ax.axvline(x=fce_antenna,linestyle='--',c=corfce,alpha=alphaval,lw=lw,label='Wall')
    hlist.append(h)
    fi += 1;f.savefig(filenamepng%(fi),dpi=figdpi);f.savefig(filenamepdf%(fi),dpi=figdpi);print filenamepng%(fi)

    h = ax.axvline(x=fce_sep,linestyle='--',c=corsep,alpha=alphaval,lw=lw,label='Separatrix')
    hlist.append(h)
    fi += 1;f.savefig(filenamepng%(fi),dpi=figdpi);f.savefig(filenamepdf%(fi),dpi=figdpi);print filenamepng%(fi)

    hff = ax.axvline(x=ff,linestyle='--',c=corff,alpha=alphaval,lw=lw,label='First fringe')
    #hlist.append(h)
    fi += 1;f.savefig(filenamepng%(fi),dpi=figdpi);f.savefig(filenamepdf%(fi),dpi=figdpi);print filenamepng%(fi)

    ax.axhline(y=fblims[0],linestyle='--',c=corroi,alpha=alphaval,lw=lw,)
    ax.axhline(y=fblims[1],linestyle='--',c=corroi,alpha=alphaval,lw=lw,label=r'$f_b$ limits')
    fi += 1;f.savefig(filenamepng%(fi),dpi=figdpi);f.savefig(filenamepdf%(fi),dpi=figdpi);print filenamepng%(fi)

    hplot.remove()
    hplot2 = ax.pcolormesh(stft_pf,stft_fb[minidx:maxidx],stft[minidx:maxidx,:],cmap='Blues',rasterized=True,zorder=1,norm=norm)

    fi += 1;f.savefig(filenamepng%(fi),dpi=figdpi);f.savefig(filenamepdf%(fi),dpi=figdpi);print filenamepng%(fi)

    #yasp = fblims[1]-fblims[0]
    #ax.set_aspect(yasp/xasp/aspratio)

    #ax.set_ylim(fblims)
    
    l = ax.legend(fontsize='small')
    fi += 1;f.savefig(filenamepng%(fi),dpi=figdpi);f.savefig(filenamepdf%(fi),dpi=figdpi);print filenamepng%(fi)
    #plt.show()
    l.remove()    
    x1 = fce_antenna
    y1 = fblims[0]

    wx = fce_sep-fce_antenna
    wy = fblims[1]-fblims[0]
    
    


    print x1,y1,wx,wy
    rect1 = patches.Rectangle((x1,y1),wx,wy, facecolor='none', 
                              edgecolor=corroi, 
                              alpha=alphaval,zorder=100)
    h = ax.add_patch(rect1)
    hlist.append(h)
    fi += 1;f.savefig(filenamepng%(fi),dpi=figdpi);f.savefig(filenamepdf%(fi),dpi=figdpi);print filenamepng%(fi)
    hplot2.remove()
    

    # plot parsed data
    ax.pcolormesh(stftroi_pf,stftroi_fb,stftroi,cmap='Blues',rasterized=True,norm=norm)

    fi += 1;f.savefig(filenamepng%(fi),dpi=figdpi);f.savefig(filenamepdf%(fi),dpi=figdpi);print filenamepng%(fi)

    
    for h in hlist:
        h.remove()
    fi += 1;f.savefig(filenamepng%(fi),dpi=figdpi);f.savefig(filenamepdf%(fi),dpi=figdpi);print filenamepng%(fi)
    hff.remove()
    fi += 1;f.savefig(filenamepng%(fi),dpi=figdpi);f.savefig(filenamepdf%(fi),dpi=figdpi);print filenamepng%(fi)
   
    #fi += 1;f.savefig(filenamepng%(fi),dpi=figdpi);f.savefig(filenamepdf%(fi),dpi=figdpi);print filenamepng%(fi)


    f,axarr = plt.subplots(1,1, figsize=(5,3))

    ax=axarr
    #ax.pcolormesh(stftroi,cmap='Blues',rasterized=True,norm=norm)


    x1 = closest_index(stftroi_pf,fce_antenna)
    y1 = 0

    wx = closest_index(stftroi_pf,fce_sep)-x1
    wy = 127
    xff = closest_index(stftroi_pf,ff)

    print x1,y1,wx,wy
    rect1 = patches.Rectangle((x1,y1),wx,wy, facecolor='none', 
                              edgecolor=corroi, 
                              alpha=alphaval,zorder=100)

    #h = ax.add_patch(rect1)
    
    hff = ax.axvline(x=xff,linestyle='--',c=corff,alpha=alphaval,lw=lw,label='First fringe')

    ax.imshow(stftroi,cmap='Blues',norm=norm,origin='lower')


    ax.set_xlabel('x')
    ax.set_ylabel('y')

    #box = ax.get_position()
    #ax.set_position([box.x0*1.05, box.y0, box.width, box.height])

    #divider = make_axes_locatable(ax)
    #cax = divider.append_axes('top', size='5%', pad=0.05,frameon=False)
    #cax.
    #hff = cax.axvline(x=xff,linestyle='--',c=corff,alpha=alphaval,lw=lw,label='First fringe')



    #l = ax.legend(fontsize='small')
    fi += 1;f.savefig(filenamepng%(fi),dpi=figdpi);f.savefig(filenamepdf%(fi),dpi=figdpi);print filenamepng%(fi)
    #plt.show()
    #l.remove()

    


    plt.show()
