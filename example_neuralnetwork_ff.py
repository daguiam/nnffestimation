import libnn as nn
import keras
#from keras.models import load_model


import sys
sys.path.append('/draco/u/daguiam/codes/ricglibrary/')

import ricg





#from ricg import get_diagSignal



if __name__ == "__main__":
    
   


    print "opening ricg"

    shotnumber = 34290
    shotnumber = 33490
    antenna = 1
    time  = 1.6
    persistence = 1
    shot = ricg.RICG(shotnumber, antenna,time)
    shot.params['stft.pfspan'] = 2.5e9
    shot.params['stft.padding'] = 1024
    shot.gotoTime(time)
    freqramp, signal, stft_pf, stft_fb, stft = shot.processRaw()

    print shot.params['stft.pfspan'],shot.params['stft.window']
    r,z,bfield, fce_antenna, fce_sep = shot.getBfieldProfile(shot.time, shot.los.r, shot.los.z)

    print fce_antenna/1e9, fce_sep/1e9
    print "loaded signal data"

    model_file = 'models/nn_model_shifted_all.h5'

    print "loading model",model_file
    #model = load_model(model_file)
    NN = nn.ModelFFNN()
    NN.open(model_file)
    #NN.fblims = (0, 20e6)
    print "fblims", NN.fblims

    import matplotlib.pyplot as plt

    print "stftshape", stft.shape
    #plt.imshow(stft)
    #plt.show()

    #vec_x = parse_data_vec_x_2d(stft_pf, stft_fb, stft, fblims=NN.fblims, maxbw=64e9)
   # print "vecx shape", vec_x.shape
    #ff = NN.model.predict(vec_x)
    
    
    
    print "calculating ff"
    ff = NN.calc_ff(stft_pf,stft_fb, stft, fce_antenna, fce_sep)
    ff = ff[0]
    print shot.time, ff
    
pfscale = 1e-9
fbscale = 1e-6

plt.pcolormesh(stft_pf*pfscale, stft_fb*fbscale, stft, rasterized=True)
plt.axvline(fce_antenna*pfscale, label='wall', color='C2')
plt.axvline(fce_sep*pfscale, label='sep', color='C4')
plt.axvline(ff*pfscale, label='ff', color='m')
plt.ylim(-5,30)

plt.xlabel('Probing frequency [GHz]')
plt.ylabel('Beat frequency [MHz]')
plt.title(shot.time)
plt.legend()

plt.show()




