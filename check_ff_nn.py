"""
Verifies and tries the ModelFFNN form libnn

"""

import numpy as np


import ricg
import libnn

if __name__ == "__main__":
    
    import argparse
    
    import matplotlib.pylab as plt
    import matplotlib.colors as colors


    parser = argparse.ArgumentParser(description='Loads data, creates nn model, trains the data')
    parser.add_argument('-s','--shotnumber',help='Shotnumber',type=int,default=33520)

    
    args = parser.parse_args()
    shotnumber = args.shotnumber
   


    modelfilename = 'models/nn_model_shifted_all.h5'

    #print 'Initializing model'
    #Model = libnn.ModelFFNN(modelfilename=modelfilename)


    #Model.fblims = (0,10e6)

    #print 'fblims are',Model.fblims
    antenna = 1
    time = 2.4
    persistence = 4
    print 'Getting RIF data'

    shot = ricg.RIF(shotnumber=shotnumber,
                    antenna=antenna,
                    time=time,
                    persistence=persistence,
                    verbose=True)
    shot.gotoTime(time)
    
    print shot.params.stft_window
    print shot.params.stft_padding
    print shot.params.stft_step
    
    shot.params.stft_window = 321
    shot.params.stft_padding = 2048
    shot.params.stft_padding = 1024

    shot.params.stft_step = 10
    print shot.params.stft_window
    print shot.params.stft_padding
    print shot.params.stft_step
 
    for i in range(persistence):
        print i, shot.sweepnr, shot.raw.time, shot.time
        
        
        shot.nextSweep()
        shot.processProfile()
        

    stft_pf = shot.stft_pf
    stft_fb = shot.stft_fb
    stft = shot.stft
    fce_antenna = shot.fce_antenna
    fce_sep = shot.fce_sep

    ff = shot.ModelFF.calc_ff(stft_pf, stft_fb, stft,
                       fce_antenna, fce_sep)

    print 'Calculated nn FF is %0.2f GHz and shot is %0.2f GHZ'%(ff/1e9,shot.ff/1e9)
    
