import libnn

import matplotlib.pylab as plt

from keras.models import load_model
import numpy as np
import pickle
if __name__ == "__main__":


    epochs =  range(50)

    print 'Loading datasets'

    if 0:
        if 0:

            filename = 'data/nn_trainning_sorted_stft_split0.9.pickle'
            filenametest = 'data/nn_trainning_sorted_stft_split0.1.pickle'
            #filename = 'data/nn_trainning_sorted_stft_split0.1.pickle'

            shifted=True
            #shifted=False
            kind ='2d'
            #if 1:
            print 'Loading trained set',filename
            dataset = libnn.DataSet(filename=filename)
            print 'Loading test set',filenametest
            datatest = libnn.DataSet(filename=filenametest)

            print 'Parsing trained set'
            train = dataset.parse(kind=kind,shifted=shifted)
            print 'Total trained:',len(train.ff)
            print 'Parsing trained set'
            test = datatest.parse(kind=kind,shifted=shifted)
            print 'Total test:',len(test.ff)
        else:
            print 'loading test_y'
            test_y = np.load('data/parsed_test_y_split0.1.npy')
            print 'loading test_x'
            test_x = np.load('data/parsed_test_x_split0.1.npy')
            test = libnn.DataXY(x=test_x,y=test_y)

            print 'loading train_y'
            train_y = np.load('data/parsed_train_y_split0.9.npy')
            print 'loading train_x'
            train_x = np.load('data/parsed_train_x_split0.9.npy')
            train = libnn.DataXY(x=train_x,y=train_y)


        print 'Splitting data train',len(train.ff)
        a,train = libnn.split_dataxy(train,split=0.01)
        print 'splitted data train',len(train.ff)
        print 'Splitting data test',len(test.ff)
        a,test = libnn.split_dataxy(test,split=0.1)
        print 'splitted data test',len(test.ff)


        #if 0:
        train_acc = []
        test_acc = []

        true_train = []
        true_test = []
        pred_train = []
        pred_test = []
        for epoch in epochs:

            modelfilename = 'models/nn_model_epochs_%02d.hdf5'%(epoch)
            print 'Loading epoch %d'%(epoch), modelfilename
            model =load_model(modelfilename)


            print 'Predicting trained set'
            trainpred = libnn.DataXY(x=[],y=model.predict(train.x))
            print 'Predicting test set'
            testpred = libnn.DataXY(x=[],y=model.predict(test.x))
        #
            truetrainff = train.ff
            truetestff = test.ff
            trainpredff = trainpred.ff
            testpredff = testpred.ff

            true_train.append(truetrainff)
            true_test.append(truetestff)
            pred_train.append(trainpredff)
            pred_test.append(testpredff)

        #
            trainaccuracy = 1- np.abs(truetrainff-trainpredff)/np.mean([truetrainff,trainpredff],axis=0)
            testaccuracy = 1- np.abs(truetestff-testpredff)/np.mean([truetestff,testpredff],axis=0)

            print 'Train mean', trainaccuracy.mean(), 'test mean', testaccuracy.mean()        

            train_error = np.abs(truetrainff-trainpredff)
            test_error = np.abs(truetestff-testpredff)

            print 'Train error',np.mean(train_error)/1e9,'Test error',np.mean(test_error)/1e9
            print 'Train std error',np.std(train_error)/1e9,'Test std error',np.std(test_error)/1e9

            train_acc.append(trainaccuracy)
            test_acc.append(testaccuracy)

        true_train = np.array(true_train)
        true_test = np.array(true_test)
        pred_train = np.array(pred_train)
        pred_test = np.array(pred_test)
        data = {'true_train':true_train,'true_test':true_test, 'pred_train':pred_train,'pred_test':pred_test}
        pickle.dump(data,open('data/accuracy_model.pickle'))


    else:
        data = pickle.load(open('data/accuracy_model.pickle','rb'))
        true_train = data['true_train']
        true_test = data['true_test']
        pred_train = data['pred_train']
        pred_test = data['pred_test']

    train_error = true_train-pred_train
    test_error = true_test-pred_test

    train_error = np.abs(train_error)/1e9
    test_error = np.abs(test_error)/1e9


    
    figdpi = 300
    fi = 0
    filenamepng = 'images/sofe2017_model_accuracy_%d.png'
    filenamepdf = 'images/sofe2017_model_accuracy_%d.pdf'

    #axarr = plt.subplot(111,projection='polar')
    f,axarr = plt.subplots(2,1,sharex=True,figsize=(4,3))
    f.add_subplot(111, frameon=False)
    plt.tick_params(labelcolor='none', top='off', bottom='off', left='off', right='off')
    #plt.ylabel('FF Accuracy')
    plt.xlabel('Trained epochs')

    plt.title('FF estimation error')

    plt.subplots_adjust(left=0.15, bottom=0.15, right=None, top=None, wspace=0.05, hspace=0.05)


    ax = axarr[0]

    ax.set_ylabel('Mean [GHz]')
    ax.plot(epochs,np.mean(train_error,axis=1),label='Train set')
    ax.plot(epochs,np.mean(test_error,axis=1),label='Validation set')

    #ax.legend(fontsize='small')
    ax = axarr[1]
    ax.set_ylabel('Deviation [GHz]')
    ax.plot(epochs,np.std(train_error,axis=1),label='Train set')
    ax.plot(epochs,np.std(test_error,axis=1),label='Validation set')

    ax.legend(fontsize='small')
    fi += 1;f.savefig(filenamepng%(fi),dpi=figdpi);f.savefig(filenamepdf%(fi),dpi=figdpi);print filenamepng%(fi)

    plt.show()


    
