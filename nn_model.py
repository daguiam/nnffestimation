"""
Creates and tests the model for neural network FF estimation



"""


import keras
# Create first network with Keras
from keras.models import Sequential
from keras.layers import Dense

from keras.layers import Dropout
from keras.layers import Flatten
from keras.layers.convolutional import Conv2D
from keras.layers.convolutional import MaxPooling2D
from keras.models import load_model
import numpy as np
from scipy.stats import norm
import matplotlib.pylab as plt
import pickle


import libdata




def load_data_old():

    trainfile = 'data/traindata.dat.npy'
    dataset = np.load(trainfile)

    trainfile = 'data/traindatapf.dat.npy'
    datapf = np.load(trainfile)

    trainfile = 'data/traindataff.dat.npy'
    dataffidx = np.load(trainfile)

    pf = datapf

    # Real test value of ff
    testff = pf[dataffidx]
    modelfile='modelFFeval_epochs%d.h5'%(epochs)


    ffspan = max(pf)-min(pf)
    singletestff = (testff-min(pf))/ffspan
    singletestff = (testff-40e9)/28e9

    X = dataset.reshape(len(dataset[:,0,0]),-1)
    data_x = X
    data_y = singletestff


    return data_x,data_y


def load_data(filename='data/nn_training_sorted_stft.pickle'):

    print 'loading',filename
    data = pickle.load(open(filename,'rb'))
    
    return data

def obsolete_data(data):

    print 'parsing data ',len(data.shotnumber)

    data_x,data_y = libdata.parse_data_nn(data,kind='1d')
    
    return data_x,data_y


def split_train_test_data(data_x, data_y, split=0.1):
    """ Splits the given data_x into train and test data for SPLIT
    """



    totalpoints = len(data_x)

    splitpoints = int((1-split)*totalpoints)
    testsplit = int((split)*totalpoints)

    indexes = np.arange(totalpoints)
    randindexes = np.random.randint(0,totalpoints,testsplit)
    splitmask = np.in1d(indexes, randindexes)
    #print splitmask
    nonsplitmask = np.logical_not(splitmask)
    #print nonsplitmask
    #print indexes

    #print 'Split at ',splitpoints


    #return data_x[:splitpoints],data_y[:splitpoints], data_x[splitpoints:], data_y[splitpoints:]
    if data_x.ndim >1:
        return data_x[nonsplitmask,:],data_y[nonsplitmask], data_x[splitmask,:], data_y[splitmask]
    else:
        return data_x[nonsplitmask],data_y[nonsplitmask], data_x[splitmask], data_y[splitmask]







def create_model(sizeH1,sizeH2,sizeH3):



    model = Sequential()
    model.add(Dense(sizeH1, input_dim=sizeH1, init='uniform', activation='relu'))
    model.add(Dense(sizeH2, init='uniform', activation='relu'))
    #model.add(Dense(sizeH2, init='uniform', activation='relu'))
    model.add(Dense(sizeH3, init='uniform', activation='sigmoid'))

    model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy','mean_squared_error'])


    return model


def create_model_2d(input_shape=(102,268,1),filter_shape=(5,5),filters=20,densesize=512,dropout=0.2):
    model = Sequential()
    model.add(Conv2D(filters, 
                     filter_shape, 
                     input_shape=input_shape,
                     activation='relu',
                     strides=3,
                     data_format='channels_last'))
    model.add(MaxPooling2D(pool_size=(2,2)))
    model.add(Dropout(dropout))
    model.add(Conv2D(filters, 
                     filter_shape, 
                     input_shape=input_shape,
                     activation='relu',
                     strides=3,
                     data_format='channels_last'))
    model.add(MaxPooling2D(pool_size=(2,2)))
    model.add(Dropout(dropout))
    model.add(Flatten())
    model.add(Dense(densesize, activation='relu'))
    model.add(Dense(1,activation='sigmoid'))

    model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy','mean_squared_error'])

    return model
    
if __name__ == "__main__":

    import argparse
    

    parser = argparse.ArgumentParser(description='Loads data, creates nn model, trains and plots the data')
    parser.add_argument('-b','--batches',help='batches',type=int,default=128)
    parser.add_argument('-e','--epochs',help='epochs',type=int,default=20)
    parser.add_argument('-s','--split',help='epochs',type=float,default=0.1)
    
    parser.add_argument('-r','--rampup',action="store_true",default=False)
    parser.add_argument('-lc','--lowercutoff',action="store_true",default=False)
    parser.add_argument('-new','--trainnew',action="store_true",default=False)

    
    
    args = parser.parse_args()
    
    
    epochs = args.epochs
    batches = args.batches
    split = args.split

    rampup = args.rampup
    lowercutoff = args.lowercutoff

    trainnew = args.trainnew
    np.random.seed(2)


    modelfile = 'testmodel.h5'
    # fix random seed for reproducibility
    seed = 7
    np.random.seed(seed)
    # load pima indians datase


    #do_1d()



    data = load_data()
    
    
    if not rampup:
        print 'Removing ramp up data'
        data = libdata.select_data_filter(data,data.flattop)
    if not lowercutoff:
        print 'Removing lower cutoff data'
        data = libdata.select_data_filter(data,np.invert(data.lowercutoff))
    #data = libdata.select_data_filter(data,data.antenna==1)
    
    
    
    data_x,data_y = libdata.parse_data_nn(data,kind='1d')
   
    #data_x,data_y = load_data_old()
 
    #print 'datax',data_x
    #print 'datay',data_y

    #exit()
    train_x, train_y, test_x, test_y = split_train_test_data(data_x, data_y, split=split)

    sizeH1 = len(data_x[0])
    sizeH3 = len(data_y[0]) if data_y.ndim>1 else 1
    sizeH2 = 2048*4*2




    print 'sizeh1',sizeH1
    print 'sizeh2',sizeH2
    print 'sizeh3',sizeH3

    history = None
    if trainnew:
        pass
        model = create_model(sizeH1,sizeH2,sizeH3)



    #print train_x.shape
        #history = model.fit(train_x, train_y,  epochs=epochs,batch_size=batches,verbose=1,shuffle=True,validation_split=0.100)
        history = model.fit(train_x, train_y,   epochs=epochs,batch_size=batches,verbose=1,shuffle=True,validation_data=(test_x,test_y))


        model.save(modelfile)
        print history.history.keys()

        del model
    model = load_model(modelfile)

    


    #loss= model.evaluate(test_x, test_y, batch_size=32, verbose=1, sample_weight=None)
    #print loss

    pred_train = model.predict(train_x)
    if len(test_x) > 0:
        pred_test = model.predict(test_x)
    else:
        pred_test = test_x


    traindiff = libdata.unnormalise_ff_data(pred_train)-libdata.unnormalise_ff_data(train_y)
    #print traindiff

    etrainmean = np.mean(traindiff)
    etrainstd = np.std(traindiff)
    
    testdiff = libdata.unnormalise_ff_data(pred_test)-libdata.unnormalise_ff_data(test_y)
    #print testdiff

    etestmean = np.mean(testdiff)
    eteststd = np.std(testdiff)

    f,axarr = plt.subplots(3,1)

    ax = axarr[0]

    x = data_y
    y = data_y
    x = libdata.unnormalise_ff_data(x)/1e9
    y = libdata.unnormalise_ff_data(y)/1e9
    ax.plot(x,y,c='c',label='Correct')


    x = train_y
    y = pred_train
    x = libdata.unnormalise_ff_data(x)/1e9
    y = libdata.unnormalise_ff_data(y)/1e9


    emean = (etrainmean)/1e9
    estd = (etrainstd)/1e9

    ax.scatter(x,y,c='b',label='Train set [E:%0.3f S:%0.3f]'%(emean,estd))


    x = test_y
    y = pred_test
    x = libdata.unnormalise_ff_data(x)/1e9
    y = libdata.unnormalise_ff_data(y)/1e9
    
    emean = (etestmean)/1e9
    estd = (eteststd)/1e9
    
    ax.scatter(x,y,c='r',label='Pred set [E:%0.3f S:%0.3f]'%(emean,estd))
    #ax.scatter(data_y,data_y,c='c')

  
    
    ax.legend(loc='lower right',fontsize='x-small')


    #plt.plot(data_x[100])
    #plt.plot(data_y[100])
    #plt.plot(pred_y,'r')

    print len(train_y),len(test_y)

    if history is not None:
        ax = axarr[1]
        
        # summarize history for accuracy
    #plt.plot(history.history['acc'])
    #plt.plot(history.history['val_acc'])
        ax.plot(history.history['mean_squared_error'])
        ax.plot(history.history['val_mean_squared_error'])
        ax.set_title('model mean squared')
        ax.set_ylabel('accuracy')
        ax.set_xlabel('epoch')
        ax.legend(['train', 'test'], loc='upper right',fontsize='x-small')
    
        ax = axarr[2]
    # summarize history for loss
        ax.plot(history.history['loss'])
        ax.plot(history.history['val_loss'])
        ax.set_title('model loss')
        ax.set_ylabel('loss')
        ax.set_xlabel('epoch')
        ax.legend(['train', 'test'], loc='upper right',fontsize='x-small')





    #plt.figure()
    #plt.plot(testdiff)

    if 0:
        f,axarr = plt.subplots(2,1)
        
        ax = axarr[0]
        totalpower = data.nbi + data.ecrh+data.icrh

        print totalpower
        print len(totalpower),len(traindiff)
   


        ax.scatter(totalpower,traindiff/1e9)
    plt.show()
