import libnn as nn


import numpy as np

#from  nn_model import load_data


if __name__ == "__main__":
    import matplotlib.pylab as plt
#    data = load_data(filename='data/nn_trainning_sorted.pickle')
    data = libdata.load_obj_test_data(filename='data/nn_trainning_sorted.pickle')
    data.totalpower = data.nbi + data.icrh + data.ecrh
    data = libdata.select_data_filter(data,data.flattop)

    data1 = libdata.select_data_filter(data,data.antenna==1)
    data4 = libdata.select_data_filter(data,data.antenna==4)
    data8 = libdata.select_data_filter(data,data.antenna==8)

    #print len(data)

    
    filename = 'data/nn_training_sorted_stft.pickle'
    #filename = 'data/smallsubset1.pickle'
    #filename = 'data/smallsubset1_nondict.pickle'

   

    dataset = DataSet(filename=filename)
    
    dataset.filter(np.random.choice([0,1],size=dataset.size))
    print len(dataset.data.ff)



    axarr = plt.subplot(111,projection='polar')
    
    ax = axarr

    
    filterslist = [data.flattop,data.lowercutoff,data.elm,data]
    thetastep = 2*np.pi/len(filterslist)
    filternameslist = ['flattop','lowercutoff','elm']


    for antenna in [1,4,8]:
        thetavec =[]
        rvec = []
        dataantenna = libdata.select_data_filter(data, data.antenna==antenna)

        filterslist = [dataantenna.flattop,
                       dataantenna.lowercutoff,
                       dataantenna.elm,
                       dataantenna.icrh>0,
                       dataantenna.ecrh>0,
                       dataantenna.nbi>0,]
        thetastep = 2*np.pi/len(filterslist)
        filternameslist = ['Flattop','Lower Cutoff','ELM','ICRH','ECRH','NBI']
        
        for i,datafilter in enumerate(filterslist):

            datafilt = libdata.select_data_filter(dataantenna, datafilter)

            theta = i*thetastep
        #thetavec = np.ones(len(datafilt.ff))*theta
        
            thetavec.append(theta)
            r = len(datafilt.ff)
            rvec.append(r)

        thetavec.append(thetavec[0])
        rvec.append(rvec[0])
        ax.plot(thetavec,rvec,label='Antenna %d'%(antenna))

    ax.set_xticks(thetavec[:-1])
    ax.set_xticklabels(filternameslist)

    plt.legend(loc=1)

    #plt.figure()

    

    plt.show()
