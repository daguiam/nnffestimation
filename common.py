# Common python functions to be imported to other programs
# 
# 
# 

import numpy as np
from scipy import constants as konst
from scipy import signal
import math

from scipy import interpolate
#from peakdetect import peakdet
import matplotlib.pylab as plt


def argmax(pairs):
    return max(pairs, key=lambda x: x[1])[0]


def filtfilt_lowpass(sig,cutoff,order=5):
    nyq = 1;
    cutoffnorm = cutoff/nyq;
    b, a = signal.butter(order,cutoffnorm,btype='low');

    sig = signal.filtfilt(b,a,sig)

    return sig


def filtfilt_bandpass(sig,lowcut,highcut,Fs=1,order=5):
    """ Band pass butterworth filt filter
    """
    nyq = np.float(Fs)/2
    lowcut = lowcut/nyq
    highcut = highcut/nyq
    
    b, a = signal.butter(order,[lowcut,highcut],btype='band')

    sig = signal.filtfilt(b,a,sig)

    return sig


def filt_bandpass(sig,lowcut,highcut,Fs=1,order=5):
    """ Band pass butterworth filter
    """
    nyq = np.float(Fs)/2
    lowcut = lowcut/nyq
    highcut = highcut/nyq
    
    b, a = signal.butter(order,[lowcut,highcut],btype='band')

    sig = signal.lfilter(b,a,sig)

    return sig


def do_butter_lowpass_filter(x,cutoff,Fs=1,order=5):
    nyq = np.float(Fs)/2;
    cutoffnorm = cutoff/nyq;
    b, a = signal.butter(order,cutoffnorm,btype='low');
        #self.sig = np.fliplr(self.sig)
    out = signal.lfilter(b,a,x);
        #self.sig = np.fliplr(self.sig)
    return out



# Implemented smooth moving average algorithm
def smooth(data,N=5):
    return np.convolve(np.ones(N)/N,data,'same');


def smooth2d(data,N):
    for i in range(len(data[0,:])):
        data[:,i] = smooth(data[:,i],N)
    return data

# Finds the index of the array element nearest to the provided value
def get_arrayValueIdx(array,value):
    return (np.abs(array-value)).argmin();

# Translates a dictionary to an object
class dict2obj():
    def __init__(self,d):
        self.__dict__ = d

def sortMultipleArrays(array_list,sortidx):
    """
    Receives multiple array variables in a list with the same length and sorts each in place according to the sortidx
    """
    #print array_list.shape
    for array in array_list:
        array[:len(sortidx)] = array[sortidx]
        
        


def calc_fce(bfield):
    """ Calculates the electron cyclotron frequency from a given Magnetic Field
    """
    return np.abs(konst.e*bfield/(2*konst.pi*konst.m_e))

def calc_bfield(fce):
    """ Calculates the magnetic field from a given electron cyclotron frequency
    """
    return (fce*2*konst.pi*konst.m_e)/(konst.e)





        
# Caculates the stft amplitude along each bin
def calcSTFTAmplitude(pf,fb,stft):
    pflen = len(pf)
    zeroidx = get_arrayValueIdx(fb,0)
    maxstft = np.zeros(pflen);
    maxfb = np.zeros(pflen);
    sumstft = np.zeros(pflen);
    for i in range(0,pflen):
        idx = np.argmax(stft[zeroidx:,i]);
        maxstft[i] = stft[idx,i];
        maxstft[i] = np.linalg.norm(stft[zeroidx:,i])
        maxfb[i] =  fb[zeroidx+idx]
        sumstft[i] = np.sum(stft[:,i])
        
        #maxstft = sumstft
    stftamplitude = maxstft/max(maxstft);
    stftpeakfb = maxfb

    return stftamplitude,stftpeakfb

# Computes the spectrogram of a broadband reflectometry signal using a sliding window short-time fourier transform
# Adapted to python from ref_stft() by CFN-IST Portugal
def calc_stft(x,y,Fs=1,window=128,padding=4096,step=10,windowtype=1):
        

        Fs = Fs;
        freqramp = x
        signal = y;
        n = len(signal);
        nf = len(freqramp)
        N = padding;
                
        # Produces the wanted window
        def get_fft_window_type(window=128,windowtype=1):
    
            if (windowtype==1):
                WIN = np.ones(window);
            elif (windowtype==2):
                WIN = np.hamming(window);
            elif (windowtype==3):
                WIN = np.hanning(window);
            elif (windowtype==4):
                WIN = np.blackman(window);
        
            return WIN;
        
        WIN = get_fft_window_type(window,windowtype);

        if window < step:
            step = window

        if step==0:
            stft_x = int(math.floor(n/window));
            step=window;
        else:
            stft_x = int(math.floor( (n-window)/step) +1);
            
        #stft_x,step = calc_stftStep(window,step,n)

        stft_y = padding
        stft = np.zeros( ( stft_y,stft_x ));

        halfwin = window/2;
        pfindex = np.array([],dtype='int');
        fmax = Fs
        fb = np.linspace(0,fmax,stft_y);
        win_i = 0;
        # Calculates the fft in each window
        for i in range(0,stft_x):
            win_i =  i*step;
            win_f = win_i+window;
            #print win_i,win_f
            sigwindow =  signal[win_i:win_f];
            len_sig = len(sigwindow);
            if len_sig < window:
                auxwindow = get_fft_window_type(len_sig,windowtype);
                sigwindow = auxwindow * sigwindow;
            else:
                sigwindow = WIN * sigwindow;

            stft[:,i]=np.abs(np.fft.fft(sigwindow,padding))[0:stft_y];

            # Each probing frequency bin is at the middle of the FFT window
            pfindex = np.append(pfindex,win_i+halfwin);
        pf = freqramp[pfindex];

        # FFT shift
        for i in range(0,len(pf)):
            stft[:,i]=np.fft.fftshift(stft[:,i]);
        fb = fb-max(fb)/2;
        
        # Outputs
        stft = stft;
        pf = pf;
        fb = fb;

        return pf,fb,stft



# Uses the calculated stft and calculates its normalization along the bins
def calc_stftNorm(stft):
    bins = stft.shape[1];
    stftnorm = np.zeros(stft.shape);
        #self.stftorig = self.stft;
    for i in range(0,bins):
        maxbin = np.max(stft[:,i]);
        stftnorm[:,i] =stft[:,i]/maxbin;
    return stftnorm
    
    
    
    

# Calculates the FF from the Peak FB and the decision ratio with the amplitude
def calcFF_STFTPeakFB(pf,amplitude,peakfb,fce_antenna):
    """Calculates the FF from the Peak FB and the decision ratio with the amplitude
    """
    # Number of considerations for each iteration
    nrcon = 15
    stftpeakfbampthreshold = 0.18
    ff_maxdistfce = 5e9
    stftpeakthreshold = 0.03
    
    ff_previous = None
    
    fceidx = get_arrayValueIdx(pf,fce_antenna)+1
    
    lastf = fce_antenna+ff_maxdistfce
    if ff_previous is not None:
        lastf = ff_previous+1.5e9
    
    
    fceidx2 = get_arrayValueIdx(pf,lastf)+1

    amp = amplitude[fceidx:fceidx2]
    amp = amp/max(abs(amp))

    fbvalues = peakfb[fceidx:fceidx2]
    fbvalues = fbvalues/max(fbvalues)

    fbampratio = fbvalues/amp


    # Adds another decision factor with difference between consecutive fbvalues version 10.2016
    if 1:
        difffb = np.diff(fbvalues)
        difffb = difffb/np.max(np.abs(difffb))
        
        difffb = np.abs(np.append([0],difffb))
        difffb = 1.1-difffb
        fbampratio = fbampratio/(difffb*difffb)
        
    fbampratio = fbampratio/max(fbampratio)
        

    fbampratio = fbampratio
    fbpf = pf[fceidx:fceidx2]
    
    maxes,mines = peakdet(fbampratio,stftpeakthreshold);

    peaksidx = maxes[:,0].astype(int)
    #the amplitude at the peaks must be somewhat low
    idxabovethreshold = np.where(amp[peaksidx] <stftpeakfbampthreshold)
    bestffs = pf[fceidx+peaksidx]

    # Considers the amplitude to the sides of each peak as a decision factor
    # if most amplitude is to the right, then it contributes positively to the decision
    sidesampratio = []
    for idx in peaksidx:
        if idx == 0:
            sidesampratio.append(0)
            continue
        leftamp = np.mean(np.power(amp[:idx],2))
        rightamp = np.mean(np.power(amp[idx:],2))

        #rightamp = 1
        #leftamp = 1
        sidesampratio.append(rightamp/leftamp)
    sidesampratio = np.array(sidesampratio)
    decisionratio = sidesampratio*fbampratio[peaksidx]
    bestidx = peaksidx[np.argmax(decisionratio)]
    
    # If there was a previous first fringe calculated, then tries to find the nearest new ff
    ff_previous=None
    if ff_previous is not None:
        ffprevidx =  get_arrayValueIdx(pf[fceidx:fceidx2],ff_previous)
        # If a previous ff is available, considers the distance as a decision factor

        distances = np.abs(peaksidx - ffprevidx)
        #peakdistratio = fbampratio[peaksidx]/distances
        peakdistratio = decisionratio/distances
        closestidx = np.argmax(peakdistratio)
        newff = bestffs[closestidx]
    #   print 'New FF closest to previous is',newff,' ffprev=',self.ff_previous
    else:
        newff = pf[fceidx+bestidx]
    ff_stftpeakfb=newff


    return newff
        



def calc_ff(pf,amp,fb,fce):
    fceidx = get_arrayValueIdx(pf,fce)
    ratio = fb/amp
    ffidx = np.argmax(ratio[fceidx:])
    ff = pf[fceidx + ffidx]
  
    return ff

def calcInterpSTFTAmplitude(freqramp,amplitude,pf):
    ratio = int(len(freqramp)/len(pf))
    amplitude = smooth(amplitude,ratio)
    interp = interpolate.interp1d(freqramp,amplitude)
    newamp = interp(pf)
    return newamp



def freq_hilbert(sig,Fs):
    """ Calcultes the instantaneous frequency based on the hilbert 
    transform
    """
    sig = sig.real
    ts = 1.0/Fs
    hilb = signal.hilbert(sig)
    ampli = np.abs(hilb)
    
    norm = sig/ampli
    hilbnorm = signal.hilbert(norm)
    env = np.abs(hilbnorm)
    phase = np.unwrap(np.angle(hilbnorm))
    phasediff = np.append([0],np.diff(phase))
    freq = phasediff/(2*np.pi*ts)
    return freq


def calcFF_D3D(freqramp,sig,fce,fsep,Fs):
    """ Performs the first fringe estimation algorithm descreibed in the
    DIII-D paper "Improved reflectometer electron density profile 
    measurements on DIII-D" by Wang 2003
    """
    sig = sig.real
    
    #1. Limit range to fce at wall and fce at separatrix
    fceidx = get_arrayValueIdx(freqramp,fce)
    fsepidx = get_arrayValueIdx(freqramp,fsep)
    sigrange = sig[fceidx:fsepidx]
    freqramprange = freqramp[fceidx:fsepidx]
    
    #2. Rough estimate of fstart by increase in signal amplitude
    sigamp = np.abs(sigrange)
    maxidx = np.argmax(np.diff(sigamp))+1
    fstartrough = freqramprange[maxidx]
    #print 'Rough fstart is',fstartrough
    fstartidx = get_arrayValueIdx(freqramprange,fstartrough)
    
    #3. Band pass filter around average IF frequency from start to sep
    startsig = sigrange[fstartidx:]
    #use HILBERT
    freqsig = freq_hilbert(startsig,Fs)
    freqmean = np.mean(freqsig)
    freqrms = np.sqrt(np.mean(np.power(freqsig,2)))
    #print freqrms
    #freqrms = np.std(freqsig)
    #print freqrms
    #print 'freqmean:',freqmean,'freqrms',freqrms
    
    if freqrms <=0.0:
        freqrms = 1e6
    lowcut = freqmean - freqrms/2
    if lowcut <0:
        lowcut = 0
    highcut = freqmean+freqrms/2
    if highcut <6e6:
        highcut = 4e6
    print freqmean,[lowcut,highcut]
    
    if freqmean >1e6 and freqrms > 0.0:
        try:
            sigfilt = filt_bandpass(sigrange,lowcut,highcut,Fs=Fs,order=5)
        except:
            print ' calcff D3D Error: Freqmean %f, Freqrms %f. not filtering'%(freqmean,freqrms)
            sigfilt = sigrange
    
    else:
        print ' calcff D3D Error: Freqmean %f, Freqrms %f. not filtering'%(freqmean,freqrms)
        sigfilt = sigrange
    
    #4. Find Max power of the smoothed square of the filtered signal
    power = np.abs(np.power(sigfilt,2))
    #power = smooth(power,1)
    Pmax = np.max(power)
    Pmin = np.min(power)
    #print 'maxpower',Pmax
    
    #5. First frequency below whose smoothed square avalue is
    threshold = 0.02*Pmax+Pmin
    
    thresholdidx = np.argmax(power>threshold)-1
    print 'threshold',threshold,thresholdidx
    
    fstart = freqramprange[thresholdidx]
    #print 'fstart',fstart
    
    if 0:
        plt.figure()
        ax=plt.subplot(111)
        plt.plot(freqramprange,sigrange,'b')
        plt.plot(freqramprange,sigfilt,'g')
        plt.plot(freqramprange,power,'r')
        
        ax.axvline(x=fstart,color='k')
        plt.show()
    
    return fstart