""" libnn - Contains the set of tools to handle the neural networks
train and test data sets, trainning models, storing and loading models,
etc


"""
import numpy as np
import pickle

import copy
#import keras
from keras.models import load_model
import keras

from keras import metrics
import keras.backend as K

__vecy_pflims = (40e9,68e9)
__vecy_npoints = 280*4
__vecx_fblims = (0,25e6)
__vecy_shiftlims = (40e9,64e9)
__vecy_shiftspan = 2e9
__vecy_shiftcases = 50

vecy_pflims = __vecy_pflims
vecy_npoints = __vecy_npoints 
vecx_fblims = __vecx_fblims

colors2 = ['#1f77b4','#ff7f0e','#2ca02c','#d62728','#9467bd','#8c564b','#e377c2','#7f7f7f','#bcbd22','#17becf']


class dict2obj():
    """ Translates a dictionary to an object """
    def __init__(self,d):
        self.__dict__ = d

def closest_index(array,value):
    """ Finds the index of the array element closest to value
    """
    array = np.array(array)
    return (np.abs(np.array(array)-value)).argmin()

def bin_func(x,y,bins,func=np.mean):
    """
    Calculates the func of y inside the bins using numpy digitize

    """
    digitized = np.digitize(x, bins)
    bin_means = [func(y[digitized == i]) for i in range(len(bins[:-1]))]
    #bin_means = [func(y[digitized == i]) for i in np.unique(digitized)]
    bin_means = np.array(bin_means)
    print digitized.size,bin_means.size,np.unique(digitized).size
    return bin_means
    


def separate_data(data,split=0.1,idxlist=None):
    data_x = data.x
    data_y = data.y

    train_x, train_y, test_x, test_y = split_train_test_data(data_x, 
                                                             data_y,
                                                             split=split,
                                                             idxlist=idxlist)


    train = DataXY(x=train_x, y=train_y)
    test = DataXY(x=test_x, y=test_y)
    return train,test

def split_train_test_data(data_x, data_y, split=0.1,idxlist=None):
    """ Splits the given data_x into train and test data for SPLIT
    """
    totalpoints = len(data_x)

    splitpoints = int((1-split)*totalpoints)
    testsplit = int((split)*totalpoints)

    indexes = np.arange(totalpoints)
    randindexes = np.random.randint(0,totalpoints,testsplit)
    splitmask = np.in1d(indexes, randindexes)
    #print splitmask
    nonsplitmask = np.logical_not(splitmask)
    #print nonsplitmask
    #print indexes
    #print 'Split at ',splitpoints
    if idxlist is not None:
        idxlist[:] = randindexes
        print idxlist

    #return data_x[:splitpoints],data_y[:splitpoints], data_x[splitpoints:], data_y[splitpoints:]
    if data_x.ndim >1:
        return data_x[nonsplitmask,:],data_y[nonsplitmask], data_x[splitmask,:], data_y[splitmask]
    else:
        return data_x[nonsplitmask],data_y[nonsplitmask], data_x[splitmask], data_y[splitmask]



def split_dataxy(data,split=0.1):
    if split <= 0:
        return data

    train_x,train_y,test_x,test_y = split_train_test_data(data.x, data.y, split=split)

    train = DataXY(x=train_x,y=train_y)
    test = DataXY(x=test_x,y=test_y)

    return train, test

def select_data_filter(data,filter,verbose=False):
    """ Returns the data points that have a valid filter"""

    if len(filter)==1:
        print 'Filter has only one point'
        return data

    valididx = np.where(filter)[0]
    newdata = copy.copy(data)

    keys = data.__dict__.keys()

    for key in keys:
        vector = data.__dict__[key]
        if verbose:
            print key,len(vector)#,vector
        if len(vector) <len(valididx):
            if verbose:
                print '%s does not have enough data, skipping'%(key),vector
            continue
        if isinstance(vector,list):
        #print valididx
            newdata.__dict__[key] = [vector[i] for i in valididx]
        else:
            newdata.__dict__[key] = vector[valididx]
            #newdata.__dict__[key].shape

        if verbose:
            print 'Select Data Filter:',key,len(newdata.__dict__[key])
    newdata.__dict__['idx'] = np.arange(len(newdata.shotnumber))
    return newdata

def sort_data_dict(data,idx,verbose=False):
    """ Returns the sorted data points according to idx"""

    if len(idx)==1:
        print 'Data has only one point'
        return data

    valididx = np.where(filter)[0]
    valididx = idx
    newdata = copy.copy(data)

    keys = data.__dict__.keys()

    for key in keys:
        vector = data.__dict__[key]
        if verbose:
            print key,len(vector)#,vector
        if len(vector) <=1:
            if verbose:
                print '%s does not have enough data, skipping'%(key),vector
            continue
        if isinstance(vector,list):
        #print valididx
            newdata.__dict__[key] = [vector[i] for i in valididx]
        else:
            newdata.__dict__[key] = vector[valididx]
            #newdata.__dict__[key].shape

        if verbose:
            print 'Select Data Filter:',key,len(newdata.__dict__[key])
    newdata.__dict__['idx'] = np.arange(len(newdata.shotnumber))
    return newdata



def parse_data_vec_x(fce_antenna, fce_sep, pf, fb, amp):
    """ Creates the vec_x 1d data
    """
    vec_x = []
    pfvalididx =  (pf<fce_antenna )| (pf> fce_sep)
    
    normamps = amp/max(amp)

    normamps[pfvalididx]=0
    for x in normamps:
        vec_x.append(x)

    normfb = fb/max(fb)
    normfb[pfvalididx] = 0

    for x in normfb:
        vec_x.append(x)

    fce_antenna = normalise_ff_data(fce_antenna) 
    fce_sep = normalise_ff_data(fce_sep) 
    vec_x.append(fce_antenna)
    vec_x.append(fce_sep)
    
    vec_x = np.array(vec_x).flatten()

    return vec_x

def parse_data_vec_x_2d(stft_pf,stft_fb,stft,fce_antenna,fce_sep,fblims=__vecx_fblims,maxbw=66.1e9):
    """ Creates the vec_x 2d data
    """
    vec_x = []
        
    minfbidx = closest_index(stft_fb,fblims[0])
    maxfbidx  = closest_index(stft_fb,fblims[1])
    maxbwidx = closest_index(stft_pf, maxbw)
    stft = np.copy(stft)


    stft = stft[minfbidx:maxfbidx,:maxbwidx]
    stft_fb = stft_fb[minfbidx:maxfbidx]


    stft = np.copy(stft)
    stft_fb = np.copy(stft_fb)

    #stft = stft/np.max(stft,axis=0)

    # Clears stft outside region of interest wall-separatrix
    minpfidx = closest_index(stft_pf,fce_antenna)
    maxpfidx  = closest_index(stft_pf,fce_sep)
    
#    print 'Min max pf',minpfidx,maxpfidx
    stft[:,:minpfidx] = 0
    stft[:,maxpfidx:] = 0
    
    vec_x = stft
    return vec_x
    

def parse_data_vec_x_2d_shifted(stft_pf, stft_fb, stft, 
                                fce_antenna, fce_sep,
                                fshift=None,
                                fblims=__vecx_fblims):
    """ 
    Creates the vec_x 2d data shifted to start at fshift
    
    If an fshift is provided, it will restrict the sft to the region
    of interest between fce_antenna and fce_sep and then shift this region
    so the left border is at fshift
    

    
    """

    if fshift is None:
        fshift = fce_antenna
    vec_x = []
        
    minfbidx = closest_index(stft_fb,fblims[0])
    maxfbidx  = closest_index(stft_fb,fblims[1])

    stft = np.copy(stft)


    stft = stft[minfbidx:maxfbidx,:]
    stft_fb = stft_fb[minfbidx:maxfbidx]

    # Creates a zeroed window for the stft
    newstft = np.zeros(stft.shape)

    # Clears stft outside region of interest wall-separatrix
    minpfidx = closest_index(stft_pf,fce_antenna)
    maxpfidx  = closest_index(stft_pf,fce_sep)

    # Captures the windowed region of stft
    stft = stft[:,minpfidx:maxpfidx]
    
    # Determines where is the fshift position in the bins
    fshiftidx  = closest_index(stft_pf,fshift)

    # Allocates the stft boxed region of interest onto the correct position
    regionwidth = np.abs(maxpfidx-minpfidx)
    windowwidth = len(newstft[0,:])
    leftidx = fshiftidx
    rightidx = fshiftidx + regionwidth
    if rightidx > windowwidth:
        rightidx = windowwidth-1

    #print fshift,newstft.shape, ':', leftidx, rightidx, rightidx-leftidx, stft.shape,stft[:,:rightidx-leftidx].shape
    
    newstft[:,leftidx:rightidx] = stft[:,:rightidx-leftidx]
    
    vec_x = newstft
    return vec_x



def parse_data_vec_y(ff, 
                     pflims=__vecy_pflims, 
                     npoints=__vecy_npoints,
                     type='bin'):
    vec_y = []
    
    if type in ['bin']:
        pfband = np.linspace(pflims[0],pflims[1],npoints)
        
        binarray = np.zeros(len(pfband))
        ffidx = closest_index(pfband,ff)
        binarray[ffidx] = 1
        pf = normalise_ff_data(pfband)
        vec_y = binarray
    else:
        ff = normalise_ff_data(ff)
        vec_y.append(ff)

    vec_y = np.copy(np.array(vec_y).flatten())
    
    return vec_y


def parse_train_data_1d(data):
    """ Parses the data object and returns the data_x,data_y vectors 
    to be stored """

    data_x = []
    data_y = []
    
    # 1D version
    if True:
        """ In 1d parsing the data is concatenated in a single 1d vector
        Data is [ pk_amp,pk_fb,delims]
        """
        for i in data.idx:
            
            times = data.time[i]
            signals = data.signals[i]
            freqramps = data.freqramps[i]

            fce_antenna = data.fce_antenna[i]
            fce_sep = data.fce_sep[i] 
          
            pf = data.pk_pf[i]
            fb = data.pk_fb[i]
            amp = data.pk_amp[i]
            ff = data.ff[i]
            
            vec_x = parse_data_vec_x(fce_antenna, fce_sep, pf, fb, amp)
            vec_y = parse_data_vec_y(ff)


            data_x.append(vec_x)
            data_y.append(vec_y)
        
    data_x = np.array(data_x)
    data_y = np.array(data_y)

    return data_x, data_y




def parse_train_data_2d(data, fblims=__vecx_fblims):
    """ Parses the provided data and returns the 2d data_x and single value
    data_y.
    The 2d data is the stft matrix limited to the relevant data boundaries

    """
    data_x = []
    data_y = []
    
    if 1:
        """ In 1d parsing the data is concatenated in a single 1d vector
        Data is [ pk_amp,pk_fb,delims]
        """
        for i in data.idx:
            
            times = data.time[i]
            signals = data.signals[i]
            freqramps = data.freqramps[i]
            
            vec_x = []
            vec_y = []
 
            stft = data.stft[i]

            stft_pf = data.stft_pf[i]
            stft_fb = data.stft_fb[i]

            if stft_pf[0] < 40e9:
                print i,stft_pf[0], data.antenna[i]

            fce_antenna = data.fce_antenna[i]
            fce_sep = data.fce_sep[i] 

            vec_x = parse_data_vec_x_2d(stft_pf,stft_fb,stft,fce_antenna,fce_sep,fblims=fblims)

            
            ff = data.ff[i]

            vec_y = parse_data_vec_y(ff)
            
            data_x.append(np.array(vec_x))
            data_y.append(np.array(vec_y))
            
    print 'returning 2d data as np array'
    data_x = np.array(data_x)
    data_x = data_x.reshape(data_x.shape+(1,))
    data_y = np.array(data_y)

    return data_x, data_y



def parse_train_data_2d_shifted(data, fblims=__vecx_fblims):
    """ Parses the provided data and returns the 2d data_x and data_y
    using the shifted trained set to increase traning cases.

    The 2d data is the stft matrix limited to the relevant data boundaries

    """
    data_x = []
    data_y = []
    
    """ In 1d parsing the data is concatenated in a single 1d vector
    Data is [ pk_amp,pk_fb,delims]
    """
    for i in data.idx:
        vec_x = []
        vec_y = []
        
        stft = data.stft[i]
        stft_pf = data.stft_pf[i]
        stft_fb = data.stft_fb[i]
        fce_antenna = data.fce_antenna[i]
        fce_sep = data.fce_sep[i] 
        ff = data.ff[i]
       
        fshift = fce_antenna
        # Processes original train case
        vec_x = parse_data_vec_x_2d_shifted(stft_pf,stft_fb,stft,
                                    fce_antenna,fce_sep,
                                    fshift=fshift,
                                    fblims=fblims)
        vec_y = parse_data_vec_y(ff)
       
        data_x.append(np.array(vec_x))
        data_y.append(np.array(vec_y))


        # Sweeps along the shifted cases
        fshiftcases = np.linspace(__vecy_shiftlims[0],__vecy_shiftlims[1],__vecy_shiftcases)

        fshiftmin = ff-__vecy_shiftspan
        if fshiftmin < __vecy_shiftlims[0]:
            fshiftmin = __vecy_shiftlims[0]
        fshiftmax = ff+__vecy_shiftspan
        if fshiftmax > __vecy_shiftlims[1]:
            fshiftmax = __vecy_shiftlims[1]

        fshiftcases = np.linspace(fshiftmin,fshiftmax,__vecy_shiftcases)

        #print fshiftcases
        for fshift in fshiftcases:
            #print 'Shifting for case',fshift
            vec_x = parse_data_vec_x_2d_shifted(stft_pf,stft_fb,stft,
                                        fce_antenna,fce_sep,
                                        fshift=fshift,
                                        fblims=fblims)
            ffshifted = ff - (fce_antenna-fshift)
            vec_y = parse_data_vec_y(ffshifted)
   
        
            data_x.append(np.array(vec_x))
            data_y.append(np.array(vec_y))
            
    print 'returning shifted 2d data as np array'
    data_x = np.array(data_x)
    data_x = data_x.reshape(data_x.shape+(1,))
    data_y = np.array(data_y)

    return data_x, data_y
            
def normalise_ff_data(ff):
    """ Normalises the frequency data to the expected range between 0 and 1
    from 40 to 68 GHz
    """
    #if len(ff) == 0:
    #    return ff
    ffspan = np.max(ff)-np.min(ff)
    fmin = __vecy_pflims[0]
    fmax = __vecy_pflims[1]
    ffspan = fmax-fmin

    ff = (ff-fmin)/ffspan

    return ff
    

def unnormalise_ff_data(ff):
    """ unormalises the frequency data to the expected range between 0 and 1
    from 40 to 68 GHz
    """
    #if len(ff) == 0:
    #    return ff
    ffspan = np.max(ff)-np.min(ff)
    fmin = __vecy_pflims[0]
    fmax = __vecy_pflims[1]

    ffspan = fmax-fmin

    ff = ff*ffspan+fmin

    return ff



class DataXY(object):
    """
    Simple data object with x, y values
    """

    def __init__(self,
                 x = None,
                 y = None,):
        self._x = x
        self._y = y

    @property
    def x(self):
        return self._x
 
    @property
    def y(self):
        return self._y

        
    @property
    def size(self):
        return len(self.x)

    @property
    def xshape(self):
        return self.x.shape
    
    @property
    def yshape(self):
        return self.y.shape
  
    @property
    def shape(self):
        return self.xshape,self.yshape

    @property
    def input_shape(self):
        return self.xshape[1:]

    @property
    def output_shape(self):
        return self.yshape[1]
    

    @property
    def ff(self):
        global __vecy_pflims
        global __vecy_npoints
        __vecy_npoints = 280*4
        __vecy_pflims = (40e9,68e9)
        return  __vecy_pflims[0]+ np.argmax(self.y,axis=1)*np.abs(__vecy_pflims[0]-__vecy_pflims[1])/__vecy_npoints        

    @property
    def pf(self):
        global __vecy_pflims
        global __vecy_npoints
        __vecy_npoints = 280*4
        __vecy_pflims = (40e9,68e9)
        
        pf = np.linspace(__vecy_pflims[0],__vecy_pflims[1],__vecy_npoints,endpoint=False)

        return pf


class DataSet(object):
    """
    Handles the Data Universe data functions.
    """


    def __init__(self, 
                 filename='data/nn_training_sorted_stft.pickle',
                 
                 ):
        self.filename = filename
        
        self.open(filename=filename)

    def reset_filter(self,filter=None):
        self.data = copy.copy(self.source)
        if filter is not None:
            print 'doing reset and filtering again'
            self.filter(filter)

    def open(self, filename=None):
        self.source = self.load_data(filename=self.filename)

        if not hasattr(self.source, '__dict__'):
            print 'converting dict2obj'
            #print self.source
            self.source = dict2obj(self.source)


        self.data = copy.copy(self.source)
        return 
        
    def load_data(self, 
                  filename=None):

        print 'Loading data universe from',filename
        self.data = pickle.load(open(filename,'rb'))
        return self. data

    def save_data(self,data,
                  filename=None):

        print 'Saving data universe with length %d to %s'%(len(data.ff),filename)
        pickle.dump(data,open(filename,'wb'))
        print 'Saved file!'
        return 

    def split(self,source,split=0.1):
        assert split <=1,'Split must be between 0 and 1'
        filter = np.random.choice([0,1],
                                      size=len(source.ff),
                                      p=[1-split,split])

        data = select_data_filter(source,1-filter)
        splitdata = select_data_filter(source,filter)

        print 'Split data length %d, rest data length %d'%(len(splitdata.ff),len(data.ff))
        return splitdata,data



        

    def filter(self,filter):
        self.data = select_data_filter(self.data,filter)


    def parse(self,kind='1d',shifted=False,fblims=vecx_fblims):
        if kind in ['1d','1D']:
            x,y = parse_train_data_1d(self.data)
            return  DataXY(x=x, y=y)
        else:
            if shifted:
                print 'Parsing trainned data using shift technique'
                x,y = parse_train_data_2d_shifted(self.data)
            else:
                x,y = parse_train_data_2d(self.data)
            return  DataXY(x=x, y=y)
        
    def sort(self,idx=None,source=False):
        if idx is None:
            idx = np.argsort(self.data.ff)
        if source:
            self.source = sort_data_dict(self.source,idx)
        self.data = sort_data_dict(self.data,idx)

    @property
    def size(self):
        return len(self.data.ff)


    






def create_model_1d(input_shape=1024, 
                 output_size=1, 
                 densesize=512,
                 dropout=0.2):
    """ Creates 1D model

    """
    from keras.models import Sequential
    from keras.layers import Dense
    
    from keras.layers import Dropout
    from keras.layers import Flatten
    from keras.layers.convolutional import Conv2D
    from keras.layers.convolutional import MaxPooling2D
    from keras.models import load_model

    model = Sequential()
    model.add(Dense(densesize, input_shape=input_shape,  activation='relu'))
    model.add(Dense(densesize,  activation='relu'))
    #model.add(Dense(sizeH2, init='uniform', activation='relu'))
    model.add(Dense(output_size,  activation='sigmoid'))

    model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy','mean_squared_error',acc_ff])


    return model

def acc_ff(y_true, y_pred):
    #datatrue = DataXY(x=np.array([]),y=np.array([y_true]))
    #datapred = DataXY(x=np.array([]),y=np.array([y_pred]))

    print y_true,y_pred
    truemax = K.argmax(y_true)
    predmax = K.argmax(y_pred)
    acc =(1-(truemax-predmax)/((truemax+predmax)/2))
    
    print truemax,predmax,acc
    print ''
    print ''
    print ''
    #print datatrue.ff,datapred.ff,datatrue.ff-datapred.ff
    return K.mean(predmax,truemax)


def create_model_2d(input_shape=(102,268,1), 
                    output_size=1, 
                    filter_shape=(5,5),
                    filters=50,
                    densesize=512*2,
                    dropout=0.2):
    """ Creates 2D model

    """

    from keras.models import Sequential
    from keras.layers import Dense
    
    from keras.layers import Dropout
    from keras.layers import Flatten
    from keras.layers.convolutional import Conv2D
    from keras.layers.convolutional import MaxPooling2D
    from keras.models import load_model

    model = Sequential()
    model.add(Conv2D(filters, 
                     filter_shape, 
                     input_shape=input_shape,
                     activation='relu',
                     strides=3,
                     data_format='channels_last'))
    model.add(MaxPooling2D(pool_size=(2,2)))
    model.add(Dropout(dropout))
#    model.add(Conv2D(filters, 
#                     filter_shape, 
#                     input_shape=input_shape,
#                     activation='relu',
#                     strides=3,
#                     data_format='channels_last'))
#    model.add(MaxPooling2D(pool_size=(2,2)))
#    model.add(Dropout(dropout))
    model.add(Flatten())
    model.add(Dense(densesize, activation='relu'))
#    model.add(Dense(output_size, activation='softmax'))
    model.add(Dense(output_size, activation='sigmoid'))

    if output_size ==1:
        print 'Keras using binary_crossentropy'
        model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy','mean_squared_error','binary_accuracy','categorical_accuracy'])
    else:
        print 'Keras using categorical_crossentropy'
        model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy','mean_squared_error','binary_accuracy','categorical_accuracy'])
        #model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy','mean_squared_error','binary_accuracy','categorical_accuracy'])

    return model




def nn_model_train(model, train, test, epochs=10, batches=512):
    filepath = 'models/nn_model_epochs_{epoch:02d}.hdf5'
    callback = keras.callbacks.ModelCheckpoint(filepath, monitor='val_loss', verbose=1, save_best_only=False, save_weights_only=False, mode='auto', period=1)

    history = model.fit(train.x, train.y,
                        epochs=epochs,
                        batch_size=batches,
                        verbose=1,
                        shuffle=True,
                        validation_data=(test.x,test.y),
                        
                        callbacks=[callback])
    print history.history.keys()
        
    return history
    


class ModelFFNN(object):
    """
    Class object of the FF estimation neural network model
    """


    def __init__(self,
                 modelfilename=None,
                 ):
        self._model = None

        self.pflims = vecy_pflims
        self.npoints = vecy_npoints
        self.fblims = vecx_fblims
        self.data = None

        
        self.open(modelfilename) 

        self.history = {}

    def open(self,modelfilename=None):
        if modelfilename is None:
            modelfilename = 'models/nn_model.h5'

        print 'ModelFFNN: Loading model from %s'%(modelfilename)
        self._model =load_model(modelfilename)
        self.model = self._model

    def train(self, train, test, epochs=10, batches=512):
        nn_model_train(model, train, test, epochs=10, batches=512)
    
    def calc_ff(self, 
                stft_pf, stft_fb, stft,
                fce_antenna, fce_sep):

        vec_x = parse_data_vec_x_2d(stft_pf,stft_fb,stft,fce_antenna,fce_sep,fblims=self.fblims)
        vec_x = np.array([vec_x])
        #vec_x = vec_x.reshape((1,)+vec_x.shape)
        vec_x = vec_x.reshape(vec_x.shape+(1,))

        preds = self._model.predict(vec_x)
        
        self.data = DataXY(x=vec_x,y=preds)
        #result = preds[0]

        #print vec_x.shape,preds.shape
        #pfbinidx = np.argmax(result)
        #pfbins = np.linspace(self.pflims[0],self.pflims[1],self.npoints)
        
        #ff = pfbins[pfbinidx]
        
        #print 'Result is %0.2f and shape '%(self.data.ff/1e9),self.data.shape

        return self.data.ff



def predict_data_set(data,model=None,modelfilename='models/nn_model_shifted_all.h5'):
    
    if model is None:
        print 'Model: Loading model from %s'%(modelfilename)
        model = load_model(modelfilename)

    vec_x = data.x
    print 'Model: Predicting data %d'%(len(vec_x))
    preds = model.predict(vec_x)
    result = DataXY(x=vec_x,y=preds)
    return result

if __name__ == "__main__":
    
    import argparse
    
    import matplotlib.pylab as plt
    import matplotlib.colors as colors


    parser = argparse.ArgumentParser(description='Loads data, creates nn model, trains the data')
    parser.add_argument('-b','--batches',help='batches',type=int,default=512)
    parser.add_argument('-e','--epochs',help='epochs',type=int,default=10)
    parser.add_argument('-s','--split',help='split',type=float,default=0.1) 
    parser.add_argument('-k','--kind',help='kind',type=str,default='2d') 
    parser.add_argument('-m','--model',help='kind',type=str,default='') 

   
    parser.add_argument('-r','--rampup',action="store_true",default=False)
    parser.add_argument('-lc','--lowercutoff',action="store_true",default=False)
    parser.add_argument('-new','--newmodel',action="store_true",default=False)
    parser.add_argument('-train','--newtrain',action="store_true",default=False)
    parser.add_argument('-shifted','--shifted',action="store_true",default=False)

    args = parser.parse_args()
    
    
    epochs = args.epochs
    batches = args.batches
    split = args.split
    kind = args.kind
    modelfile = args.model

    rampup = args.rampup
    lowercutoff = args.lowercutoff

    newmodel = args.newmodel
    newtrain = args.newtrain
    shifted = args.shifted

    np.random.seed(111)
   #data_x = np.random.rand(1292,102,268,1)
   #data_y = np.random.rand(1292,1)

   #data = DataXY(x=data_x, y=data_y)

    filename = 'data/nn_training_sorted_stft.pickle'
    filename = 'data/nn_trainning_sorted_stft_split0.9.pickle'
    filenametest = 'data/nn_trainning_sorted_stft_split0.1.pickle'
    #filename = 'data/smallsubset1.pickle'
    #filename = 'data/smallsubset1_nondict.pickle'

    if modelfile is '':
        modelfilename = 'models/nn_model.h5'
    else:
        modelfilename = modelfile
    #modelfilename = 'models/nn_model_shifted.h5'



    if 0:
        dataset = DataSet(filename=filename)
        datatest = DataSet(filename=filenametest)
        #dataset.filter(np.random.choice([0,1],size=dataset.size))

        print 'train',len(dataset.data.ff)
        print 'test',len(datatest.data.ff)
        train = dataset.parse(kind=kind,shifted=shifted)
        print 'Kind: ',kind,train.size, train.shape
   
        totalpoints = len(dataset.data.ff)
        idxlist = np.zeros(totalpoints)
    #print 'idxlist',len(idxlist),idxlist
    #train,test = separate_data(train,split=split)
    
        test = datatest.parse(kind=kind,shifted=shifted)

    #print 'idxlist after',len(idxlist),idxlist

   #train.x.reshape(train.x.shape+(1,))
    else:
        print 'loading test_y'
        test_y = np.load('data/parsed_test_y_split0.1.npy')
        print 'loading test_x'
        test_x = np.load('data/parsed_test_x_split0.1.npy')
        test = DataXY(x=test_x,y=test_y)

        print 'loading train_y'
        train_y = np.load('data/parsed_train_y_split0.9.npy')
        print 'loading train_x'
        train_x = np.load('data/parsed_train_x_split0.9.npy')
        train = DataXY(x=train_x,y=train_y)


    print 'Train:', train.size, train.shape,train.input_shape,train.output_shape
    print 'Test:', test.size, test.shape,test.input_shape,test.output_shape

#   kind = '2d'


    if newmodel:
        if kind in ['1d','1D']:
            model = create_model_1d(input_shape=train.input_shape,
                                    output_size=train.output_shape)
        else:
            model = create_model_2d(input_shape=train.input_shape,
                                    output_size=train.output_shape)
    else:
        model = load_model(modelfilename)


    history = None
    if newtrain:
        history = nn_model_train(model, train, test, epochs=epochs, batches=batches)
        
        model.save(modelfilename)
    


    

    print train.size, train.shape



    dataset.reset_filter()
    test = dataset.parse(kind=kind,shifted=shifted)

    preds = model.predict(test.x)
    #testx = test.x[0]
    #testx = testx.reshape((1,)+testx.shape)
    #preds = model.predict(testx)

    prediction = DataXY(x=test.x,y=preds)
    
    trainpred = DataXY(x=train.x,y=model.predict(train.x))
    testred = DataXY(x=test.x,y=model.predict(test.x))
    
    maxtests = np.argmax(test.y,axis=1)
    maxpreds = np.argmax(preds,axis=1)
    
    print 'Tests:',maxtests
    print 'Preds:',maxpreds
    
    prederror = (maxpreds-maxtests)*np.abs(__vecy_pflims[0]-__vecy_pflims[1])/__vecy_npoints
    prederror = test.ff-prediction.ff
    prederrormean = prederror.mean()
    prederrorstd = prederror.std()
 
    print 'Error mean %0.4f GHz std %0.4f GHz'%(prederrormean/1e9,prederrorstd/1e9)
    #preds = (preds.T/np.argmax(preds,axis=1)).T

    if 0:
        ntests = 3
        #f,axarr = plt.subplots(ntests+1,1)
        #plt.figure()
        idx = 300
        stft = dataset.data.stft[idx]
        pf = dataset.data.stft_pf[idx]
        fb = dataset.data.stft_fb[idx]
    #plt.pcolormesh(pf,fb,stft)
        
        stft = test.x[idx].reshape(test.x[idx].shape[:-1])
        
        plt.pcolormesh(stft)
        ffidx = float(np.argmax(test.y[idx]))
        ffidx = ffidx/len(test.y[idx])*268
        plt.gca().axvline(x=ffidx)
        

    plt.figure()
    if 0:
    #norm = colors.LogNorm(vmin=1e-20,vmax=1)
        sortidx = np.argsort(maxtests)
        sortedtests = maxtests[sortidx]
        sortedpreds = maxpreds[sortidx]
    #ffalgorithm = dataset.data.ff_algorithm[sortidx]
        plt.plot(sortedpreds,c='y')
        plt.plot(sortedtests,c='k')
        #    plt.pcolormesh(preds.T,norm=norm,cmap='jet')
    #plt.pcolormesh(preds.T,cmap='jet')
        plt.plot(maxtests,c='w',label='FF')
        plt.plot(maxpreds,c='y',label='preds')
    
    plt.plot(test.ff,c='k')
    plt.plot(prediction.ff,c='y')
    plt.plot(dataset.data.ff_algorithm,c='b')

    

    if 0:

        plt.figure()
        
        totalpreds = len(preds)
        stepy = 1
        for i in range(totalpreds):
            
            plt.plot(preds[i]*0.9+stepy*i,lw=1,c='lightgray')
            plt.plot(test.y[i]*0.95+stepy*i,lw=0.1,c='k')
            
    if history is not None:
        #plt.figure()
        historyfilename = modelfilename+'_history.pickle'
        print 'Saving model training history to ',historyfilename
        pickle.dump(history.history,open(historyfilename,'wb'))


        figdpi = 300
        fi = 0
        filenamepng = 'images/sofe2017_train_history_%d.png'
        filenamepdf = 'images/sofe2017_train_history_%d.pdf'

        f,axarr = plt.subplots(2,1,figsize=(5,4),sharex=True)

        ax = axarr[0]
        ax.plot(history.history['acc'])
        ax.plot(history.history['val_acc'])
        #ax.title('model accuracy')
        ax.set_ylabel('Accuracy')
        #plt.xlabel('epoch')
        

        #plt.legend(['train', 'test'], loc='upper left')
        #plt.show()
# summarize history for loss
        ax = axarr[1]
        ax.plot(history.history['loss'])
        ax.plot(history.history['val_loss'])
        #plt.title('model loss')
        ax.set_ylabel('Loss')
        ax.set_xlabel('Training epoch')

        ax.legend(['Train', 'Test'], loc='upper right')
        
        f.add_subplot(111, frameon=False)
        plt.tick_params(labelcolor='none', top='off', bottom='off', left='off', right='off')
        plt.subplots_adjust(left=None, bottom=0.16, right=None, top=None, wspace=0.05, hspace=0)
        plt.title('Model training results')
        fi += 1;f.savefig(filenamepng%(fi),dpi=figdpi);f.savefig(filenamepdf%(fi),dpi=figdpi);print filenamepng%(fi)


    if 0:
        shifted=False
        train = dataset.parse(kind=kind,shifted=shifted)
        test = datatest.parse(kind=kind,shifted=shifted)
    #
        trainpred = DataXY(x=train.x,y=model.predict(train.x))
        testpred = DataXY(x=test.x,y=model.predict(test.x))
    #
        truetrainff = train.ff
        truetestff = test.ff
        trainpredff = trainpred.ff
        testpredff = testpred.ff
    #
        trainaccuracy = 1- np.abs(truetrainff-trainpredff)/np.mean([truetrainff,trainpredff],axis=0)
        testaccuracy = 1- np.abs(truetestff-testpredff)/np.mean([truetestff,testpredff],axis=0)

        print 'Train mean', trainaccuracy.mean(), 'test mean', testaccuracy.mean()
    plt.show()
#   print train2d.size, train2d.shape
#   print data.x
#   print data.y
