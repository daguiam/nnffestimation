#!/usr/bin/env python
import matplotlib.pyplot as plt
#import matplotlib.pylab as plt
#import matplotlib as mpl
from matplotlib import gridspec
#from plot_params import paper_params

import dd 	
import numpy as np
import sys
#sys.path.insert(0, '../classpython/')
#from  rif_augdlib import *
#from rif_library import *

import ricg

#from latexify import *

def get_diagSignal(diagname, signalname, shotnumber,experiment='AUGD'):
  try:
    diag = dd.shotfile(diagname, shotnumber,experiment=experiment);
  except:
    print 'dd error: There is no ' + signalname + ' in '+ diagname+' for #'+str(shotnumber);
    #exit();
    return 0;
  else:
    signal = diag(signalname);
    return signal;

def get_areabase(diagname, signalname, shotnumber,experiment='AUGD'):
  try:
    diag = dd.shotfile(diagname, shotnumber,experiment=experiment);
  except:
    print 'dd error: There is no ' + signalname + ' in '+ diagname+' for #'+str(shotnumber);
    #exit();
    return 0;
  else:
    signal = diag.getAreaBase(signalname);
    return signal;

# Implemented smooth moving average algorithm
def smooth(data,N=5):
    return np.convolve(np.ones(N)/N,data,'same');


def smooth2d(data,N):
    for i in range(len(data[0,:])):
        data[:,i] = smooth(data[:,i],N)
    return data

shotnumber = 33520
shotnumber = 33490
timeval = 5.5

# 33520
#shotnumber =33214
#timeval = 2.4763

experiment = 'RICG'

diagname = 'RIL'
signalname = 'RhoAnt1'
antenna = 1
nsmooth = 50


#plt.subplot(311)
nescale = 1e-19
pfscale = 1e-9
fbscale = 1e-6
dgscale = 1e9

diagname = 'RIC'
sigprefix = 'Ne_Ant%d'

tickpad =4
labelpad=3
markersize = 2
textsize = 7
markers = ['-^','-<','->',]
markeri = 0
alphavalue=0.6
antennaarray = [1,4,8]

datastride = 100
#gs = gridspec.GridSpec(1,2,width_ratios=[3,2])


#plt.rcParams.update(paper_params)



fig=plt.figure(figsize=(3.37,2.2),dpi=200)

#fig=plt.figure(figsize=(3.39,3))
#fig = plt.figure()
#fig.set_size_inches(3.5,2)
#fig.set_dpi=300

#ax4 = plt.subplot(gs[0])

  
cmapval = 'nipy_spectral'
cmapval = 'Greys'
#cmapval = 'Blues'

plostruct = (4,1)
xlim=[40,68]
ylim=[-4,14]

yticks=[0,5,10]
nracums =10
#nracums =2
############## 1
ax4 = plt.subplot2grid(plostruct,(0,0),colspan=1,rowspan=1)

shotnumber = 33291
timeval = 3.4
timeval=2.635
shot = ricg.RIF(shotnumber=shotnumber,antenna=antenna,time=timeval);
#shot.initOutput();
shot.sweepnr=shot.sweepnr-nracums
shot.initPersistenceSTFTNwindow(nracums)
nriter = nracums
#shot.dospline = True
for i in range(0,nriter):
    shot.nextSweep();

    print "Shot #"+str(shot.shotnumber)+' Ant'+str(shot.antenna)+" Sweep "+str(shot.sweepnr)+" Time %0.4f s"%shot.time
    shot.processProfile()


pfbeg = shot.ff*pfscale#shot.dp_pf[0]*pfscale
pfend = 68#shot.dp_pf[len(shot.dp_pf)-1]*pfscale
ax4.axvspan(pfbeg,pfend,alpha=0.05,color='red',label='Upper cut off')


plt.pcolormesh(shot.pf*pfscale,shot.dg*dgscale,shot.stft,cmap=cmapval,rasterized=True)

plt.plot(shot.dp_pf[2:]*pfscale,shot.dp_dg[2:]*dgscale,'r.',alpha=0.5,label='Upper cut off',linewidth=0.3);

shot.calcReflectionSignalLC()
if len(shot.dplc_pf) >0:
  plt.plot(shot.dplc_pf*pfscale,shot.dplc_dg*dgscale,'g.',alpha=0.5,label='Lower cut off',linewidth=0.3);
  pfbeg = shot.dplc_pf[0]*pfscale
  pfbeg = 40*pfscale
  pfend = shot.dplc_pf[len(shot.dplc_pf)-1]*pfscale
  ax4.axvspan(pfbeg,pfend,alpha=0.05,color='green',label='Lower cut off')


#if shot.ff is not None:
plt.axvline(x=shot.ff*pfscale, ymin=0, ymax=1,color='b',linestyle='--',dashes=(1,1),linewidth=0.5,label='FF')

#ax4.tick_params(axis='both',which='major',pad=tickpad)
strtext = r'a) #%d  @  $%.1f$ s   B$_T$=$%.1f$ T   n$_e$ = $%.1f\times10^{19}$ m$^{-3}$'%(shotnumber,timeval,-2.1,3.1)
ax4.text(0.5,0.62,strtext,horizontalalignment='center',transform=ax4.transAxes,fontsize=6)
#ax4.yaxis.set_ticks(np.arange(0, 30,5))

plt.xlim(xlim)
plt.ylim(ylim)
plt.grid(False)
ax4.set_xticklabels([])
#ax4.set_yticklabels([yticks])
ax4.yaxis.set_ticks(yticks)

#plt.ylabel(r'$\tau_g$ [ns]')



############## 2

ax4 = plt.subplot2grid(plostruct,(1,0),colspan=1,rowspan=1)

shotnumber = 33291
timeval = 3.4
#timeval=2.635
shot = ricg.RIF(shotnumber=shotnumber,antenna=antenna,time=timeval);
#shot.initOutput();
shot.sweepnr=shot.sweepnr-nracums
shot.initPersistenceSTFTNwindow(nracums)
nriter = nracums
#shot.dospline = True
for i in range(0,nriter):
    shot.nextSweep();

    print "Shot #"+str(shot.shotnumber)+' Ant'+str(shot.antenna)+" Sweep "+str(shot.sweepnr)+" Time %0.4f s"%shot.time
    shot.processProfile()


pfbeg = shot.ff*pfscale#shot.dp_pf[0]*pfscale
pfend = 68#shot.dp_pf[len(shot.dp_pf)-1]*pfscale
ax4.axvspan(pfbeg,pfend,alpha=0.05,color='red',label='Upper cut off')


plt.pcolormesh(shot.pf*pfscale,shot.dg*dgscale,shot.stft,cmap=cmapval,rasterized=True)

plt.plot(shot.dp_pf[2:]*pfscale,shot.dp_dg[2:]*dgscale,'r.',alpha=0.5,label='Upper cut off',linewidth=0.3);

shot.calcReflectionSignalLC()
if len(shot.dplc_pf) >0:
  plt.plot(shot.dplc_pf*pfscale,shot.dplc_dg*dgscale,'g.',alpha=0.5,label='Lower cut off',linewidth=0.3);
  pfbeg = shot.dplc_pf[0]*pfscale
  pfbeg = 40*pfscale
  pfend = shot.dplc_pf[len(shot.dplc_pf)-1]*pfscale
  ax4.axvspan(pfbeg,pfend,alpha=0.05,color='green',label='Lower cut off')


#if shot.ff is not None:
plt.axvline(x=shot.ff*pfscale, ymin=0, ymax=1,color='b',linestyle='--',dashes=(1,1),linewidth=0.5,label='FF')

#ax4.tick_params(axis='both',which='major',pad=tickpad)
strtext = r'#%d @ %.1f s B$_T$=%.1f T'%(shotnumber,timeval,-2.1)

strtext = r'b) #%d  @  $%.1f$ s   B$_T$=$%.1f$ T   n$_e$ = $%.1f\times10^{19}$ m$^{-3}$'%(shotnumber,timeval,-2.1,6)
ax4.text(0.5,0.62,strtext,horizontalalignment='center',transform=ax4.transAxes,fontsize=6)
#ax4.yaxis.set_ticks(np.arange(0, 30,5))

plt.xlim(xlim)
plt.ylim(ylim)
plt.grid(False)
ax4.set_xticklabels([])
ax4.yaxis.set_ticks(yticks)
#plt.ylabel(r'$\tau_g$ [ns]')


############## 3

ax4 = plt.subplot2grid(plostruct,(2,0),colspan=1,rowspan=1)

shotnumber = 33490
timeval = 3.4
timeval=1.53
shot = RIF(shotnumber=shotnumber,antenna=antenna,time=timeval);
shot.initOutput();
shot.sweepnr=shot.sweepnr-nracums
shot.initPersistenceSTFTNwindow(nracums)
nriter = nracums
#shot.dospline = True
for i in range(0,nriter):
    shot.nextSweep();

    print "Shot #"+str(shot.shotnumber)+' Ant'+str(shot.antenna)+" Sweep "+str(shot.sweepnr)+" Time %0.4f s"%shot.time
    shot.processProfile()


pfbeg = shot.ff*pfscale#shot.dp_pf[0]*pfscale
pfend = 68#shot.dp_pf[len(shot.dp_pf)-1]*pfscale
ax4.axvspan(pfbeg,pfend,alpha=0.05,color='red',label='Upper cut off')


plt.pcolormesh(shot.pf*pfscale,shot.dg*dgscale,shot.stft,cmap=cmapval,rasterized=True)

plt.plot(shot.dp_pf[2:]*pfscale,shot.dp_dg[2:]*dgscale,'r.',alpha=0.5,label='Upper cut off',linewidth=0.3);

shot.calcReflectionSignalLC()
if len(shot.dplc_pf) >0:
  plt.plot(shot.dplc_pf*pfscale,shot.dplc_dg*dgscale,'g.',alpha=0.5,label='Lower cut off',linewidth=0.3);
  pfbeg = shot.dplc_pf[0]*pfscale
  pfbeg = 40*pfscale
  pfend = shot.dplc_pf[len(shot.dplc_pf)-1]*pfscale
  ax4.axvspan(pfbeg,pfend,alpha=0.05,color='green',label='Lower cut off')


#if shot.ff is not None:
plt.axvline(x=shot.ff*pfscale, ymin=0, ymax=1,color='b',linestyle='--',dashes=(1,1),linewidth=0.5,label='FF')

#ax4.tick_params(axis='both',which='major',pad=tickpad)
strtext = r'#%d @ %.1f s B$_T$=%.1f T'%(shotnumber,timeval,-2.6)
strtext = r'c) #%d  @  $%.1f$ s   B$_T$=$%.1f$ T   n$_e$ = $%.1f\times10^{19}$ m$^{-3}$'%(shotnumber,timeval,-2.6,5)
ax4.text(0.5,0.62,strtext,horizontalalignment='center',transform=ax4.transAxes,fontsize=6)
#ax4.yaxis.set_ticks(np.arange(0, 30,5))

plt.xlim(xlim)
plt.ylim(ylim)
plt.grid(False)
ax4.set_xticklabels([])
#ax4.set_yticklabels([yticks])
ax4.yaxis.set_ticks(yticks)

plt.ylabel(r'$\tau_g$ [ns]')


############## 4

ax4 = plt.subplot2grid(plostruct,(3,0),colspan=1,rowspan=1)

shotnumber = 33490
timeval = 3.4
timeval=1.8
shot = RIF(shotnumber=shotnumber,antenna=antenna,time=timeval);
shot.initOutput();
shot.sweepnr=shot.sweepnr-nracums
shot.initPersistenceSTFTNwindow(nracums)
nriter = nracums
#shot.dospline = True
for i in range(0,nriter):
    shot.nextSweep();

    print "Shot #"+str(shot.shotnumber)+' Ant'+str(shot.antenna)+" Sweep "+str(shot.sweepnr)+" Time %0.4f s"%shot.time
    shot.processProfile()


pfbeg = shot.ff*pfscale#shot.dp_pf[0]*pfscale
pfend = 68#shot.dp_pf[len(shot.dp_pf)-1]*pfscale
ax4.axvspan(pfbeg,pfend,alpha=0.05,color='red',label='Upper cut off')


plt.pcolormesh(shot.pf*pfscale,shot.dg*dgscale,shot.stft,cmap=cmapval,rasterized=True)

plt.plot(shot.dp_pf[2:]*pfscale,shot.dp_dg[2:]*dgscale,'r.',alpha=0.5,label='Upper cut off',linewidth=0.3);

shot.calcReflectionSignalLC()
if len(shot.dplc_pf) >0:
  plt.plot(shot.dplc_pf*pfscale,shot.dplc_dg*dgscale,'g.',alpha=0.5,label='Lower cut off',linewidth=0.3);
  pfbeg = 40*pfscale#shot.dplc_pf[0]*pfscale
  pfend = shot.dplc_pf[len(shot.dplc_pf)-1]*pfscale
  ax4.axvspan(pfbeg,pfend,alpha=0.05,color='green',label='Lower cut off')


#if shot.ff is not None:
plt.axvline(x=shot.ff*pfscale, ymin=0, ymax=1,color='b',linestyle='--',dashes=(1,1),linewidth=0.5,label='FF')

#ax4.tick_params(axis='both',which='major',pad=tickpad)
strtext = r'#%d @ %.1f s B$_T$=%.1f T'%(shotnumber,timeval,-2.6)
strtext = r'd) #%d  @  $%.1f$ s   B$_T$=$%.1f$ T   n$_e$ = $%.1f\times10^{19}$ m$^{-3}$'%(shotnumber,timeval,-2.6,7)
ax4.text(0.5,0.62,strtext,horizontalalignment='center',transform=ax4.transAxes,fontsize=6)
#ax4.yaxis.set_ticks(np.arange(0, 30,5))

plt.xlim(xlim)
plt.ylim(ylim)
plt.grid(False)
#ax4.set_xticklabels([])
#ax4.set_yticklabels([yticks])
ax4.yaxis.set_ticks(yticks)

#plt.ylabel(r'$\tau_g$ [ns]')
#plt.legend()

plt.xlabel('Probing frequency [GHz]',labelpad=labelpad)

#lh = plt.legend(loc=4,scatterpoints=2)

#lh.get_frame().set_color('w')


#strtitle = 'Antenna %d #%d @ %0.4f s & B$_T$=-2.59 T'%(antenna,shotnumber,timeval)

#plt.suptitle(strtitle)


ax = plt.gca()

fig_width = 3.39
fig_height = 2

#plt.rcParams['font.size'] = 20

#plt.style.use('ggplot')
print(plt.style.available)
plt.tight_layout()
plt.subplots_adjust(top=0.99,hspace=0.08,wspace=0.25,bottom=0.19,left=0.12,right=0.99)

##ax.tick_params(axis='both',whowhichlabelsize=8)


###plt.savefig('comparefig.eps',format='eps',dpi=300)
plt.savefig('soft_xmodescenarios.png',dpi=300,transparent=True)
print 'saving eps'

plt.savefig('soft_xmodescenarios.eps',format='eps',dpi=300)
print 'showing plot'

plt.savefig('soft_xmodescenarios.pdf',format='pdf',dpi=300,transparent=True)
print 'showing pdf'

plt.show()



