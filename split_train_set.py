import libnn as nn
import numpy as np

if __name__ == "__main__":

    #filename = 'data/nn_training_sorted_stft_all.pickle'
    filename = 'data/nn_training_sorted_stft.pickle'
    #filename = 'data/smallsubset1.pickle'
    #filename = 'data/smallsubset1_nondict.pickle'

   
    print 'Loading data'
    dataset = nn.DataSet(filename=filename)
    dataset.reset_filter()


    print 'splitting data'
    split = 0.1
    #splitdata,rest = dataset.split(dataset.data,split)

    #filename='data/nn_trainning_sorted_stft_split%0.1f.pickle'%(split)
    #dataset.save_data(splitdata,filename=filename)
    
    #filename='data/nn_trainning_sorted_stft_split%0.1f.pickle'%(1-split)
    #dataset.save_data(rest,filename=filename)

    print 'parsing data'
    kind='2d'
    shifted=True

    train = dataset.parse(kind=kind,shifted=shifted)

    print 'splitting data'

    train,test = nn.split_dataxy(data=train,split=split)
    
    print 'Sizes train',train.size,' test',test.size
    
    if 0:
        filename='data/parsed_dataset_split%0.1f.pickle'%(split)
        print 'Saving',filename
        dataset.save_data(test,filename=filename)

    

        filename='data/parsed_dataset_split%0.1f.pickle'%(1-split)
        print 'Saving',filename
        dataset.save_data(train,filename=filename)

    else:
        filename='data/parsed_test_x_split%0.1f.npy'%(split)
        print 'Saving',filename
        np.save(filename,test.x)
        filename='data/parsed_test_y_split%0.1f.npy'%(split)
        print 'Saving',filename
        np.save(filename,test.y)
        
        filename='data/parsed_train_x_split%0.1f.npy'%(1-split)
        print 'Saving',filename
        np.save(filename,train.x)
        filename='data/parsed_train_y_split%0.1f.npy'%(1-split)
        print 'Saving',filename
        np.save(filename,train.y)
