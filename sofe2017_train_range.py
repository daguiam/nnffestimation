import libnn as nn
import libnn

import numpy as np

#from  nn_model import load_data

from libnn import colors2 as colors
from keras.models import load_model

from mpl_toolkits.axes_grid1 import make_axes_locatable


if __name__ == "__main__":
    import matplotlib.pylab as plt


    
    filename = 'data/nn_training_sorted_stft.pickle'
    filename = 'data/nn_training_sorted_stft_all.pickle'
    #filename = 'data/nn_trainning_sorted.pickle'
    #filename = 'data/smallsubset1.pickle'
    #filename = 'data/smallsubset1_nondict.pickle'

   
    if 0:
        dataset = nn.DataSet(filename=filename)
        dataset.reset_filter()
        
        #dataset.filter(np.random.choice([0,1],size=dataset.size))
        print len(dataset.data.ff)
        
        print 'Parssing 2d data without shift'
        parseddata = dataset.parse(kind='2d',shifted=False)
        #print 'Parssing 2d data with shift'
        #parsedshift = dataset.parse(kind='2d',shifted=True)
   

    ffidx = np.load('data/traindataff.dat.npy')
    pfvec = np.load('data/traindatapf.dat.npy')

    ffdata = pfvec[ffidx]

    print 'loading test_y'
    test_y = np.load('data/parsed_test_y_split0.1.npy')
    print 'loading test_x'
    test_x = np.load('data/parsed_test_x_split0.1.npy')
    test = libnn.DataXY(x=test_x,y=test_y)

    if 1:
        print 'loading train_y'
        train_y = np.load('data/parsed_train_y_split0.9.npy')
        print 'loading train_x'
        train_x = np.load('data/parsed_train_x_split0.9.npy')
        
        print 'appending test to train_x',test_x.shape,test_y.shape
        #train_y = np.append(train_y,test_y)
        #train_x = np.append(train_x,test_x)
        print 'appended test to train_x',test_x.shape,test_y.shape
        
        train = libnn.DataXY(x=train_x,y=train_y)
        #exit()

        parsedshift = train
    else:
        parsedshift = test

    modelfilename = 'models/nn_model_split09_nonshift.h5'
    model_nonshifted = load_model(modelfilename)
    
    modelfilename = 'models/nn_model_shifted_all.h5'
    modelfilename = 'models/nn_model_complete_shifted_splot09.h5'
    modelfilename = 'models/nn_model_epochs_26.hdf5'



    model = load_model(modelfilename)

    parseddata = test
    ff = parseddata.ff/1e9
    ff = ffdata/1e9
    bins = parsedshift.pf/1e9
    ffshifted = parsedshift.ff/1e9

    testff = parsedshift.ff

    ffhist,bins = np.histogram(ff,bins)
    ffshist,bins = np.histogram(ffshifted,bins)

    widths = np.diff(bins)
    histx = bins[:-1]
    
    totalff = len(ff)
    totalffs = len(ffshifted)





    #filename = 'data/nn_training_sorted_stft.pickle'
    #filename = 'data/nn_trainning_sorted_stft_split0.1.pickle'
    #dataset = nn.DataSet(filename=filename)
    #dataset.reset_filter()

    #print len(dataset.data.ff)

    #print 'Parssing 2d data without shift'
    #parseddata = dataset.parse(kind='2d',shifted=False)
    #print 'Parssing 2d data with shift'
    #parsedshift = dataset.parse(kind='2d',shifted=True)
   
    if 0:
        preds = model_nonshifted.predict(parseddata.x)
        prednonshift = nn.DataXY(x=parseddata.x,y=preds)
        preds= model.predict(parseddata.x)
        predshift = nn.DataXY(x=parseddata.x,y=preds)
        x = ff
    else:
        
        preds = model_nonshifted.predict(test.x)
        #preds = model_nonshifted.predict(parseddata.x)
        prednonshift = nn.DataXY(x=[],y=preds)
        preds= model.predict(test.x)
        predshift = nn.DataXY(x=[],y=preds)
        x = test.ff/1e9

        if 0:
            print 'Prediction train set data to compare with true FF and algorithm ff'
            predsaux = model.predict(parseddata.x)
            ff_nn = nn.DataXY(x=[],y=predsaux).ff/1e9
            ff_true = dataset.data.ff/1e9
            ff_ampfilt = dataset.data.ff_algorithm/1e9
            
            error_nn = ff_nn-ff_true
            error_ampfilt = ff_ampfilt-ff_true
            
        
            print 'Mean std error non %0.2f - %0.2f, Shifted %0.2f - %0.2f'%(error_nn.mean(),error_nn.std(),error_ampfilt.mean(),error_ampfilt.std())





    predff = prednonshift.ff/1e9
    predffs = predshift.ff/1e9
    trueff = test.ff/1e9
    trueffnon = test.ff/1e9
    #trueffnon = parseddata.ff/1e9
    
    ff_true = np.load('data/traindata_ff.npy')
    ff_ampfilt = np.load('data/traindata_ff_ampfilt.npy')

    ff_true /= 1e9
    ff_ampfilt /= 1e9

    boundmin = min(ff_true)
    boundmax = max(ff_true)

    print 'Total ff',totalff,'Total shifted',totalffs
    print 'Total ff',len(trueffnon),'Total shifted',len(predff)
    errornon = trueffnon-predff
    errorshift = trueff-predffs

    abserrornon = np.abs(errornon)
    abserrorshift = np.abs(errorshift)
    #errornon = np.abs(errornon)
    #errorshift = np.abs(errorshift)

    aboveidx = np.array(trueff>boundmin)
    belowidx = np.array(trueff<boundmax)
    
    nonidx = np.where(aboveidx*belowidx)

    
    print 'All NN REAL Mean std error sparse %0.2f - %0.2f, Extended %0.2f - %0.2f'%(errornon.mean(),errornon.std(),errorshift.mean(),errorshift.std())
    
    print 'All abs NN REAL Mean std error sparse %0.2f - %0.2f, Extended %0.2f - %0.2f'%(abserrornon.mean(),abserrornon.std(),abserrorshift.mean(),abserrorshift.std())

    print 'limited NN REAL Mean std error sparse %0.2f - %0.2f, Extended %0.2f - %0.2f'%(errornon[nonidx].mean(),errornon[nonidx].std(),errorshift[nonidx].mean(),errorshift[nonidx].std())
    print 'limited abs NN REAL Mean std error sparse %0.2f - %0.2f, Extended %0.2f - %0.2f'%(abserrornon[nonidx].mean(),abserrornon[nonidx].std(),abserrorshift[nonidx].mean(),abserrorshift[nonidx].std())
    


    aux,newbins = np.histogram(ffshifted,bins=300)
    aux,newbins = np.histogram(x,bins=300)
    nbins = newbins
    nwidths = np.diff(nbins)
    errorhistmean = nn.bin_func(x,errornon,nbins,func=np.mean)
    errorhiststd = nn.bin_func(x,errornon,nbins,func=np.std)
    errorshistmean = nn.bin_func(x,errorshift,nbins,func=np.mean)
    errorshiststd = nn.bin_func(x,errorshift,nbins,func=np.std)
    errorx = nbins[:-1]+nwidths/2


    corff= colors[1]
    corerror = colors[0]

    errorylims = [-1.4,1.4]

    corfce = 'k'
    corbound = colors[2]
    corsep = colors[7]
    alphaval = 0.8
    alphashade = 0.6
    lw = 1

    figdpi = 300
    fi = 0
    filenamepng = 'images/sofe2017_train_range_sep_%d.png'
    filenamepdf = 'images/sofe2017_train_range_sep_%d.pdf'

    #axarr = plt.subplot(111,projection='polar')
    f,axarr = plt.subplots(2,1,sharex=True,figsize=(5,3))


    f.add_subplot(211, frameon=False)
    plt.tick_params(labelcolor='none', top='off', bottom='off', left='off', right='off')

    plt.ylabel('Number of cases')
    #plt.xlabel('First fringe frequency [GHz]')
    f.add_subplot(212, frameon=False)
    plt.tick_params(labelcolor='none', top='off', bottom='off', left='off', right='off')

    plt.ylabel('Error [GHz]')
    plt.xlabel('First fringe frequency [GHz]')

    plt.subplots_adjust(left=None, bottom=0.15, right=None, top=None, wspace=0.05, hspace=0.05)

    ax = axarr[0]
    
    #axarr[1].set_ylim([-0.8,0.8])
    #axarr[3].set_ylim([-0.4,0.4])
    ax.set_xlim([40,64])
    ax.bar(histx,ffhist,widths)

    fi += 1;f.savefig(filenamepng%(fi),dpi=figdpi);f.savefig(filenamepdf%(fi),dpi=figdpi);print filenamepng%(fi)


    ax = axarr[1]

    ax.set_ylim(errorylims)
    
    ax.set_yticks([-1,-0.5,0,0.5,1])

    ax.fill_between(errorx,errorhistmean-errorhiststd,errorhistmean+errorhiststd,facecolor=corerror,alpha=alphaval,label='Deviation')
    ax.plot(errorx,errorhistmean,lw=lw,c=corff,label='Mean')
    #rects2 = ax.bar(ind + width, women_means, width, color='y', yerr=women_std)
    #ax.legend(fontsize='small')
    ax.legend(fontsize='small',loc='lower right')
    ax.axhline(0,c=corfce,alpha=alphaval,linestyle='--',lw=0.5)

    #ax.bar(histx,ffshist,widths)
    for ax in axarr:
        ax.axvline(boundmin,c=corbound,alpha=alphaval,linestyle='--',lw=0.5)
        ax.axvline(boundmax,c=corbound,alpha=alphaval,linestyle='--',lw=0.5)

    fi += 1;f.savefig(filenamepng%(fi),dpi=figdpi);f.savefig(filenamepdf%(fi),dpi=figdpi);print filenamepng%(fi)



    #axarr = plt.subplot(111,projection='polar')
    f,axarr = plt.subplots(2,1,sharex=True,figsize=(5,3))


    # EXTENDED DATA SAET
    
    f.add_subplot(211, frameon=False)
    plt.tick_params(labelcolor='none', top='off', bottom='off', left='off', right='off')

    plt.ylabel('Number of cases')
    #plt.xlabel('Probing frequency [GHz]')





    f.add_subplot(212, frameon=False)
    plt.tick_params(labelcolor='none', top='off', bottom='off', left='off', right='off')

    plt.ylabel('Error [GHz]')
    plt.xlabel('First fringe frequency [GHz]')

    plt.subplots_adjust(left=None, bottom=0.15, right=None, top=None, wspace=0.05, hspace=0.05)

    

    # With shifted model data
    axarr[0].set_ylim([0,290])

    ax = axarr[0]
    ax.set_xlim([40,64])


    ax.bar(histx,ffshist,widths)

    fi += 1;f.savefig(filenamepng%(fi),dpi=figdpi);f.savefig(filenamepdf%(fi),dpi=figdpi);print filenamepng%(fi)


    ax = axarr[1]
    ax.set_ylim(errorylims)

    ax.fill_between(errorx,errorshistmean-errorshiststd,errorshistmean+errorshiststd,facecolor=corerror,alpha=alphaval,label='Deviation')
    ax.plot(errorx,errorshistmean,lw=lw,c=corff,label='Mean')

    ax.axhline(0,c=corfce,alpha=alphaval,linestyle='--',lw=0.5)


    ax.set_yticks([-1,-0.5,0,0.5,1])

    #ax.legend(fontsize='small')
    ax.legend(fontsize='small',loc='lower right')
    for ax in axarr:
        ax.axvline(boundmin,c=corbound,alpha=alphaval,linestyle='--',lw=0.5)
        ax.axvline(boundmax,c=corbound,alpha=alphaval,linestyle='--',lw=0.5)

    fi += 1;f.savefig(filenamepng%(fi),dpi=figdpi);f.savefig(filenamepdf%(fi),dpi=figdpi);print filenamepng%(fi)






    if 1:
        ff_true = np.load('data/traindata_ff.npy')
        ff_ampfilt = np.load('data/traindata_ff_ampfilt.npy')

        ff_true /= 1e9
        ff_ampfilt /= 1e9



        #error_nn = np.abs(ff_nn-ff_true)
        error_ampfilt = np.abs(ff_ampfilt-ff_true)
        
        #error_nn = ff_nn-ff_true
        error_ampfilt = ff_ampfilt-ff_true
        abserror_ampfilt = np.abs(ff_ampfilt-ff_true)
        
        aux,newbins = np.histogram(ff_true,bins=100)
        nbins = newbins
        nwidths = np.diff(nbins)
        #errorhistmean = nn.bin_func(ff_true,error_nn,nbins,func=np.mean)
        #errorhiststd = nn.bin_func(ff_true,error_nn,nbins,func=np.std)
        errorshistmean = nn.bin_func(ff_true,error_ampfilt,nbins,func=np.mean)
        errorshiststd = nn.bin_func(ff_true,error_ampfilt,nbins,func=np.std)
        errorx = nbins[:-1]+nwidths/2


        
        print 'All ampfilt REAL Mean std error %0.2f - %0.2f, '%(error_ampfilt.mean(),error_ampfilt.std())
        print 'All ABS ampfilt REAL Mean std error %0.2f - %0.2f, '%(abserror_ampfilt.mean(),abserror_ampfilt.std())
    
        

    
        f,axarr = plt.subplots(1,1,sharex=True,figsize=(5,2))
        plt.subplots_adjust(left=None, bottom=0.28, right=None, top=None, wspace=0.05, hspace=0.05)
        
        ax = axarr
        ax.set_ylim([-2.1,2.1])
        
        ax.fill_between(errorx,errorshistmean-errorshiststd,errorshistmean+errorshiststd,facecolor=corerror,alpha=alphaval,label='Deviation')
        ax.plot(errorx,errorshistmean,lw=lw,c=corff,label='Mean')
        ax.axhline(0,c=corfce,alpha=alphaval,linestyle='--',lw=0.5)
        
        ax.set_yticks([-2,-1,0,1,2])
        
        ax.legend(fontsize='small',loc='lower right')
        
        plt.ylabel('Error [GHz]')
        plt.xlabel('First fringe frequency [GHz]')
        
        ax.set_xlim([40,64])

        ax.axvline(boundmin,c=corbound,alpha=alphaval,linestyle='--',lw=0.5)
        ax.axvline(boundmax,c=corbound,alpha=alphaval,linestyle='--',lw=0.5)
        
        fi += 1;f.savefig(filenamepng%(fi),dpi=figdpi);f.savefig(filenamepdf%(fi),dpi=figdpi);print filenamepng%(fi)
    

    plt.show()
