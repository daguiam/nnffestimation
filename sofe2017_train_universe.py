import libnn as nn


import numpy as np

#from  nn_model import load_data

from libnn import colors2 as colors

if __name__ == "__main__":
    import matplotlib.pylab as plt


    
    filename = 'data/nn_training_sorted_stft.pickle'
    #filename = 'data/smallsubset1.pickle'
    #filename = 'data/smallsubset1_nondict.pickle'

   

    dataset = nn.DataSet(filename=filename)
    dataset.reset_filter()

    dataset.filter(np.random.choice([0,1],size=dataset.size))
    print len(dataset.data.ff)



    #axarr = plt.subplot(111,projection='polar')
    f,axarr = plt.subplots(5,1,sharex=True)
    
    ax = axarr


    for antenna in [1]:
    #for antenna in [1,4,8]:

        if 1:

            dataset.reset_filter()
            totaldata = dataset.size*1.0
            print 'Antenna %d total %d'%(antenna,totaldata)
#            dataset.reset_filter(dataset.data.elm)
            dataset.reset_filter()
            dataset.filter(dataset.data.elm)
            print 'ELMs %0.3f%%'%(dataset.size/totaldata*100.0)
            dataset.reset_filter()
            dataset.filter(dataset.data.lowercutoff)
            print 'Lower Cutoff %0.3f%%'%(dataset.size/totaldata*100.0)
            dataset.reset_filter()
            dataset.filter(dataset.data.icrh>0)
            print 'ICRH %0.3f%%'%(dataset.size/totaldata*100.0)
            dataset.reset_filter()
            dataset.filter(dataset.data.ecrh>0)
            print 'ECRH %0.3f%%'%(dataset.size/totaldata*100.0)
            dataset.reset_filter()
            dataset.filter(dataset.data.nbi>0)
            print 'NBI %0.3f%%'%(dataset.size/totaldata*100.0)

        dataset.reset_filter()
        dataset.sort()
        dataset.filter(dataset.data.antenna==antenna)

        ax = axarr[0]
        ax.plot(dataset.data.ff,dataset.data.elm,label='ELMs')
        ax.plot(dataset.data.ff,dataset.data.icrh>0,label='ICRH')
        ax.plot(dataset.data.ff,dataset.data.ecrh>0,label='ECRH')
        ax.plot(dataset.data.ff,dataset.data.nbi>0,label='NBI')
        ax.plot(dataset.data.ff,dataset.data.lowercutoff,label='Lower cutoff')

    
    ax.scatter(dataset.data.ff-dataset.data.fce_antenna,dataset.data.fce_sep-dataset.data.ff,c=dataset.data.flattop)

        #ax.plot(thetavec,rvec,label='Antenna %d'%(antenna))

    #plt.legend(loc=1)

    plt.show()
