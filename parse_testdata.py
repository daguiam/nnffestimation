#!/usr/bin/env python


import sys

from scipy import constants as konst
from scipy import signal
from scipy.stats import norm

from scipy import interpolate

import math
import pickle

import numpy as np

from common import *
import glob

def ff_rule1( fb):
        return fb/max(fb)

def ff_rule2(pf, fb, amp):

        continuity = np.append([0],np.diff(fb))

        #continuity = continuity/2+0.5
        continuity = np.abs(continuity)
        #continuity = 1-continuity
        continuity = continuity/np.max(continuity)

        return continuity




def ff_rightmean(x):

        meanamp = np.zeros(len(x))
        meanampi = np.zeros(len(x))
        for i in range(len(amp)):
            meanamp[i] = np.nanmean(x[i:])
            meanampi[i] = np.nanmean(x[:i])
            #print meanamp[i],meanampi[i]
        meanamp = np.array(meanamp)
        meanampi = np.array(meanampi)
        #meanamp = np.append([0],np.diff(meanamp))

        meanamp = meanamp-meanampi
        meanamp = np.nan_to_num(meanamp)
        dist = np.max(meanamp)-np.min(meanamp)

        meanamp = meanamp-np.min(meanamp)
        meanamp = meanamp/dist

        return meanamp


def ff_rule7(pf, fb, amp):

        amp = 1-amp
        return amp

def ff_decision(amp,continuity,meanamp,meanfb=0):

        w1 = 1
        wp1 = 0
        w2 = 1
        w3 = 3
        w4 = 1
        wp4 = 0

        decision =  continuity*w2 + meanamp*w3+(meanfb*w4+wp4) * (amp*w1+wp1)#+(1-amp)
        decision = decision/np.max(np.abs(decision))
        return decision

def ff_rulefce(pf,fce):

    fceidx = get_arrayValueIdx(pf,fce)
    out = np.zeros(len(pf))
    out[fceidx:] = 1
    return out


def ff_decision2(amp,continuity,meanamp,meanfb=0):

        w1 = 0
        w2 = 0
        w3 = 1
        w4 = 1

        decision = meanfb*w4*(1-continuity)
        decision = decision/np.max(np.abs(decision))
        return decision




def ff_score(rules,weights):
    score = 0
    for i in range(len(weights)):
        r = np.array(rules[i])
        w = weights[i]

        out = r*w
        score = score+out

    maxscore = np.max(np.abs(score))
    if maxscore !=0:
        score = score/maxscore
    return score


cutoff =0.07





output_dir = './'
filepickle = output_dir+'testsweeps_total.pickle'
filepickle = output_dir+'nn_training_all.pickle'
#filepickle = output_dir+'testsweeps_lg.pickle'


output_dir = 'testsweeps/'
picklelist = glob.glob(output_dir+'nn_trainning_?????.pickle')
print picklelist

print 'Loading FFs from stored picke'

loadedtestff = []
for i,filepickle in enumerate(picklelist):
    break
    currenttest = pickle.load(open(filepickle,'rb'))
    loadedtestff += currenttest
    print len(loadedtestff), 'test cases loaded.'

filepickle = output_dir+'nn_trainning_sorted.pickle'

loadedtestff = pickle.load(open(filepickle,'rb'))
#rules matrix is [shotnumber,antenna,time,[rule1[],rule[],rule3[],... ]

evalffmatrix = []
datashotnumber = []
datatimes =[]

outdata = []
outff = []
outresult = []


print 3
#loadedtestff = pickle.load(open(filepickle,'rb'))

print '2'
for testi in range(len(loadedtestff)):

    testdata = dict2obj(loadedtestff[testi])
    testdata.stft_Fs = testdata.stft_Fs.flatten()


    Fs = testdata.stft_Fs
    window = testdata.stft_window
    windowtype =testdata.stft_wintype
    padding =testdata.stft_padding
    step = testdata.stft_step
    times = testdata.times
    sigs = np.array(testdata.sigs)
    freqramps = np.array(testdata.freqramp)
    shotnumber = testdata.shotnumber
    antenna = testdata.antenna

    fce = testdata.fce

    time0 = times[-1]
    striter = '%d: #%d Antenna %d %0.4f s'%(testi,shotnumber,antenna,time0)

    striter = striter+' FF=['
    for ff in np.array(testdata.ffs):
        striter = striter +'%0.2f, '%(ff/1e9)
    striter = striter+']'

    print striter
    #print 'FF from test file',testdata.ffs
    #print testdata.sigs


    stftpersist = None
    for i in range(len(times)):

        sig = np.array(sigs[i,:])
        time = testdata.times
        freqramp = np.array(freqramps[i,:])

        pf,fb,stft = calc_stft(freqramp,sig,Fs=Fs,window=window,padding=padding,step=step,windowtype=windowtype)

        if stftpersist is None:
            stftpersist = stft
        else:
            stftpersist = stftpersist + stft


        stft = stftpersist
    stftamplitude,stftpeakfb  = calcSTFTAmplitude(pf,fb,stft)


    def calcInterpSTFTAmplitude(freqramp,amplitude,pf):
        ratio = int(len(freqramp)/len(pf))
        amplitude = smooth(amplitude,ratio)
        interp = interpolate.interp1d(freqramp,amplitude)

        newamp = interp(pf)
        return newamp


    amp = stftamplitude
    sigamp = np.abs(sig)
    amp = calcInterpSTFTAmplitude(freqramp,sigamp,pf)
    peakfb = stftpeakfb
    pf = pf


    # Evaluate each ff based on the data collected by human operators:
    realffmean = np.mean(testdata.ffs)
    pfspan = 0.05#1.5e9

    result = norm.pdf(pf/1e9,realffmean/1e9,pfspan)
    # Normalizing
    result = result/np.max(result)
    normfb = ff_rule1(peakfb)
    #outdata.append([amp,normfb,ff_rightmean(amp),ff_rightmean(normfb),ff_rulefce(pf,fce),result])
    outdata.append([amp,normfb,ff_rulefce(pf,fce)])

    #outff.append(realffmean)
    fceidx = get_arrayValueIdx(pf,realffmean)
    outff.append(fceidx)
outff = np.array(outff)
outdata = np.array(outdata)
print outdata.shape


outfile = 'traindata.dat'
np.save(outfile,outdata)

outfile = 'traindataff.dat'
np.save(outfile,outff)

outfile = 'traindatapf.dat'
np.save(outfile,pf)
