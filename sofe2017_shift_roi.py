import libnn as nn

from matplotlib import patches
import matplotlib as mpl

from libnn import colors2 as colors
import numpy as np

from libnn import closest_index

#from keras.models import load_model


#from  nn_model import load_data


if __name__ == "__main__":
    import matplotlib.pylab as plt


    filename = 'data/nn_training_sorted_stft.pickle'
    #filename = 'data/smallsubset1.pickle'
    filename = 'data/smallsubset1_nondict.pickle'

   

    dataset = nn.DataSet(filename=filename)
    dataset.sort(np.argsort(dataset.data.ff),source=True)
    dataset.reset_filter()

    if 0:
        antenna = 4
        #dataset.reset_filter()
        datafilt = dataset.data.antenna==antenna & dataset.data.flattop & ~dataset.data.lowercutoff
        #dataset.reset_filter()
        datafiltlc = dataset.data.antenna==antenna & dataset.data.flattop & dataset.data.lowercutoff
        #dataset.reset_filter()
    if 1:
        #dataset.reset_filter()
        datafilt =  dataset.data.flattop & ~dataset.data.lowercutoff
        #dataset.reset_filter()
        datafiltlc = dataset.data.flattop & dataset.data.lowercutoff
        #dataset.reset_filter()
    

    ffvals = np.array([  45.5, 47, 51.6, 53.1, 54.5])*1e9
    indexes = []
    indexeslc = []
    for ff in ffvals:
        print 'test for normal',ff
        dataset.reset_filter()
        dataset.filter(datafilt)
        idx = nn.closest_index(dataset.data.ff, ff)
        indexes.append(idx)
        print 'test for lc',ff
        dataset.reset_filter()
        dataset.filter(datafiltlc)
        idx = nn.closest_index(dataset.data.ff, ff)
        indexeslc.append(idx)




    parseddata = dataset.parse(kind='2d',shifted=False)
    fblims = np.array(nn.vecx_fblims)/1e6
    i = 80

    stft_pf = dataset.data.stft_pf[i]/1e9
    stft_fb = dataset.data.stft_fb[i]/1e6
    stft = dataset.data.stft[i]
    
    pk_pf = dataset.data.pk_pf[i]/1e9
    pk_fb = dataset.data.pk_fb[i]/1e6
    
    ff = dataset.data.ff[i]/1e9
    fce_antenna = dataset.data.fce_antenna[i]/1e9
    fce_sep = dataset.data.fce_sep[i]/1e9
    ff_algorithm = dataset.data.ff_algorithm[i]/1e9

    stftroi =  parseddata.x[i]
    ffroi =  parseddata.y[i]
    stftroi = stftroi.reshape(stftroi.shape[0:2])
    ffroi = ffroi.reshape(ffroi.shape[0:1])

    stftroi_pf = stft_pf
    minidx = nn.closest_index(stft_fb,fblims[0])
    maxidx = nn.closest_index(stft_fb,fblims[1])
    stftroi_fb = stft_fb[minidx:maxidx]


    figdpi = 300
    fi = 0
    filenamepng = 'images/sofe2017_shift_roi_%d.png'
    filenamepdf = 'images/sofe2017_shift_roi_%d.pdf'

    print indexes,indexeslc




    corroi = colors[2]
    corff= colors[1]

    corffshift = colors[9]
    corpred = colors[4]

    corfce = 'k'
    corsep = colors[7]
    alphaval = 0.8
    alpharoi = 0.2
    lw = 1

    rows = 4
    cols = 1
    f,axarr = plt.subplots(rows,cols, figsize=(5,4),sharex=True)
    #sharex=True,sharey=True)
 
    f.add_subplot(111, frameon=False)
# hide tick and tick label of the big axes
    plt.tick_params(labelcolor='none', top='off', bottom='off', left='off', right='off')
    #plt.ylabel("Beat frequency [MHz]")
    #plt.xlabel("Probing frequency [GHz]")

    plt.ylabel('f [MHz]')
    plt.xlabel('Probing frequency [GHz]')
    plt.subplots_adjust(left=None, bottom=0.16, right=None, top=None, wspace=0.05, hspace=0)

    #plt.suptitle('X-mode response')


    for ax in axarr:
        ax.set_ylim(fblims)
        ax.set_xlim([np.min(stftroi_pf),np.max(stftroi_pf)])
    axarr[3].get_yaxis().set_ticks([])


    ff = parseddata.ff[i]/1e9

    preds = nn.predict_data_set(parseddata)
    predff = preds.ff[i]/1e9

    dataresy = parseddata.y[i]
    predsresy = preds.y[i]

    

    lenresx = len(dataresy)

    roix = np.arange(len(stftroi_pf))
    resx = np.arange(lenresx)
    respf =   np.interp(resx*len(roix)*1.0/lenresx,roix,stftroi_pf)

    respf = parseddata.pf/1e9
    norm = mpl.colors.Normalize(vmin=0.,vmax=stft.max())
    
    ax = axarr[0]

    #ax.set_ylabel('f [MHz]')

    #hff = ax.axvline(x=ff,linestyle='--',c=corff,alpha=alphaval,lw=lw,label='First fringe')

    #hff = ax.axvline(x=predff,linestyle='--',c=corpred,alpha=alphaval,lw=lw,label='Predicted FF')

    #print x1,y1,wx,wy
    #rect1 = patches.Rectangle((x1,y1),wx,wy, facecolor='none', 
    #                          edgecolor=corroi, 
    #                          alpha=alphaval,zorder=100)
    #h = ax.add_patch(rect1)
    #hlist.append(h)
    #fi += 1;f.savefig(filenamepng%(fi),dpi=figdpi);f.savefig(filenamepdf%(fi),dpi=figdpi);print filenamepng%(fi)
  

    # plot parsed data
    ax.pcolormesh(stftroi_pf,stftroi_fb,stftroi,cmap='Blues',rasterized=True)

    ax.axvspan(xmin=fce_antenna,xmax=fce_sep,ymin=0,ymax=0.1,fc=corroi,alpha=alpharoi)

    hff = ax.axvline(x=ff,linestyle='--',c=corff,alpha=alphaval,lw=lw,label='First fringe')

    fi += 1;f.savefig(filenamepng%(fi),dpi=figdpi);f.savefig(filenamepdf%(fi),dpi=figdpi);print filenamepng%(fi)

  

    fspan = 0.8

    fdiff = -fspan
    fshift = fce_antenna+fdiff
    newstft_roi = nn.parse_data_vec_x_2d_shifted(stft_pf, stft_fb, stft, 
                                fce_antenna, fce_sep,
                                fshift=fshift,
                                fblims=fblims)

    ax = axarr[1]
    ax.pcolormesh(stftroi_pf,stftroi_fb,newstft_roi,cmap='Blues',rasterized=True)
    ax.axvspan(xmin=fce_antenna+fdiff,xmax=fce_sep+fdiff,ymin=0,ymax=0.1,fc=corroi,alpha=alpharoi)
    #hff = ax.axvline(x=ff,linestyle='--',c=corff,alpha=alphaval,lw=lw,label='First fringe')

    hff = ax.axvline(x=ff+fdiff,linestyle='--',c=corffshift,alpha=alphaval,lw=lw,label='First fringe')
    fi += 1;f.savefig(filenamepng%(fi),dpi=figdpi);f.savefig(filenamepdf%(fi),dpi=figdpi);print filenamepng%(fi)


    ax = axarr[2]
    fdiff = fspan
    fshift = fce_antenna+fdiff
    newstft_roi = nn.parse_data_vec_x_2d_shifted(stft_pf, stft_fb, stft, 
                                fce_antenna, fce_sep,
                                fshift=fshift,
                                fblims=fblims)


    ax.pcolormesh(stftroi_pf,stftroi_fb,newstft_roi,cmap='Blues',rasterized=True)
   
    ax.axvspan(xmin=fce_antenna+fdiff,xmax=fce_sep+fdiff,ymin=0,ymax=0.1,fc=corroi,alpha=alpharoi)
    #hff = ax.axvline(x=ff,linestyle='--',c=corff,alpha=alphaval,lw=lw,label='First fringe')
    hff = ax.axvline(x=ff+fdiff,linestyle='--',c=corffshift,alpha=alphaval,lw=lw,label='First fringe')

    fi += 1;f.savefig(filenamepng%(fi),dpi=figdpi);f.savefig(filenamepdf%(fi),dpi=figdpi);print filenamepng%(fi)

#    ax.plot(respf,dataresy)

    #ax.scatter(ff,1)
#    ax.scatter(respf,predsresy)
    #predsresy = predsresy/np.max(predsresy)
    #ax.plot(respf,predsresy)
    #ax.scatter(predff,1,c=corpred)
    #ax = axarr[2]
    totalpredsy = np.sum(preds.y,axis=0)

    #ax.axvspan(xmin=fce_antenna+fdiff,xmax=fce_sep+fdiff,ymin=0,ymax=0.1,fc=corroi,alpha=alpharoi)
    #hff = ax.axvline(x=ff,linestyle='--',c=corff,alpha=alphaval,lw=lw,label='First fringe')

    ax = axarr[3]
    
    fspan = 1.3
    ax.axvspan(xmin=ff-fspan,xmax=ff+fspan,fc=corffshift,alpha=alpharoi,
               label='Trained FF vicinity')

    hff = ax.axvline(x=ff,linestyle='--',c=corff,alpha=alphaval,lw=lw,label='Original FF')
    ax.tick_params( left='off', right='off')
   
    ax.legend(loc=2)


    fi += 1;f.savefig(filenamepng%(fi),dpi=figdpi);f.savefig(filenamepdf%(fi),dpi=figdpi);print filenamepng%(fi)

    #fi += 1;f.savefig(filenamepng%(fi),dpi=figdpi);f.savefig(filenamepdf%(fi),dpi=figdpi);print filenamepng%(fi)


    plt.show()
