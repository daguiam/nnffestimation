import libnn as nn

from libnn import colors2 as colors

import numpy as np

#from  nn_model import load_data

def plot_ax_stft(ax,dataset,i):
    

    sig = dataset.data.signals[i,0]*2
    freqramp = dataset.data.freqramps[i,0]/1e9


    stft_pf = dataset.data.stft_pf[i]/1e9
    stft_fb = dataset.data.stft_fb[i]/1e6
    stft = dataset.data.stft[i]
    
    pk_pf = dataset.data.pk_pf[i]/1e9
    pk_fb = dataset.data.pk_fb[i]/1e6
    
    ff = dataset.data.ff[i]/1e9
    fce_antenna = dataset.data.fce_antenna[i]/1e9
    fce_sep = dataset.data.fce_sep[i]/1e9
    ff_algorithm = dataset.data.ff_algorithm[i]/1e9
    
    ax.pcolormesh(stft_pf,stft_fb,stft,cmap='Blues', rasterized=True)


    ax.plot(freqramp,8+sig, alpha=0.9,c='k',lw=0.5)

    ffidx = nn.closest_index(pk_pf,ff)
    #ax.plot(pk_pf[ffidx:], pk_fb[ffidx:], alpha=0.5)

  

    corff= colors[1]
    corsep = colors[7]

    corfce = 'k'
    alphaval = 0.5
    ax.axvline(x=ff,c=corff,alpha=alphaval,label=r'$f_{FF}$')
    ax.axvline(x=fce_antenna,c=corfce,alpha=alphaval,label=r'$f_{ce_{wall}}$',linestyle='--')
    ax.axvline(x=fce_sep,c=corsep,alpha=alphaval,label=r'$f_{ce_{sep}}$',linestyle='--')
    #ax.axvline(x=fce_sep,c='k',alpha=alphaval)
    
 

if __name__ == "__main__":
    import matplotlib.pylab as plt


    
    filename = 'data/nn_training_sorted_stft.pickle'
    #filename = 'data/smallsubset1.pickle'
    #filename = 'data/smallsubset1_nondict.pickle'

   

    dataset = nn.DataSet(filename=filename)
    dataset.sort(np.argsort(dataset.data.ff),source=True)
    dataset.reset_filter()

    if 0:
        antenna = 4
        #dataset.reset_filter()
        datafilt = dataset.data.antenna==antenna & dataset.data.flattop & ~dataset.data.lowercutoff
        #dataset.reset_filter()
        datafiltlc = dataset.data.antenna==antenna & dataset.data.flattop & dataset.data.lowercutoff
        #dataset.reset_filter()
    if 1:
        #dataset.reset_filter()
        datafilt =  dataset.data.flattop & ~dataset.data.lowercutoff
        #dataset.reset_filter()
        datafiltlc = dataset.data.flattop & dataset.data.lowercutoff
        #dataset.reset_filter()
    

    ffvals = np.array([  45.5, 47, 51.6, 53.1, 54.5])*1e9
    indexes = []
    indexeslc = []
    for ff in ffvals:
        print 'test for normal',ff
        dataset.reset_filter()
        dataset.filter(datafilt)
        idx = nn.closest_index(dataset.data.ff, ff)
        indexes.append(idx)
        print 'test for lc',ff
        dataset.reset_filter()
        dataset.filter(datafiltlc)
        idx = nn.closest_index(dataset.data.ff, ff)
        indexeslc.append(idx)


    rows = len(indexes)
    cols = 2
    f,axarr = plt.subplots(rows,cols,sharex=True,sharey=True)
 
    f.add_subplot(111, frameon=False)
# hide tick and tick label of the big axes
    plt.tick_params(labelcolor='none', top='off', bottom='off', left='off', right='off')
    plt.ylabel("Beat frequency [MHz]")
    plt.xlabel("Probing frequency [GHz]")

    #plt.xlabel('Probing frequency [GHz]')
    plt.subplots_adjust(left=None, bottom=None, right=None, top=None, wspace=0.05, hspace=0)

    plt.suptitle('X-mode reflectometry scenarios')

    ax = axarr[0,0]
    ax.set_ylim([0,9.9])
    ax.set_xlim([41,64])

    dataset.reset_filter()
    dataset.filter(datafilt)

    print 'Total without lower cutoff:',dataset.size,indexes
    
    for axi,i in enumerate(indexes):
        ax = axarr[axi,0]
        
        plot_ax_stft(ax,dataset,i)

        axarr[axi,1].axis('off')

    axarr[0,0].set_title('Upper cut off')

    axarr[0,0].legend(loc='upper right',fontsize='small')

    dataset.reset_filter()
    dataset.filter(datafiltlc)

    print 'Total with lower cutoff:',dataset.size,indexeslc

    f.savefig('images/sofe2017_uppercutoff.pdf',dpi=300)
    #plt.show()
   
    for axi,i in enumerate(indexeslc):
        ax = axarr[axi,1]
        plot_ax_stft(ax,dataset,i)
       
        axarr[axi,1].axis('on')

    #plt.legend(loc=1)

    

    axarr[0,1].set_title('Upper & lower cut off')
    

#    axarr[2,0].axis('off')
#    axarr[3,0].axis('off')
#    axarr[3,0].axis('on')
    
    f.savefig('images/sofe2017_bothcutoff.pdf',dpi=300)
    f.savefig('images/sofe2017_bothcutoff.png',dpi=300)

    #plt.legend(ax.lines[2:],('fce_antennna','ffrequency'))


    plt.show()
