nn_ffestimation library


follow the `example_neuralnetwork_ff.py`


add this to pathfiles

`.profile`

```
setenv PYTHONPATH /afs/ipp/aug/ads-diags/common/python/lib
setenv PYTHONPATH /afs/ipp/aug/ads-diags/common/python/lib

module load git

module load cuda #/8.0m
module load cudnn/6.0
module load anaconda
module load keras
module load tensorflow
setenv DYLD_LIBRARY_PATH "$LD_LIBRARY_PATH"
```