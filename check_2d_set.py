"""
Creates and tests the model for neural network FF estimation



"""


import keras
# Create first network with Keras
from keras.models import Sequential
from keras.layers import Dense
from keras.layers import Conv2D
from keras.models import load_model
import numpy as np
from scipy.stats import norm
import matplotlib.pylab as plt
import pickle


import libdata

from  nn_model import *





if __name__ == "__main__":

    import argparse
    

    parser = argparse.ArgumentParser(description='Loads data, creates nn model, trains and plots the data')
    
    parser.add_argument('-r','--rampup',action="store_true",default=False)
    parser.add_argument('-lc','--lowercutoff',action="store_true",default=False)
    parser.add_argument('-new','--trainnew',action="store_true",default=False)
    parser.add_argument('-a','--antenna',help='epochs',type=int,default=-1)

    split = 0.1
    epochs = 30
    batches = 512
    args = parser.parse_args()
    
    antenna = args.antenna
    
    
    rampup = args.rampup
    lowercutoff = args.lowercutoff

    trainnew = args.trainnew
    np.random.seed(2)


    modelfile = 'testmodel.h5'
    # fix random seed for reproducibility
    seed = 7
    np.random.seed(seed)
    # load pima indians datase


    #do_1d()


    if 1:


        data = load_data()
    
    
        if not rampup:
            print 'Removing ramp up data'
            data = libdata.select_data_filter(data,data.flattop)
        if not lowercutoff:
            print 'Removing lower cutoff data'
            data = libdata.select_data_filter(data,np.invert(data.lowercutoff))
        if antenna != -1:
            data = libdata.select_data_filter(data,data.antenna==antenna)
    
    
    
    #data_x,data_y = libdata.parse_data_nn(data,kind='1d')
        data_x,data_y = libdata.parse_data_2d(data,fblims=(0,20e6))
   
        data_x = data_x.reshape(data_x.shape+(1,))
        print data_x,data_y
        print data_x.shape,data_y.shape
    #exit()
        test_x,test_y = data_x,data_y

    else:
        print 'creating random data'

        data_x = np.random.rand(1292,102,268,1)
        data_y = np.random.rand(1292,1)

    #print data_x,data_y
    print data_x.shape,data_y.shape
    #model = load_model(modelfile)
    model = create_model_2d()


    train_x, train_y, test_x, test_y = split_train_test_data(data_x, data_y, split=split)

    history = model.fit(train_x, train_y,   epochs=epochs,batch_size=batches,verbose=1,shuffle=True,validation_data=(test_x,test_y))


    pred_test = model.predict(test_x)


    testdiff = libdata.unnormalise_ff_data(pred_test)-libdata.unnormalise_ff_data(test_y)
    #print testdiff

    etestmean = np.mean(testdiff)
    eteststd = np.std(testdiff)

    print test_y-pred_test

    pred_train = model.predict(train_x)
    #exit()

 

    f,axarr = plt.subplots(4,1)

    ax = axarr[0]

    x = data_y
    y = data_y
    x = libdata.unnormalise_ff_data(x)/1e9
    y = libdata.unnormalise_ff_data(y)/1e9
    ax.plot(x,y,c='c',label='Correct')

    x = train_y
    y = pred_train

    x = libdata.unnormalise_ff_data(x)/1e9
    y = libdata.unnormalise_ff_data(y)/1e9

    traindiff = y-x
    emean = np.mean(traindiff)
    estd = np.std(traindiff)
    ax.scatter(x,y,c='b',label='Train set [E:%0.3f S:%0.3f]'%(emean,estd))

    x = test_y
    y = pred_test
    x = libdata.unnormalise_ff_data(x)/1e9
    y = libdata.unnormalise_ff_data(y)/1e9
    
    emean = (etestmean)/1e9
    estd = (eteststd)/1e9
    
    ax.scatter(x,y,c='r',label='Pred set [E:%0.3f S:%0.3f]'%(emean,estd))
    #ax.scatter(data_y,data_y,c='c')

  

    argff = np.argsort(data.ff)
    ax.plot(data.ff[argff]/1e9,data.fce_antenna[argff]/1e9)
    ax.plot(data.ff[argff]/1e9,data.fce_sep[argff]/1e9)
    ax.legend(loc='lower right',fontsize='x-small')


    #print len(train_y),len(test_y)


        
    ax = axarr[1]
    totalpower = data.nbi + data.ecrh+data.icrh
    totalpower = totalpower/1e6
    
    #print totalpower
      
    #ax.scatter(totalpower,testdiff/1e9)
        
    ax = axarr[2]
    
    #ax.scatter(totalpower,testdiff/1e9,c=totalpower)
    #ax.scatter(-data.nbi/1e6,testdiff/1e9,c=totalpower)
       
    ax = axarr[3]
    
    #ax.scatter(data.icrh/1e6,testdiff/1e9,c=totalpower)
    #ax.scatter(-data.ecrh/1e6,testdiff/1e9,c=totalpower)



    plt.show()
