"""
Visualizes a keras model

"""



import keras
# Create first network with Keras
from keras.models import Sequential
from keras.layers import Dense
from keras.layers import Conv2D
from keras.models import load_model
import numpy as np

from  nn_model import *

from keras.utils import plot_model
import pydot
import graphviz

if __name__ == "__main__":

    import argparse
    

    model = create_model_2d()


    
    plot_model(model, to_file='model.png')
    from IPython.display import SVG
    from keras.utils.vis_utils import model_to_dot

    SVG(model_to_dot(model).create(prog='dot', format='svg'))

