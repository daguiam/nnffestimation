"""
Verifies the data inside the pickle files


"""

import pickle
import numpy as np

import ricg

import matplotlib.pylab as plt

from ricg import common

from ricg.lib import rif_augdlib as augd


import libdata

import pickle

if __name__ == "__main__":

    filepickle = 'data/nn_trainning_33723.pickle'
    #filepickle = 'data/nn_trainning_33214.pickle'
    filepickle = 'data/nn_trainning_sorted.pickle'

    filestft = 'data/nn_training_sorted_stft.pickle'
    #filestft2 = 'data/nn_training_sorted_stft_old.pickle'

    if 1:
        #libdata.load_test_update_data(filepickle)
        dostft = True
    #dostft = False

        data = libdata.load_obj_test_data(filepickle,doStft=dostft)

    
        print  'storing', filestft
        pickle.dump(data,open(filestft,'wb'))
    print 'loading',filestft
    data = pickle.load(open(filestft,'rb'))

#    print vars(data)

    #print data.flattop

    # plotting ff, fce antenna and fsep


    



    data = libdata.select_data_filter(data,data.flattop)
    
    f,axarr = plt.subplots(3,1,sharex='col')
    ax = axarr[0]

    total_cases = len(data.ff)

    datax = np.arange(total_cases)

    datanorm = data.ff/1e9

    if 1:
        ax = axarr[2]

        #newdata = libdata.select_data_filter(data,data.flattop)
        newdata = libdata.select_data_filter(data,data.elm)
        newdata = libdata.select_data_filter(data,data.lowercutoff)
        newdata = libdata.select_data_filter(data,np.invert(data.lowercutoff))
        newdata = libdata.select_data_filter(newdata,newdata.antenna==1)

        
        x = newdata.idx
        y = newdata.freqramps[0,0,:]

        print newdata.amplitudes.shape
        z = newdata.amplitudes[:,0,:].T

        #y = newdata.pk_pf[0]
        #z = newdata.pk_amp.T

        print x.shape, y.shape, z.shape
        ax.pcolormesh(x,y,z)

        ax.plot(newdata.idx,newdata.fce_antenna,color='k')
        ax.plot(newdata.idx,newdata.ff,color='r')
        ax.plot(newdata.idx,newdata.fce_sep,color='k')
        #ax.plot(x,data.fce_antenna[valididx],color='k')
        #ax.plot(dataxv,data.ff[valididx],color='r')
        #ax.plot(dataxv,data.fce_sep[valididx],color='k')
        #ax.pcolormesh()
        
    if 0:
        ax = axarr[1]
        ax.plot(datax[flatidx],data.fce_antenna[flatidx]/1e9-datanorm[flatidx])
        ax.plot(datax[flatidx],data.ff[flatidx]/1e9 - datanorm[flatidx],'k')
        ax.plot(datax[flatidx],data.ff_algorithm[flatidx]/1e9 - datanorm[flatidx])
        ax.plot(datax[flatidx],data.fce_sep[flatidx]/1e9 - datanorm[flatidx])

    if 1:
        ax = axarr[1]

        kcolors = ['red' if elm else 'yellow' if flattop else 'blue' for elm,flattop in zip(data.elm,data.flattop)]
        #print kcolors
        fferror = (data.ff_algorithm-data.ff)/1e9
        #print fferror
        ax.scatter(datax,fferror,c=kcolors)
        #ax.scatter(datax[elmidx],fferror[elmidx])
        
        fcesortidx = np.argsort(datax)
        
        ax.plot(datax[fcesortidx],data.fce_sep[fcesortidx]/1e9-datax[fcesortidx])
        ax.plot(datax[fcesortidx],data.fce_antenna[fcesortidx]/1e9-datax[fcesortidx])


    if 0:
        ax = axarr[1]
        ax.plot(datax,data.fce_antenna/1e9)
        ax.plot(datax,data.ff/1e9,'k')
        ax.plot(datax,data.ff_algorithm/1e9)
        ax.plot(datax,data.fce_sep/1e9)
    plt.show()
