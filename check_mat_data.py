import scipy.io

import numpy as np

import matplotlib.pylab as plt

import libnn as nn
import matplotlib
from libnn import colors2 as colors

class dict2obj():
    """ Translates a dictionary to an object """
    def __init__(self,d):
        self.__dict__ = d


def closest_index(array,value):
    """ Finds the index of the array element closest to value
    """
    array = np.array(array)
    return (np.abs(np.array(array)-value)).argmin()

def ra_errors(t1,ra1,t2,ra2):
    t1len = len(t1)
    t2len = len(t2)
    
    ti = t1
    ri = ra1
    tn = t2
    rn = ra2
    if t2len <t1len:
        ti = t2
        ri = ra2
        tn = t1
        rn = ra

    rn = np.interp(ti,tn,rn)
    
    tn = rn
    print len(ri)
    error = ri-rn
    terror = ti
    return terror, error


def extract_common_data(t1,r1,t2,r2):
    commonTime = t1
    commonTime = np.intersect1d(commonTime,t2)
    valididx = np.in1d(t1,commonTime)
    r1 = r1[valididx]
    t1 = t1[valididx]
    valididx = np.in1d(t2,commonTime)
    r2 = r2[valididx]
    t2 = t2[valididx]
    return t1,r1,t2,r2

     



# Receives a 2D array and a interpolation array
# returns the 2D indexes of the value closest to each of the interpolation array
def meshNearestIndexes(ne,nearray):
    
    arrayshape = (len(ne),len(nearray))
    

    outidxs = np.empty(arrayshape).astype(int)*np.nan#*(-1)
    
    idxsprev = np.ones(len(ne))*(-1)
    for neidx in range(len(nearray)):
        neval = nearray[neidx] 
        idxs = np.argmin(np.abs(ne-neval),axis=1);
        for i in range(len(idxs)):
            if idxs[i] ==len(ne[i,:])-1:
                continue
                                 
            if (idxs[i] != idxsprev[i]):# and (ne[i,idxs[i]]-neval >=0):
                #print 'idxs[i]',idxs[i]
                outidxs[i,neidx] = idxs[i]
            else:
                a=0
                #print 'Skipped at ',times[idxs[i]], ' ne ', neval
        idxsprev = idxs
    return outidxs#.astype(int)

def getMeshIdxValues(matrix,idxs):
    outmatrix = np.empty(np.shape(idxs))*np.nan
    for i in range(len(matrix)):
        idxvec = idxs[i]
        idxvec = idxvec[np.logical_not( np.isnan(idxvec))].astype(int)
        outmatrix[i,:len(idxvec)] = matrix[i,idxvec]
  
    return outmatrix
  

       
def ra_errors2d(t1,ra1,t2,ra2):
    t1len = len(t1)
    t2len = len(t2)
    
    ti = t1
    ri = ra1
    tn = t2
    rn = ra2
    if t2len <t1len:
        ti = t2
        ri = ra2
        tn = t1
        rn = ra

    rn = np.interp(ti,tn,rn)
    
    tn = rn
    print len(ri)
    error = ri-rn
    terror = ti
    return terror, error
       



if __name__ == "__main__":

    filename = '/draco/u/daguiam/codes/nn_ffestimation/ricglibrary/ricg/processing/tmp/'
    filename = filename + 'ric_33520_ant1.mat'

    filename = 'data/ric_33520_ant1_nn.mat'
    filename = 'data/ric_33520_ant1_nnKalman.mat'

    print filename
    data = scipy.io.loadmat(filename)
    data = dict2obj(data)


    filename2 = '/afs/ipp/u/augd/rawfiles/RIF/3352/0/profiles/'
    filename2 = filename2 + 'ric_33520_ant1.mat'
    filename2 = 'data/ric_33520_ant1_simpleKalman.mat'
    print filename2
    data2 = scipy.io.loadmat(filename2)
    data2 = dict2obj(data2)

    #filter = data.dataValidated[0]
    #data = nn.select_data_filter(data,filter,verbose=True)

    valididx = np.where(data.dataValidated[0])[0]
    print 'filtering data'
    data.times = data.times[0,valididx]
    data.ff = data.ff[0,valididx]
    data.ff_estimated = data.ff_estimated[0,valididx]
    data.ra = data.ra[valididx]
    data.ne = data.ne[valididx]


    #filter = data2.dataValidated[0]
    #data2 = nn.select_data_filter(data2,filter,verbose=True)
    #data2.times=data2.times[0,np.where(data2.dataValidated[0])]

    valididx = np.where(data2.dataValidated[0])[0]

    print 'filtering data'
    data2.times = data2.times[0,valididx]
    data2.ff = data2.ff[0,valididx]
    data2.ff_estimated = data2.ff_estimated[0,valididx]
    data2.ra = data2.ra[valididx]
    data2.ne = data2.ne[valididx]
    

    #data2.times = data2.times[0]
    #data2.ff = data2.ff[0]
    #data2.ff_estimated = data2.ff_estimated[0]



    #scaling 


    data.ff /= 1e9
    data.ff_estimated /= 1e9
    data.ra *= 100
    data.ne /= 1e19



    data2.ff /= 1e9
    data2.ff_estimated /= 1e9
    data2.ra *= 100
    data2.ne /= 1e19
   
    
    nearray = np.linspace(1,15,3)/10.0
    print nearray
    print 'Plotting data'

    #data.ne = data.ne[0]
    #data.ra = data.ra[0]

    cornn = colors[2]
    cororig = colors[3]


    r2 = data2.ra
    r1 = data.ra
    ne1 = data.ne
    t2 = data2.times

    t1 = data.times
    ne2 = data.ne
    ta1,r1,ta2,r2 = extract_common_data(t1,r1,t2,r2)
    t1,ne1,t2,ne2 = extract_common_data(t1,ne1,t2,ne2)
    
    outidxs = meshNearestIndexes(ne1,nearray)
    ri1 = getMeshIdxValues(r1,outidxs)

    outidxs = meshNearestIndexes(ne2,nearray)
    ri2 = getMeshIdxValues(r2,outidxs)

    # remove nans
    #nanidx = 
    
    terror = t1
    error = ri1-ri2
    nerror = np.tile(nearray,(len(error),1))
    
    # Calcultes the average error along each bin

    nbins = 300
    e,bins = np.histogram(terror,bins=nbins)
    errmeanh = []
    errstdh = []
    for i in range(len(nearray)):
        errne = error[:,i]
        nearrayi = nearray[i]
        emean = nn.bin_func(t1,errne,bins,func=np.nanmean)
        estd =  nn.bin_func(t1,errne,bins,func=np.nanstd)

        errmeanh.append(emean)
        errstdh.append(estd)

        #print 'error hist', nearrayi,emean,estd
    
    tbins = bins
    twidths = np.diff(bins)
    tcentral = tbins[:-1]+twidths/2


    

    #terror, error  = ra_errors(t1,r1,t2,r2)


    #times2 = data2.times.reshape(data2.ra.shape)
    #times = data.times.reshape(data.ra.shape)

    #print times.shape
    #print data.ra.shape
    figdpi = 300
    fi = 0
    filenamepng = 'images/sofe2017_results_ne_%d.png'
    filenamepdf = 'images/sofe2017_results_ne_%d.pdf'
    f,axarr = plt.subplots(4,1,sharex=True,figsize=(5,5))



    plt.subplots_adjust(left=None, bottom=0.16, right=None, top=None, wspace=0.05, hspace=0.05)

    time = data.times

    timelims = [time.min(),time.max()]
    ax = axarr[0]

    ax.set_xlim(timelims)

    alphaval = 0.8
    alphaback = 0.3
    lw=1
    
    ax.plot(data2.times,data2.ff_estimated,alpha=alphaback,c=cororig)
    ax.plot(data.times,data.ff_estimated,alpha=alphaback,c=cornn)
    ax.plot(data2.times,data2.ff,label='Original ',c=cororig,lw=lw)
    ax.plot(data.times,data.ff,label='Neural network',c=cornn,lw=lw)

    ax.set_ylabel('FF [GHz]')

    ax.set_ylim([41.5,44.9])


    ax.legend(loc=1,fontsize='small')

    print 'Plotting Originalpcolormesh'
    ax = axarr[1]

    
    norm = matplotlib.colors.Normalize(vmin=0.1, vmax=1.5)
    clorig = ax.pcolormesh(data2.times,data2.ra.T,data2.ne.T,cmap='autumn',rasterized=True,norm=norm)





    #print terror,error
    #ax.plot(data2.times,ra2,alpha=alphaval)
    #ax.set_ylabel('Radius [cm]')

    ax.set_ylim([0,14])


    #cb = f.colorbar(clorig, ax=ax)


    ax = axarr[2]

    


    clnn = ax.pcolormesh(data.times,data.ra.T,data.ne.T,cmap='summer',rasterized=True,norm=norm)
    #ax.set_ylabel('Radius [cm]')

    #ax.plot(data.times,ra,alpha=alphaval)


    ax.set_ylim([0,14])


    ax = f.add_subplot(914, frameon=False,)
    plt.tick_params(labelcolor='none', top='off', bottom='off', left='off', right='off')
    ax.set_ylabel('Radius [cm]')

    ax = f.add_subplot(412, frameon=False)
    plt.tick_params(labelcolor='none', top='off', bottom='off', left='off', right='off')

    cb = f.colorbar(clorig, ax=ax)

    cb.ax.set_ylabel(r'$n_e [\times10^{19} m^{-3}]$',fontsize='small')

    ax = f.add_subplot(413, frameon=False)


    plt.tick_params(labelcolor='none', top='off', bottom='off', left='off', right='off')

    #plt.xlabel('Time [s]')

    cb = f.colorbar(clnn, ax=ax)
    #cb.ax.set_ylabel(r'$n_e [e^{19} m^{-3}]$',fontsize='small')
    cb.ax.set_ylabel(r'$n_e [\times10^{19} m^{-3}]$',fontsize='small')

    fi += 1;f.savefig(filenamepng%(fi),dpi=figdpi);f.savefig(filenamepdf%(fi),dpi=figdpi);print filenamepng%(fi)

    ax = f.add_subplot(414, frameon=False)
    plt.tick_params(labelcolor='none', top='off', bottom='off', left='off', right='off')
    plt.xlabel('Time [s]')


    fi += 1;f.savefig(filenamepng%(fi),dpi=figdpi);f.savefig(filenamepdf%(fi),dpi=figdpi);print filenamepng%(fi)

    cb = f.colorbar(clnn, ax=ax)
    #cb.ax.set_ylabel(r'$n_e [e^{19} m^{-3}]$',fontsize='small')
    cb.ax.set_ylabel(r'$n_e [\times10^{19} m^{-3}]$',fontsize='small')






    cm = plt.cm.get_cmap('summer')
    #cm_skip = [cm.colors[i] for i in range(len(cm.colors))]

    
    
    #f,axarr = plt.subplots(1,1,sharex=True,figsize=(5,4))

    ax = axarr[3]
    for i in range(len(nearray)):
        ne = nearray[i]
        #
        step = ne*10
        errmean = errmeanh[i]
        errstd = errstdh[i]
        #
        ax.plot(tcentral,errmean,alpha=alphaval,c=cm(norm(ne)))
        #ax.plot(tcentral,errmean+step)
        #ax.axhline(step)

    ax.set_ylabel('Error [cm]')

    #ax.set_ylim([-1.7,3.1])
    #    ax.set_xlim(timelims)
    #plt.xlabel('Time [s]')

    fi += 1;f.savefig(filenamepng%(fi),dpi=figdpi);f.savefig(filenamepdf%(fi),dpi=figdpi);print filenamepng%(fi)

    plt.show()
    
