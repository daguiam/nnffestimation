import scipy.io

import numpy as np

import matplotlib.pylab as plt

import libnn as nn
import matplotlib
from libnn import colors2 as colors
import argparse

from mpl_toolkits.axes_grid1 import make_axes_locatable

from ricg import get_diagSignal

class dict2obj():
    """ Translates a dictionary to an object """
    def __init__(self,d):
        self.__dict__ = d


def closest_index(array,value):
    """ Finds the index of the array element closest to value
    """
    array = np.array(array)
    return (np.abs(np.array(array)-value)).argmin()

def ra_errors(t1,ra1,t2,ra2):
    t1len = len(t1)
    t2len = len(t2)
    
    ti = t1
    ri = ra1
    tn = t2
    rn = ra2
    if t2len <t1len:
        ti = t2
        ri = ra2
        tn = t1
        rn = ra

    rn = np.interp(ti,tn,rn)
    
    tn = rn
    print len(ri)
    error = ri-rn
    terror = ti
    return terror, error


def extract_common_data(t1,r1,t2,r2):
    commonTime = t1
    commonTime = np.intersect1d(commonTime,t2)
    valididx = np.in1d(t1,commonTime)
    r1 = r1[valididx]
    t1 = t1[valididx]
    valididx = np.in1d(t2,commonTime)
    r2 = r2[valididx]
    t2 = t2[valididx]
    return t1,r1,t2,r2

     



# Receives a 2D array and a interpolation array
# returns the 2D indexes of the value closest to each of the interpolation array
def meshNearestIndexes(ne,nearray):
    
    arrayshape = (len(ne),len(nearray))
    

    outidxs = np.empty(arrayshape).astype(int)*np.nan#*(-1)
    
    idxsprev = np.ones(len(ne))*(-1)
    for neidx in range(len(nearray)):
        neval = nearray[neidx] 
        idxs = np.argmin(np.abs(ne-neval),axis=1);
        for i in range(len(idxs)):
            if idxs[i] ==len(ne[i,:])-1:
                continue
                                 
            if (idxs[i] != idxsprev[i]):# and (ne[i,idxs[i]]-neval >=0):
                #print 'idxs[i]',idxs[i]
                outidxs[i,neidx] = idxs[i]
            else:
                a=0
                #print 'Skipped at ',times[idxs[i]], ' ne ', neval
        idxsprev = idxs
    return outidxs#.astype(int)

def getMeshIdxValues(matrix,idxs):
    outmatrix = np.empty(np.shape(idxs))*np.nan
    for i in range(len(matrix)):
        idxvec = idxs[i]
        idxvec = idxvec[np.logical_not( np.isnan(idxvec))].astype(int)
        outmatrix[i,:len(idxvec)] = matrix[i,idxvec]
  
    return outmatrix
  

       
def ra_errors2d(t1,ra1,t2,ra2):
    t1len = len(t1)
    t2len = len(t2)
    
    ti = t1
    ri = ra1
    tn = t2
    rn = ra2
    if t2len <t1len:
        ti = t2
        ri = ra2
        tn = t1
        rn = ra

    rn = np.interp(ti,tn,rn)
    
    tn = rn
    print len(ri)
    error = ri-rn
    terror = ti
    return terror, error
       



if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Loads data, creates nn model, trains the data')
    parser.add_argument('-s','--shotnumber',help='batches',type=int,default=33841)
    args = parser.parse_args()
    shotnumber = args.shotnumber


    #filename = '/draco/u/daguiam/codes/nn_ffestimation/ricglibrary/ricg/processing/tmp/'
    #filename = filename + 'ric_33520_ant1.mat'

    #filename = 'data/ric_33520_ant1_nn.mat'
    filename = 'data/ric_%d_ant1_nnKalman.mat'%(shotnumber)

    print filename
    data = scipy.io.loadmat(filename)
    data = dict2obj(data)


    filename2 = '/afs/ipp/u/augd/rawfiles/RIF/3352/0/profiles/'
    filename2 = filename2 + 'ric_33520_ant1.mat'
    filename2 = 'data/ric_%d_ant1_simpleKalman.mat'%(shotnumber)
    print filename2
    data2 = scipy.io.loadmat(filename2)
    data2 = dict2obj(data2)

    #filter = data.dataValidated[0]
    #data = nn.select_data_filter(data,filter,verbose=True)

    valididx = np.where(data.dataValidated[0])[0]
    print 'filtering data'
    data.times = data.times[0,valididx]
    data.ff = data.ff[0,valididx]
    data.ff_estimated = data.ff_estimated[0,valididx]
    data.fce_antenna = data.fce_antenna[0,valididx]
    data.fce_sep = data.fce_sep[0,valididx]
    data.ra = data.ra[valididx]
    data.ne = data.ne[valididx]


    #filter = data2.dataValidated[0]
    #data2 = nn.select_data_filter(data2,filter,verbose=True)
    #data2.times=data2.times[0,np.where(data2.dataValidated[0])]

    valididx = np.where(data2.dataValidated[0])[0]

    print 'filtering data'
    data2.times = data2.times[0,valididx]
    data2.ff = data2.ff[0,valididx]
    data2.ff_estimated = data2.ff_estimated[0,valididx]
    data2.ra = data2.ra[valididx]
    data2.ne = data2.ne[valididx]
    

    #data2.times = data2.times[0]
    #data2.ff = data2.ff[0]
    #data2.ff_estimated = data2.ff_estimated[0]



    #scaling 


    data.ff /= 1e9
    data.ff_estimated /= 1e9
    data.ra *= 100
    data.ne /= 1e19
    data.fce_antenna /= 1e9
    data.fce_sep /= 1e9


    data2.ff /= 1e9
    data2.ff_estimated /= 1e9
    data2.ra *= 100
    data2.ne /= 1e19
   


    h1 = get_diagSignal('DCN','H-1',shotnumber)
    h1.data /= 1e19

    raus = get_diagSignal('FPG','Raus',shotnumber)
    print raus.data,raus.time
    
    maxne = np.max([data.ne.max(),data2.ne.max()])
    if maxne <0.8:
        maxne = 0.8
    
    nearray = np.linspace(0.1,maxne,5)
    
    print nearray
    print 'Plotting data'

    #data.ne = data.ne[0]
    #data.ra = data.ra[0]

    corh1 = 'C1'
    corlc=colors[1]
    cornn = colors[2]
    #cornn = 'DarkGreen'
    cororig = colors[3]
    cororig = 'tomato'
    #cororig = 'DarkRed'
    corfce = 'k'
    corsep = colors[0]

    cbfraction = 0.9
    cbshrink = 0.8
    cbpad = 0.25
    cbloc = 'left'

    r2 = data2.ra
    r1 = data.ra
    ne1 = data.ne
    t2 = data2.times

    t1 = data.times
    ne2 = data.ne
    ta1,r1,ta2,r2 = extract_common_data(t1,r1,t2,r2)
    t1,ne1,t2,ne2 = extract_common_data(t1,ne1,t2,ne2)
    
    outidxs = meshNearestIndexes(ne1,nearray)
    ri1 = getMeshIdxValues(r1,outidxs)

    outidxs = meshNearestIndexes(ne2,nearray)
    ri2 = getMeshIdxValues(r2,outidxs)

    # remove nans
    #nanidx = 
    
    terror = t1
    error = ri1-ri2
    nerror = np.tile(nearray,(len(error),1))
    
    # Calcultes the average error along each bin

    nbins = 300
    e,bins = np.histogram(terror,bins=nbins)
    errmeanh = []
    errstdh = []
    for i in range(len(nearray)):
        errne = error[:,i]
        nearrayi = nearray[i]
        emean = nn.bin_func(t1,errne,bins,func=np.nanmean)
        estd =  nn.bin_func(t1,errne,bins,func=np.nanstd)

        errmeanh.append(emean)
        errstdh.append(estd)

        #print 'error hist', nearrayi,emean,estd
    
    tbins = bins
    twidths = np.diff(bins)
    tcentral = tbins[:-1]+twidths/2


    

    #terror, error  = ra_errors(t1,r1,t2,r2)


    #times2 = data2.times.reshape(data2.ra.shape)
    #times = data.times.reshape(data.ra.shape)

    #print times.shape
    #print data.ra.shape
    figdpi = 300
    fi = 0
    filenamepng = 'images/sofe2017_results_ne_%d_%s.png'%(shotnumber,'%d')
    filenamepdf = 'images/sofe2017_results_ne_%d_%s.pdf'%(shotnumber,'%d')
    f,axarr = plt.subplots(4,1,sharex=True,figsize=(5,6))



    plt.subplots_adjust(left=None, bottom=0.08, right=0.85, top=0.95, wspace=0.05, hspace=0.05)

    time = data.times

    timelims = [time.min(),time.max()]
    if shotnumber == 33841:
        timelims = [0.8,5.5]


    hl =[]
    
    

    alphaval = 0.8
    alphaback = 0.6
    lw=1

    alphashade = 0.4

    ax = axarr[0]

    h = ax.plot(h1.time,h1.data,label=r'Core $n_e$',c=corh1)
    hl.append(h)
    ax.set_ylabel(r'$n_e$ $[\times10^{19} m^{-3}]$')
    ax.set_ylim([0,10])
    ax.axvspan(1.18,4.18,alpha=alphashade,facecolor=corlc)
    newax = ax.twinx()
    #newax.invert_yaxis()
    rlimiter = 2.206
    
    rsep = (rlimiter-raus.data)*100
    rsep = raus.data*100
    newax.invert_yaxis()

    h = newax.plot(raus.time,rsep,label=r'$R_{sep}$',lw=0.8,alpha=alphaval,c=corsep,zorder=-10)
    hl.append(h)
    newax.set_xlim(timelims)
   

    #newax.set_ylim([224,212])
    newax.set_ylabel(r'$R_{sep}$ [cm]')

    newax.set_ylim([220,208])


    ax.set_xlim(timelims)

    ax = axarr[1]
    ax.plot(data2.times,data2.ff_estimated,alpha=alphaback,c=cororig,lw=0.08)
    ax.plot(data.times,data.ff_estimated,alpha=alphaback,c=cornn,lw=0.08)
    h = ax.plot(data.times,data.fce_antenna,label=r'$fce_{wall}$',c=corfce,lw=lw,alpha=alphaval)    
    #h = ax.plot(data.times,data.fce_sep,label=r'$fce_{sep}$',c=corsep,lw=lw,alpha=alphaval)    
    hl.append(h)

    h = ax.plot(data2.times,data2.ff,label=r'$FF_{ampfilt}$ ',c=colors[3],lw=lw)
    hl.append(h)
    h = ax.plot(data.times,data.ff,label=r'$FF_{NN}$',c='DarkGreen',lw=lw)
    hl.append(h)

    ax.set_ylabel(r'$f$ [GHz]')
    
    ffmin = data.fce_antenna.min()
    ffmax = np.mean(data2.ff_estimated)+2.5*np.std(data2.ff_estimated)
    
    ax.set_ylim([ffmin,ffmax])



    handles, labels = ax.get_legend_handles_labels()

    if shotnumber == 33520:
        #ax.set_ylim([41.5,44.9])
        pass
    if shotnumber == 33841:
        ax.set_ylim([53.1,57.9])
        pass

    #ax.legend(fontsize='small')



    handles1, labels1 = newax.get_legend_handles_labels()

    #ax.legend(handles, labels)


    ax = newax
    handles = [i[0] for i in hl]
    #print handles,labels
    l = ax.legend(handles=handles,fontsize='small',loc=1)
    l.set_zorder(20)

    print 'Plotting Originalpcolormesh'
    ax = axarr[2]
    #plt.show()
    
    norm = matplotlib.colors.Normalize(vmin=0.1, vmax=maxne)
    clorig = ax.pcolormesh(data2.times,data2.ra.T,data2.ne.T,cmap='autumn',rasterized=True,norm=norm)





    #print terror,error
    #ax.plot(data2.times,ra2,alpha=alphaval)
    #ax.set_ylabel('Radius [cm]')
    rlims = [0,12]
    ax.set_ylim(rlims)

    ax.text(.65,.85,r'$n_e$ profile with ampfilt algorithm',
        horizontalalignment='center',
        transform=ax.transAxes,fontsize='small')
    #cb = f.colorbar(clorig, ax=ax)


    ax = axarr[3]

    


    clnn = ax.pcolormesh(data.times,data.ra.T,data.ne.T,cmap='summer',rasterized=True,norm=norm)
    #ax.set_ylabel('Radius [cm]')

    #ax.plot(data.times,ra,alpha=alphaval)
    ax.set_ylim(rlims)


    ax.text(.65,.85,r'$n_e$ profile with Neural Network algorithm',
        horizontalalignment='center',
        transform=ax.transAxes,fontsize='small')

    ax = f.add_subplot(917, frameon=False,)
    plt.tick_params(labelcolor='none', top='off', bottom='off', left='off', right='off')
    ax.set_ylabel('Antenna distance [cm]',labelpad=7)

    ax = f.add_subplot(4,10,30, frameon=False)
    plt.tick_params(labelcolor='none', top='off', bottom='off', left='off', right='off')
    box = ax.get_position()
    ax.set_position([box.x0*1.05, box.y0, box.width, box.height])

    divider = make_axes_locatable(ax)
    cax = divider.append_axes('right', size='5%', pad=0.05,frameon=False)
    plt.tick_params(labelcolor='none', top='off', bottom='off', left='off', right='off')

    cb = f.colorbar(clorig, ax=cax,shrink=cbshrink,pad=30)

    cb.ax.set_ylabel(r'$n_e$ $[\times10^{19} m^{-3}]$',fontsize='small')

    ax = f.add_subplot(4,10,40, frameon=False)
    plt.tick_params(labelcolor='none', top='off', bottom='off', left='off', right='off')
    box = ax.get_position()
    ax.set_position([box.x0*1.05, box.y0, box.width, box.height])

    divider = make_axes_locatable(ax)
    cax = divider.append_axes('right', size='5%', pad=0.05,frameon=False)
    plt.tick_params(labelcolor='none', top='off', bottom='off', left='off', right='off')

    cb = f.colorbar(clnn, ax=cax,shrink=cbshrink,pad=-0.1)
    #cb.ax.set_ylabel(r'$n_e [e^{19} m^{-3}]$',fontsize='small')
    cb.ax.set_ylabel(r'$n_e$ $[\times10^{19} m^{-3}]$',fontsize='small')

    fi += 1;f.savefig(filenamepng%(fi),dpi=figdpi);f.savefig(filenamepdf%(fi),dpi=figdpi);print filenamepng%(fi)

    if 0 :
        ax = f.add_subplot(5,10,50, frameon=False)
        plt.tick_params(labelcolor='none', top='off', bottom='off', left='off', right='off')
        box = ax.get_position()
        ax.set_position([box.x0*1.05, box.y0, box.width, box.height])

        divider = make_axes_locatable(ax)
        cax = divider.append_axes('right', size='5%', pad=0.05,frameon=False)
        plt.tick_params(labelcolor='none', top='off', bottom='off', left='off', right='off')

        fi += 1;f.savefig(filenamepng%(fi),dpi=figdpi);f.savefig(filenamepdf%(fi),dpi=figdpi);print filenamepng%(fi)

        cb = f.colorbar(clnn, ax=cax,shrink=cbshrink,)
        #cb.ax.set_ylabel(r'$n_e [e^{19} m^{-3}]$',fontsize='small')
        cb.ax.set_ylabel(r'$n_e$ $[\times10^{19} m^{-3}]$',fontsize='small')


    ax = f.add_subplot(111, frameon=False)
    plt.tick_params(labelcolor='none', top='off', bottom='off', left='off', right='off')

    plt.xlabel('Time [s]')



    cm = plt.cm.get_cmap('summer')
    #cm_skip = [cm.colors[i] for i in range(len(cm.colors))]

    
    
    #f,axarr = plt.subplots(1,1,sharex=True,figsize=(5,4))
    if 0:

        ax = axarr[4]
        for i in range(len(nearray)):
            ne = nearray[i]
            #
            step = ne*10
            errmean = errmeanh[i]
            errstd = errstdh[i]
            #
            ax.plot(tcentral,errmean,alpha=alphaval,c=cm(norm(ne)))
            #ax.plot(tcentral,errmean+step)
            #ax.axhline(step)

        ax.set_ylabel('Difference [cm]')
        ax.set_ylim([-2.1,2.3])
        ax.set_yticks([-2,-1,0,1,2])
        #ax.set
        #ax.set_ylim([-1.7,3.1])
        #    ax.set_xlim(timelims)
        #plt.xlabel('Time [s]')
        ax.grid()
    fi += 1;f.savefig(filenamepng%(fi),dpi=figdpi);f.savefig(filenamepdf%(fi),dpi=figdpi);print filenamepng%(fi)

    plt.show()
    
