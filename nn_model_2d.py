"""
Creates and tests the model for neural network FF estimation



"""


import keras
# Create first network with Keras
from keras.models import Sequential
from keras.layers import Dense
from keras.layers import Conv2D
from keras.models import load_model
import numpy as np
from scipy.stats import norm
import matplotlib.pylab as plt
import pickle







def load_data():

    trainfile = 'data/traindata.dat.npy'
    dataset = np.load(trainfile)

    trainfile = 'data/traindatapf.dat.npy'
    datapf = np.load(trainfile)

    trainfile = 'data/traindataff.dat.npy'
    dataffidx = np.load(trainfile)

    pf = datapf

    # Real test value of ff
    testff = pf[dataffidx]
    modelfile='modelFFeval_epochs%d.h5'%(epochs)


    ffspan = max(pf)-min(pf)
    singletestff = (testff-min(pf))/ffspan
    singletestff = (testff-40e9)/28e9

    X = dataset.reshape(len(dataset[:,0,0]),-1)
    data_x = X
    data_y = singletestff



    return data_x,data_y



def load_data2d():

    trainfile = 'data/traindata.dat.npy'
    dataset = np.load(trainfile)

    trainfile = 'data/traindatapf.dat.npy'
    datapf = np.load(trainfile)

    trainfile = 'data/traindataff.dat.npy'
    dataffidx = np.load(trainfile)

    pf = datapf

    # Real test value of ff
    testff = pf[dataffidx]
    modelfile='modelFFeval_epochs%d.h5'%(epochs)


    ffspan = max(pf)-min(pf)
    singletestff = (testff-min(pf))/ffspan
    singletestff = (testff-40e9)/28e9

    X = dataset.reshape(len(dataset[:,0,0]),-1)


    data_x = dataset
    data_y = singletestff



    return data_x,data_y

def split_train_test_data(data_x, data_y, split=0.1):
    """ Splits the given data_x into train and test data for SPLIT
    """



    totalpoints = len(data_x)

    splitpoints = int((1-split)*totalpoints)
    testsplit = int((split)*totalpoints)

    indexes = np.arange(totalpoints)
    randindexes = np.random.randint(0,totalpoints,testsplit)
    splitmask = np.in1d(indexes, randindexes)
    print splitmask
    nonsplitmask = np.logical_not(splitmask)
    print nonsplitmask
    print indexes

    #print 'Split at ',splitpoints


    #return data_x[:splitpoints],data_y[:splitpoints], data_x[splitpoints:], data_y[splitpoints:]
    if data_x.ndim >1:
        return data_x[nonsplitmask,:],data_y[nonsplitmask], data_x[splitmask,:], data_y[splitmask]
    else:
        return data_x[nonsplitmask],data_y[nonsplitmask], data_x[splitmask], data_y[splitmask]







def create_model(sizeH1,sizeH2,sizeH3):



    model = Sequential()
    model.add(Dense(sizeH1, input_dim=sizeH1, init='uniform', activation='relu'))
    model.add(Dense(sizeH2, init='uniform', activation='relu'))
    #model.add(Dense(sizeH2, init='uniform', activation='relu'))
    model.add(Dense(sizeH3, init='uniform', activation='sigmoid'))

    #model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])
    #model.compile(optimizer='rmsprop',
    #          loss='binary_crossentropy',
    #          metrics=['accuracy'])


    #model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])
    model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy','mean_squared_error'])


    return model



def create_model_2d(sizeH1,sizeH2,sizeH3):



    model = Sequential()
    #model.add(Conv2D(sizeH2, sizeH1, activation='relu', input_shape=sizeH1))
    #model.add(Dense(sizeH2, input_dim=sizeH1, init='uniform', activation='relu'))
    model.add(Dense(sizeH2, input_shape=sizeH1, init='uniform', activation='relu'))
    model.add(Dense(sizeH2, init='uniform', activation='relu'))
    #model.add(Dense(sizeH2, init='uniform', activation='relu'))
    model.add(Dense(sizeH3, init='uniform', activation='sigmoid'))

    #model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])
    #model.compile(optimizer='rmsprop',
    #          loss='binary_crossentropy',
    #          metrics=['accuracy'])


    #model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])
    model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy','mean_squared_error'])


    return model


def do_1d():


    data_x,data_y = load_data()

    train_x, train_y, test_x, test_y = split_train_test_data(data_x, data_y)


    print len(train_x), len(test_y)

    sizeH1 = len(train_x[0])
    sizeH3 = 1#len(train_y[0])
    sizeH2 = sizeH1

    model = create_model(sizeH1,sizeH2,sizeH3)


    model.save(modelfile)

    del model
    model = load_model(modelfile)

    history = model.fit(train_x, train_y,  epochs=epochs,batch_size=batches,verbose=1,shuffle=True,validation_split=0.100)


    print history.history.keys()


    loss= model.evaluate(test_x, test_y, batch_size=32, verbose=1, sample_weight=None)
    #print loss

    pred_train = model.predict(train_x)
    pred_test = model.predict(test_x)


    plt.plot(data_y,data_y,c='c')
    plt.scatter(train_y,pred_train,c='b')
    plt.scatter(test_y,pred_test,c='r')
    plt.scatter(data_y,data_y,c='c')

    #plt.plot(data_x[100])
    #plt.plot(data_y[100])
    #plt.plot(pred_y,'r')

    print len(train_y),len(test_y)

    plt.figure()
    # summarize history for accuracy
    #plt.plot(history.history['acc'])
    #plt.plot(history.history['val_acc'])
    plt.plot(history.history['mean_squared_error'])
    plt.plot(history.history['val_mean_squared_error'])
    plt.title('model mean squared')
    plt.ylabel('accuracy')
    plt.xlabel('epoch')
    plt.legend(['train', 'test'], loc='upper left')
    plt.figure()
    # summarize history for loss
    plt.plot(history.history['loss'])
    plt.plot(history.history['val_loss'])
    plt.title('model loss')
    plt.ylabel('loss')
    plt.xlabel('epoch')
    plt.legend(['train', 'test'], loc='upper left')
    plt.show()

    plt.show()



if __name__ == "__main__":


    epochs = 100
    batches = 32
    batches = 32
    np.random.seed(12)


    modelfile = 'testmodel.h5'
    # fix random seed for reproducibility
    seed = 7
    np.random.seed(seed)
    # load pima indians datase


    do_1d()



    data_x,data_y = load_data2d()

    print data_x.shape


    train_x, train_y, test_x, test_y = split_train_test_data(data_x, data_y)

    print len(train_x), len(test_y)

    sizeH1 = train_x.shape

    sizeH1 = (sizeH1[1],sizeH1[2])
    print 'sizeh1',sizeH1
    sizeH3 = 1#len(train_y[0])
    sizeH2 = 256

    model = create_model_2d(sizeH1,sizeH2,sizeH3)


    model.save(modelfile)

    del model
    model = load_model(modelfile)

    print train_x.shape
    history = model.fit(train_x[None][:,:], train_y,  epochs=epochs,batch_size=batches,verbose=1,shuffle=True,validation_split=0.100)


    print history.history.keys()


    loss= model.evaluate(test_x, test_y, batch_size=32, verbose=1, sample_weight=None)
    #print loss

    pred_train = model.predict(train_x)
    pred_test = model.predict(test_x)


    plt.plot(data_y,data_y,c='c')
    plt.scatter(train_y,pred_train,c='b')
    plt.scatter(test_y,pred_test,c='r')
    plt.scatter(data_y,data_y,c='c')

    #plt.plot(data_x[100])
    #plt.plot(data_y[100])
    #plt.plot(pred_y,'r')

    print len(train_y),len(test_y)

    plt.figure()
    # summarize history for accuracy
    #plt.plot(history.history['acc'])
    #plt.plot(history.history['val_acc'])
    plt.plot(history.history['mean_squared_error'])
    plt.plot(history.history['val_mean_squared_error'])
    plt.title('model mean squared')
    plt.ylabel('accuracy')
    plt.xlabel('epoch')
    plt.legend(['train', 'test'], loc='upper left')
    plt.figure()
    # summarize history for loss
    plt.plot(history.history['loss'])
    plt.plot(history.history['val_loss'])
    plt.title('model loss')
    plt.ylabel('loss')
    plt.xlabel('epoch')
    plt.legend(['train', 'test'], loc='upper left')
    plt.show()

    plt.show()
