import matplotlib.pylab as plt
import ricglib
import numpy as np
from test_cases import all_ricg_shots as ricgshots
from test_cases import test_cases
from test_cases import test_cases_extra
import argparse
import pickle
from scipy import constants as konst

test_cases = test_cases + test_cases_extra
parser = argparse.ArgumentParser(description='Plots multiple relevant data figures of all shotnumbers')

parser.add_argument('-nr','--number',help='Number of tests to plot',required=False,default=0)
parser.add_argument('-new','--new',help='Get new data from journal instead of trying to read locally',required=False,action='store_true',default=False)
parser.add_argument('-shot','--shot',help='Plot shotnumbers annotations',required=False,action='store_true',default=False)


args = parser.parse_args()

nrplot= int(args.number)
getnew= args.new
plotshot= args.shot


if nrplot == 0:
    nrplot = -1


testshotnumbers = []
for test in test_cases:
    testshotnumbers.append(test[0])

filepickle = 'journaldata.pickle'



try:
    data = pickle.load(open(filepickle,'rb'))

except:
    print ' No data file present'
    getnew = True
    
if getnew:
    print ' Getting new journal data'
    diagname = 'JOU'
    shotnumbers = []
    bt = []
    ipa = []
    ne = []
    q95 = []
    kappa = []
    delRob = []
    delRunt = []
    for shotnumber in ricgshots[:nrplot]:
    
        #bt = ricglib.get_diagParameter(diagname,  'MAGNETIC','BT', shotnumber).data
        #ipa = ricglib.get_diagParameter(diagname,  'PLASMA','IP', shotnumber).data
        #ne = ricglib.get_diagParameter(diagname,  'PLASMA','NE', shotnumber).data
        #q95 = ricglib.get_diagParameter(diagname,  'PLASMA','Q95', shotnumber).data
        #kappa = ricglib.get_diagParameter(diagname,  'PLASMA','kappa', shotnumber).data
        #delRob = ricglib.get_diagParameter(diagname,  'PLASMA','delRob', shotnumber).data
        #delRunt = ricglib.get_diagParameter(diagname,  'PLASMA','delRunt', shotnumber).data
        shotnumbers.append(shotnumber)
        bt.append(ricglib.get_diagParameter(diagname,  'MAGNETIC','BT', shotnumber).data)
        ipa.append(ricglib.get_diagParameter(diagname,  'PLASMA','IP', shotnumber).data)
        ne.append(ricglib.get_diagParameter(diagname,  'PLASMA','NE', shotnumber).data)
        q95.append(ricglib.get_diagParameter(diagname,  'PLASMA','Q95', shotnumber).data)
        kappa.append(ricglib.get_diagParameter(diagname,  'PLASMA','kappa', shotnumber).data)
        delRob.append(ricglib.get_diagParameter(diagname,  'PLASMA','delRob', shotnumber).data)
        delRunt.append(ricglib.get_diagParameter(diagname,  'PLASMA','delRunt', shotnumber).data)
    
    bt = np.array(bt)
    ne = np.array(ne)
    ipa = np.array(ipa)
    q95 = np.array(q95)
    kappa = np.array(kappa)
    delRob = np.array(delRob)
    delRunt = np.array(delRunt)
    
    shotnumbers = np.array(shotnumbers)
    
    
    
    output_dictionary = {
        'shotnumbers':shotnumbers,
        'bt':bt,
        'ne':ne,
        'ipa':ipa,
        'q95':q95,
        'kappa':kappa,
        'delRob':delRob,
        'delRunt':delRunt
    }
    pickle.dump(output_dictionary,open(filepickle,'wb'))
    data = pickle.load(open(filepickle,'rb'))

data = ricglib.dict2obj(data)
data.ne = data.ne/1e19

def calc_ne(fpe2):
    return fpe2*4*konst.pi*konst.epsilon_0*konst.m_e/(konst.e*konst.e)

# Calculates the electron cyclotron frequency from a given Magnetic Field
def calc_fce(bfield):
    return np.abs(konst.e*bfield/(2*konst.pi*konst.m_e))

# Calculates the magnetic field from a given electron cyclotron frequency
def calc_bfield(fce):
    return (fce*2*konst.pi*konst.m_e)/(konst.e)

minf=40e9
maxf=64e9
rgeo = 1.65
rant = 2.15
minb = ricglib.calc_bfield(minf)
maxb = ricglib.calc_bfield(maxf)

k1a = 4*np.power(konst.pi,2) * konst.epsilon_0  / np.power(konst.e,2)



minb = minb*rant/rgeo
maxb = maxb*rant/rgeo



bvec = np.linspace(-3,3,200)
fcevec = calc_fce(bvec*rgeo/rant)
#print fcevec
#Calculating minimum measureable density
fpe2min = np.power(minf-fcevec/2,2)-np.power(fcevec,2)/4
fpe2min[np.where(fpe2min<0)]=np.NaN
nemin = calc_ne(fpe2min)/1e19


#Calculating maximum measureable density
fpe2min = np.power(minf+fcevec/2,2)-np.power(fcevec,2)/4
fpe2min[np.where(fpe2min<0)]=np.NaN
nemin = calc_ne(fpe2min)/1e19

fpe2max = np.power(maxf+fcevec/2,2)-np.power(fcevec,2)/4
nemax = calc_ne(fpe2max)/1e19

# calculate there is a measureable lower cutoff
fpe2lc = np.power(minf-fcevec/2,2)-np.power(fcevec,2)/4
nelc = calc_ne(fpe2lc)/1e19

fc = minf
def calc_ne_uc(fc,fce):
    ne= k1a * konst.m_e* fc*(fc - fce)
    print ne
    #ne[np.where(ne<0)] = 0
    return ne

def calc_ne_lc(fc,fce):
    ne= k1a * konst.m_e* fc*(fc + fce)
    ne[np.where(ne<0)] = 0
    return ne

fcevec[np.where(fcevec<=minf)] = np.nan
fcevec[np.where(fcevec>=maxf)] = np.nan


neuc = calc_ne_uc(minf,fcevec)/1e19
nelc = calc_ne_lc(minf,fcevec)/1e19
nelcmax = calc_ne_lc(fcevec,fcevec)/1e19

#print neuc
#print nelc


fig = plt.figure()
ax = fig.add_subplot(111)



testedshotsidx = np.in1d(data.shotnumber,testshotnumbers)
nottestedshotsidx = np.logical_not(testedshotsidx)
if plotshot:
    for i,xy in enumerate(zip(data.bt,data.ne)):
        pass
        if testedshotsidx[i]:
            ax.annotate('%d'%(data.shotnumber[i]),xy =xy,textcoords='data',fontsize='x-small',color='Gray')


plt.axvline(x=minb, ymin=0, ymax=1,color='b',linestyle='--',linewidth=0.5,label='minb')
plt.axvline(x=-minb, ymin=0, ymax=1,color='b',linestyle='--',linewidth=0.5,label='minb')
plt.axvline(x=maxb, ymin=0, ymax=1,color='b',linestyle='--',linewidth=0.5,label='maxb')
plt.axvline(x=-maxb, ymin=0, ymax=1,color='b',linestyle='--',linewidth=0.5,label='maxb')


plt.scatter(data.bt[nottestedshotsidx],data.ne[nottestedshotsidx],c='b',edgecolor='b',s=2)

plt.scatter(data.bt[testedshotsidx],data.ne[testedshotsidx],c='r',edgecolor='r',s=5)

#plt.plot(bvec,nemin,label='nemin')
#plt.plot(bvec,nemax,label='nemax')
plt.plot(bvec,nelc,'g',label='nelc')
plt.plot(bvec,nelcmax,'g--',label='nelcmax')

plt.plot(bvec,neuc,'r',label='neuc')

plt.xlabel(r'B$_0$ [T]')
plt.ylabel(r'Core ne [$\times10^{19}$m$^{-3}$]')

#print testshotnumbers
plt.grid()
plt.savefig('journalplot.pdf',dpi=150)
plt.show()


