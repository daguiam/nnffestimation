#!/usr/bin/env python


# Create first network with Keras
from keras.models import Sequential
from keras.layers import Dense
from keras.models import load_model

import matplotlib.pylab as plt


import sys

from scipy import constants as konst
from scipy import signal
from scipy.stats import norm
import math
import pickle

import numpy as np

from common import *

doplot =0

def ff_rule1( fb):
        return fb/max(fb)

def ff_rule2(pf, fb, amp):

        continuity = np.append([0],np.diff(fb))

        #continuity = continuity/2+0.5
        continuity = np.abs(continuity)
        #continuity = 1-continuity
        continuity = continuity/np.max(continuity)

        return continuity




def ff_rightmean(x):

        meanamp = np.zeros(len(x))
        meanampi = np.zeros(len(x))
        for i in range(len(amp)):
            meanamp[i] = np.nanmean(x[i:])
            meanampi[i] = np.nanmean(x[:i])
            #print meanamp[i],meanampi[i]
        meanamp = np.array(meanamp)
        meanampi = np.array(meanampi)
        #meanamp = np.append([0],np.diff(meanamp))

        meanamp = meanamp-meanampi
        meanamp = np.nan_to_num(meanamp)
        dist = np.max(meanamp)-np.min(meanamp)

        meanamp = meanamp-np.min(meanamp)
        meanamp = meanamp/dist

        return meanamp


def ff_rule7(pf, fb, amp):

        amp = 1-amp
        return amp

def ff_decision(amp,continuity,meanamp,meanfb=0):

        w1 = 1
        wp1 = 0
        w2 = 1
        w3 = 3
        w4 = 1
        wp4 = 0

        decision =  continuity*w2 + meanamp*w3+(meanfb*w4+wp4) * (amp*w1+wp1)#+(1-amp)
        decision = decision/np.max(np.abs(decision))
        return decision

def ff_rulefce(pf,fce):

    fceidx = get_arrayValueIdx(pf,fce)
    out = np.zeros(len(pf))
    out[fceidx:] = 1
    return out


def ff_decision2(amp,continuity,meanamp,meanfb=0):

        w1 = 0
        w2 = 0
        w3 = 1
        w4 = 1

        decision = meanfb*w4*(1-continuity)
        decision = decision/np.max(np.abs(decision))
        return decision




def ff_score(rules,weights):
    score = 0
    for i in range(len(weights)):
        r = np.array(rules[i])
        w = weights[i]

        out = r*w
        score = score+out

    maxscore = np.max(np.abs(score))
    if maxscore !=0:
        score = score/maxscore
    return score


cutoff =0.07





modelfile='modelFFeval.h5'
modelfile='modelFFeval_epochs24.h5'
#modelfile='modelFFeval_epochs23.h5'

model = load_model(modelfile)

output_dir = './'
filepickle = output_dir+'testsweeps_total.pickle'
filepickle = output_dir+'nn_training_all.pickle'
#filepickle = output_dir+'testsweeps_lg.pickle'

#rules matrix is [shotnumber,antenna,time,[rule1[],rule[],rule3[],... ]

evalffmatrix = []
datashotnumber = []
datatimes =[]

outdata = []
outff = []
outpredff = []
outresult = []

print 'Loading FFs from stored picke'
loadedtestff = pickle.load(open(filepickle,'rb'))




total_tests = len(loadedtestff)

testmask = np.random.random_integers(total_tests,size=(20))

testmask = range(total_tests)


#maskneg = range(total_tests)
#maskpos = np.delete(maskneg,testmask)
#total_tests = 20
#for testi in range(total_tests):
for testi in testmask:    

    testdata = dict2obj(loadedtestff[testi])
    testdata.stft_Fs = testdata.stft_Fs.flatten()


    Fs = testdata.stft_Fs
    window = testdata.stft_window
    windowtype =testdata.stft_wintype
    padding =testdata.stft_padding
    step = testdata.stft_step
    times = testdata.times
    sigs = np.array(testdata.sigs)
    freqramps = np.array(testdata.freqramp)
    shotnumber = testdata.shotnumber
    antenna = testdata.antenna

    fce = testdata.fce

    time0 = times[-1]
    #print 'FF from test file',testdata.ffs
    #print testdata.sigs


    stftpersist = None
    for i in range(len(times)):

        sig = np.array(sigs[i,:])
        time = testdata.times
        freqramp = np.array(freqramps[i,:])

        pf,fb,stft = calc_stft(freqramp,sig,Fs=Fs,window=window,padding=padding,step=step,windowtype=windowtype)

        if stftpersist is None:
            stftpersist = stft
        else:
            stftpersist = stftpersist + stft


        stft = stftpersist
    stftamplitude,stftpeakfb  = calcSTFTAmplitude(pf,fb,stft)


    amp = stftamplitude
    peakfb = stftpeakfb
    pf = pf


    # Evaluate each ff based on the data collected by human operators:
    realffmean = np.mean(testdata.ffs)
    pfspan = 0.5#1.5e9


    normfb = ff_rule1(peakfb)
    #testX = np.array([np.array([amp,normfb,ff_rightmean(amp),ff_rightmean(normfb),ff_rulefce(pf,fce)]).flatten()])

    testX = np.array([np.array([amp,normfb,ff_rulefce(pf,fce)]).flatten()])

    #datamask = [0,1,4]
    #testX = testX[datamask,:]

    #print testX
    # Predict the ff
    predictions = model.predict(testX)

    #print predictions.shape

    predffmax = pf[np.argmax(predictions)]




    striter = '%d: #%d Antenna %d %0.4f s'%(testi,shotnumber,antenna,time0)

    striter = striter+' FF=['
    for ff in np.array(testdata.ffs):
        striter = striter +'%0.2f, '%(ff/1e9)
    striter = striter+']'
    striter = striter + '  Pred: %0.2f Error: %0.3f'%(predffmax/1e9,(realffmean-predffmax)/1e9)

    print striter


    outff.append(realffmean)
    outpredff.append(predffmax)
    # Plot stuff
    #doplot = testi%10
    if doplot:
        plt.figure()
        plt.pcolormesh(pf,fb,stft)
        plt.axvline(x=realffmean,ymin=0,ymax=1,color='k',linestyle='--',label='FF',linewidth=2)
        plt.axvline(x=pf[np.argmax(predictions[:])],ymin=0,ymax=1,color='r',linestyle='-',label='prediction',linewidth=2)

        plt.xlim(freqramp[0],freqramp[-1])
        plt.ylim(0,11e6)
        plt.title(striter)
        plt.xlabel('ProbFreq [Hz]')
        plt.ylabel('BeatFreq [Hz]')

        plt.legend(fontsize='x-small')
        if testi%10 ==0:
            plt.show()  

outff = np.array(outff)
outpredff = np.array(outpredff)


sortidx = np.argsort(outff)

outff = outff[sortidx]
outpredff = outpredff[sortidx]

outfferror = outff-outpredff


fferror_mean = np.mean(outfferror)
fferror_std = np.std(outfferror)

print 'Error is:',fferror_mean, 'Std=',fferror_std
doplotfinal=1
if doplotfinal:
    plt.figure()
    plt.plot(outff/1e9, color='b',label='Measured FF')
    plt.plot(outpredff/1e9, color='r',label='Predicted FF')
    plt.title('Error Mean: %0.4f GHz , Std: %0.4f GHz'%(fferror_mean/1e9,fferror_std/1e9))
    plt.ylabel('ProbFreq [GHz]')
    plt.xlabel('Test case')
    plt.legend(fontsize='x-small')
    plt.savefig('outff.png',dpi=150)
    plt.show()





#outff = np.array(outff)
#outdata = np.array(outdata)
#print outdata.shape
