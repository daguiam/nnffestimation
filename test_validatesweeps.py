#!/usr/bin/env python
#import matplotlib.pyplot as plt
#import matplotlib.pylab as plt
import matplotlib.pylab as plt
from matplotlib import cm

import sys
from rif_library import *
from rif_augdlib import *

from scipy import constants as konst
from scipy import signal
from peakdetect import peakdet


from plot_params import paper_params
import pickle

import numpy as np



#strusage = 'Usage: ' + sys.argv[0] + ' <local|augd> <shotnumber> <antennanr#>'

#if len(sys.argv) < 4:
#    print strusage
#    exit(1)

#path = sys.argv[1];
#shotnumber = int(sys.argv[2])
#antenna  = int(sys.argv[3])
#sweep = int(sys.argv[4])
#shottime = float(sys.argv[4])
doplot=False
if len(sys.argv) > 1:
    
    #if int(sys.argv[2])==1:
    #    doplot=True
    doplot = int(sys.argv[1])
    
print 'plot=',doplot
def ff_rule1(pf, fb, amp):
        return fb/max(fb)

def ff_rule2(pf, fb, amp):
        
        continuity = np.append([0],np.diff(fb))
        
        #continuity = continuity/2+0.5
        continuity = np.abs(continuity)
        #continuity = 1-continuity
        continuity = continuity/np.max(continuity)
        
        return continuity




def ff_rightmean(x):
        
        meanamp = np.zeros(len(x))
        meanampi = np.zeros(len(x))
        for i in range(len(amp)):
            meanamp[i] = np.nanmean(x[i:])
            meanampi[i] = np.nanmean(x[:i])
            #print meanamp[i],meanampi[i]
        meanamp = np.array(meanamp)
        meanampi = np.array(meanampi)
        #meanamp = np.append([0],np.diff(meanamp))
        
        meanamp = meanamp-meanampi
        meanamp = np.nan_to_num(meanamp)
        dist = np.max(meanamp)-np.min(meanamp)

        meanamp = meanamp-np.min(meanamp)
        meanamp = meanamp/dist

        return meanamp
  
    
def ff_rule7(pf, fb, amp):
            
        amp = 1-amp
        return amp

def ff_decision(amp,continuity,meanamp,meanfb=0):
        
        w1 = 1
        wp1 = 0
        w2 = 1
        w3 = 3
        w4 = 1
        wp4 = 0

        decision =  continuity*w2 + meanamp*w3+(meanfb*w4+wp4) * (amp*w1+wp1)#+(1-amp)
        decision = decision/np.max(np.abs(decision))
        return decision
    
def ff_rulefce(pf,fce):
    
    fceidx = get_arrayValueIdx(pf,fce)
    out = np.zeros(len(pf))
    out[fceidx:] = 1
    return out


def ff_decision2(amp,continuity,meanamp,meanfb=0):
        
        w1 = 0
        w2 = 0
        w3 = 1
        w4 = 1

        decision = meanfb*w4*(1-continuity)
        decision = decision/np.max(np.abs(decision))
        return decision




def ff_score(rules,weights):
    score = 0
    for i in range(len(weights)):
        r = np.array(rules[i])
        w = weights[i]
        
        out = r*w
        score = score+out
    
    maxscore = np.max(np.abs(score))
    if maxscore !=0:
        score = score/maxscore
    return score


cutoff =0.07
def filtfilt_lowpass(sig,cutoff,order=5):
    nyq = 1;
    cutoffnorm = cutoff/nyq;
    b, a = signal.butter(order,cutoffnorm,btype='low');

    sig = signal.filtfilt(b,a,sig)

    return sig




def do_butter_lowpass_filter(x,cutoff,Fs,order=5):
    nyq = np.float(Fs)/2;
    cutoffnorm = cutoff/nyq;
    b, a = signal.butter(order,cutoffnorm,btype='low');
        #self.sig = np.fliplr(self.sig)
    out = signal.lfilter(b,a,x);
        #self.sig = np.fliplr(self.sig)
    return out

def get_diagSignal(diagname, signalname, shotnumber,experiment='AUGD'):
  try:
    diag = dd.shotfile(diagname, shotnumber,experiment=experiment);
  except:
    print 'dd error: There is no ' + signalname + ' in '+ diagname+' for #'+str(shotnumber);
    #exit();
    return 0;
  else:
    signal = diag(signalname);
    return signal;

def get_areabase(diagname, signalname, shotnumber,experiment='AUGD'):
  try:
    diag = dd.shotfile(diagname, shotnumber,experiment=experiment);
  except:
    print 'dd error: There is no ' + signalname + ' in '+ diagname+' for #'+str(shotnumber);
    #exit();
    return 0;
  else:
    signal = diag.getAreaBase(signalname);
    return signal;



# Caculates the stft amplitude along each bin
def calcSTFTAmplitude(pf,fb,stft):
    pflen = len(pf)
    zeroidx = get_arrayValueIdx(fb,0)
    maxstft = np.zeros(pflen);
    maxfb = np.zeros(pflen);
    sumstft = np.zeros(pflen);
    for i in range(0,pflen):
        idx = np.argmax(stft[zeroidx:,i]);
        maxstft[i] = stft[idx,i];
        maxstft[i] = np.linalg.norm(stft[zeroidx:,i])
        maxfb[i] =  fb[zeroidx+idx]
        sumstft[i] = np.sum(stft[:,i])
        
        #maxstft = sumstft
    stftamplitude = maxstft/max(maxstft);
    stftpeakfb = maxfb

    return stftamplitude,stftpeakfb

# Computes the spectrogram of a broadband reflectometry signal using a sliding window short-time fourier transform
# Adapted to python from ref_stft() by CFN-IST Portugal
def calc_stft(x,y,Fs=1,window=128,padding=4096,step=10,windowtype=1):
        

        Fs = Fs;
        freqramp = x
        signal = y;
        n = len(signal);
        nf = len(freqramp)
        N = padding;
                
        # Produces the wanted window
        def get_fft_window_type(window=128,windowtype=1):
    
            if (windowtype==1):
                WIN = np.ones(window);
            elif (windowtype==2):
                WIN = np.hamming(window);
            elif (windowtype==3):
                WIN = np.hanning(window);
            elif (windowtype==4):
                WIN = np.blackman(window);
        
            return WIN;
        
        WIN = get_fft_window_type(window,windowtype);

        if window < step:
            step = window

        if step==0:
            stft_x = int(math.floor(n/window));
            step=window;
        else:
            stft_x = int(math.floor( (n-window)/step) +1);
            
        #stft_x,step = calc_stftStep(window,step,n)

        stft_y = padding
        stft = np.zeros( ( stft_y,stft_x ));

        halfwin = window/2;
        pfindex = np.array([],dtype='int');
        fmax = Fs
        fb = np.linspace(0,fmax,stft_y);
        win_i = 0;
        # Calculates the fft in each window
        for i in range(0,stft_x):
            win_i =  i*step;
            win_f = win_i+window;
            #print win_i,win_f
            sigwindow =  signal[win_i:win_f];
            len_sig = len(sigwindow);
            if len_sig < window:
                auxwindow = get_fft_window_type(len_sig,windowtype);
                sigwindow = auxwindow * sigwindow;
            else:
                sigwindow = WIN * sigwindow;

            stft[:,i]=np.abs(np.fft.fft(sigwindow,padding))[0:stft_y];

            # Each probing frequency bin is at the middle of the FFT window
            pfindex = np.append(pfindex,win_i+halfwin);
        pf = freqramp[pfindex];

        # FFT shift
        for i in range(0,len(pf)):
            stft[:,i]=np.fft.fftshift(stft[:,i]);
        fb = fb-max(fb)/2;
        
        # Outputs
        stft = stft;
        pf = pf;
        fb = fb;

        return pf,fb,stft



# Uses the calculated stft and calculates its normalization along the bins
def calc_stftNorm(stft):
    bins = stft.shape[1];
    stftnorm = np.zeros(stft.shape);
        #self.stftorig = self.stft;
    for i in range(0,bins):
        maxbin = np.max(stft[:,i]);
        stftnorm[:,i] =stft[:,i]/maxbin;
    return stftnorm

# Implemented smooth moving average algorithm
def smooth(data,N=5):
    return np.convolve(np.ones(N)/N,data,'same');


def smooth2d(data,N):
    for i in range(len(data[0,:])):
        data[:,i] = smooth(data[:,i],N)
    return data

#shotnumber = 33214
#timeval = 3.999 # 33520
#shotnumber =33214
#timeval = 2.4763


experiment = 'AUGD'

nsmooth = 1

#starttime = 5.502
#stoptime=5.59

#plt.subplot(311)
nescale = 1e-19
pfscale = 1e-9
fbscale = 1e-6
dgscale = 1e9

#plt.rcParams.update(paper_params)


#fig=plt.figure(figsize=(3.37,2.5),dpi=100)



output_dir = 'testsweeps/'
filepickle = output_dir+'testsweeps_total.pickle'
filepickle = output_dir+'testsweeps_lg.pickle'
filepickle = output_dir+'nn_training.pickle'

#rules matrix is [shotnumber,antenna,time,[rule1[],rule[],rule3[],... ]


evalffmatrix = []
datashotnumber = []
datatimes =[]
   
print 'Loading FFs from stored picke'
loadedtestff = pickle.load(open(filepickle,'rb'))

for testi in range(len(loadedtestff)):

    testdata = dict2obj(loadedtestff[testi])


    #testdata.ffs = testdata.ffs.flatten()
    #testdata.times = testdata.times.flatten()
    testdata.stft_Fs = testdata.stft_Fs.flatten()
    #testdata.stft_window = testdata.stft_window.flatten()
    #testdata.stft_padding = testdata.stft_padding.flatten()
    #testdata.stft_step = testdata.stft_step.flatten()
    #testdata.stft_wintype = testdata.stft_wintype.flatten()
    
    #Fs = np.asscalar(testdata.stft_Fs)
    #window = np.asscalar(testdata.stft_window)
    #windowtype =np.asscalar( testdata.stft_wintype)
    #padding =np.asscalar( testdata.stft_padding)
    #step = np.asscalar(testdata.stft_step)
  
    Fs = testdata.stft_Fs
    window = testdata.stft_window
    windowtype =testdata.stft_wintype
    padding =testdata.stft_padding
    step = testdata.stft_step
    times = testdata.times
    sigs = np.array(testdata.sigs)
    freqramps = np.array(testdata.freqramp)
    shotnumber = testdata.shotnumber
    antenna = testdata.antenna

    fce = testdata.fce

    time0 = times[-1]
    striter = '%d: #%d Antenna %d %0.4f s'%(testi,shotnumber,antenna,time0)

    striter = striter+' FF=['
    for ff in np.array(testdata.ffs):
        striter = striter +'%0.2f, '%(ff/1e9)
    striter = striter+']'

    print striter
    #print 'FF from test file',testdata.ffs
    #print testdata.sigs

    
    stftpersist = None 
    for i in range(len(times)):
        
        sig = np.array(sigs[i,:])
        time = testdata.times
        freqramp = np.array(freqramps[i,:])
        
        pf,fb,stft = calc_stft(freqramp,sig,Fs=Fs,window=window,padding=padding,step=step,windowtype=windowtype)
        
        if stftpersist is None:
            stftpersist = stft
        else:
            stftpersist = stftpersist + stft
            
            
        stft = stftpersist
        stftamplitude,stftpeakfb = calcSTFTAmplitude(pf,fb,stft)
        
    
    if doplot:
        plt.figure()
        ax=plt.subplot(2,1,1)
        plt.pcolormesh(pf,fb,stftpersist)
    #plt.plot(pf,stftamplitude*10e6)
        plt.plot(pf,stftpeakfb,color='Green',linewidth=2)

        for ff in testdata.ffs:
            plt.axvline(x=ff, ymin=0, ymax=1,color='r',linestyle='--',linewidth=2)

    amp = stftamplitude
    fb = stftpeakfb
    pf = pf

    
    fb = ff_rule1(pf,fb,amp)
    
    continuity = ff_rule2(pf,fb,amp)

    smoothcontinuity = smooth(continuity,5)
    #smoothcontinuity = smooth(continuity,window)
    #smoothcontinuity = smoothcontinuity/np.max(smoothcontinuity)


    #meanamp = ff_rule3(pf,fb,amp)
    meanamp = ff_rightmean(amp)
    meanfb = 1-ff_rightmean(fb)
    
    ampi = ff_rule7(pf,fb,amp)

    #r1 = fb
    r2 = smoothcontinuity
    r3 = meanamp
    r4 = meanfb
    r5 = ampi
    r1 = ff_rulefce(pf,fce)
    
    w1 = 1
    w2 = 1
    w3 = 1
    w4 = 1
    w5 = 1


    decision = ff_decision(amp,smoothcontinuity,meanamp,meanfb)
    decision2 = ff_decision2(ampi,smoothcontinuity,meanamp,meanfb)

    rules = [r1,r2,r3,r4,r5]
    weights = [w1,w2,w3,w4,w5]
    score = ff_score(rules,weights)
    
    decision = score
  

    
    #decision =  smoothcontinuity*w2 + meanamp*w3+(meanfb*w4+wp4) * (amp*w1+wp1)#+(1-amp)
    #decision = decision/np.max(np.abs(decision))

    pfmax1 = pf[np.argmax(decision)]
    pfmax2 = pf[np.argmax(decision2)]

    strtitle= "Shot #"+str(shotnumber)+" Antenna %d "%antenna+" Time %0.4f s"%time0
    print strtitle

    if doplot:
        plt.axvline(x=pfmax1, ymin=0, ymax=1,color='k',linestyle='--',linewidth=2,label='d1')


        plt.axvline(x=pfmax2, ymin=0, ymax=1,color='Gray',linestyle='--',linewidth=2,label='d1')


        plt.axvline(x=fce, ymin=0, ymax=1,color='Cyan',linestyle='--',linewidth=2,label='fce')


        plt.title(strtitle)

        plt.ylabel('BeatFreq [Hz]')
        plt.ylim(np.array([-1,15])*1e6)

 #plt.figure()
        plt.subplot(2,1,2,sharex=ax)
    #plt.plot(pf,fb,'k',linewidth=2,label='fb values');
        plt.plot(pf,fb,'y',linewidth=2,label='1: Normfb');
        plt.plot(pf,amp,'r',linewidth=2,label='Amplitudes');
        plt.plot(pf,ampi,'DarkRed',linewidth=2,label='InvAmplitudes');
        
        plt.plot(pf,continuity,'g',linewidth=2,label='Continuity');
    
        plt.plot(pf,smoothcontinuity,'c',linewidth=2,label='SmoothContinuity');
        
        plt.plot(pf,meanamp,'Orange',linewidth=2,label='Right MeanAmp');
        plt.plot(pf,meanfb,'b',linewidth=2,label='Right MeanFb');
    #plt.plot(pf,meanamp1,'DarkOrange',linewidth=2,label='Left MeanAmp');

        plt.plot(pf,decision,'k',linewidth=2,label='Decision');
        plt.plot(pf,decision2,'Gray',linewidth=2,label='Decision');

    rules = [r1,r2,r3,r4,r5]
    wparams = [8,4,2,1,0]
    wparams1 = [1]
    scoresff = []
    weightrange = []
    weightsx = []
    # Go through all the weight groups and get their score and best ff
    for w1 in wparams1:
        for w2 in wparams:
            for w3 in wparams:
                for w4 in wparams:
                    for w5 in wparams:


                        datashotnumber.append(shotnumber)
                        datatimes.append(time0)

                        weights = [w1,w2,w3,w4,w5]
                        weightrange.append(weights)
                        score = ff_score(rules,weights)
                        scoreffidx = np.argmax(score)
                        scoresff.append(pf[scoreffidx])
                        weightx = w1*10000+w2*1000+w3*100+w4*10+w5
                        weightsx.append(weightx)
                        #print scoresff[-1]
                        if doplot:
                            a=1
                            plt.plot(pf,score,linewidth=1,label='Score[%d,%d,%d,%d,%d'%(w1,w2,w3,w4,w5));
    scoresff = np.array(scoresff)
    #print weightrange

    # Evaluate each ff based on the data collected by human operators:
    realffmean = np.mean(testdata.ffs)
    # gives the argument of the best weight group for this specific shot data
    evalff = scoresff-realffmean
    #evalff = np.abs(evalff)
    bestweightsidx = np.argmin(np.abs(evalff))

    #print evalff
    print testi,'Best weight idx is=',bestweightsidx,weightrange[bestweightsidx]

    if doplot:
        for ff in testdata.ffs:
            plt.axvline(x=ff, ymin=0, ymax=1,color='r',linestyle='--',linewidth=2)

        plt.axvline(x=pfmax1, ymin=0, ymax=1,color='k',linestyle='--',linewidth=2,label='d1')

        plt.axvline(x=pfmax2, ymin=0, ymax=1,color='Gray',linestyle='--',linewidth=2,label='d1')


        plt.xlim(freqramp[0],freqramp[-1])
        plt.ylim(0,1.1)
        plt.xlabel('ProbFreq [Hz]')

        plt.legend(fontsize='x-small')
        plt.show()
    
    evalffmatrix.append(evalff)




evalffmatrix = np.array(evalffmatrix) 
#print evalffmatrix
#print weightrange

# the best row is the one with the lowest vector norm, meaning in most cases it matched the best to the data
normrows = np.linalg.norm(evalffmatrix,axis=0)
meanrows = np.mean(evalffmatrix,axis=0)
stdrows = np.std(evalffmatrix,axis=0)
#print normrows,len(normrows),len(weightrange)
bestrowidx = np.argmin(normrows)
bestmeanrowidx = np.argmin(np.abs(meanrows))
beststdrowidx = np.argmin(np.abs(stdrows))

#print normrows
print normrows[bestrowidx]
print 'Total test cases:',len(loadedtestff)
print 'Best weight Norm configuration is:',normrows[bestrowidx],bestrowidx,weightrange[bestrowidx]
print 'Best weight Mean configuration is:',meanrows[bestmeanrowidx],bestmeanrowidx,weightrange[bestmeanrowidx]
print 'Best weight Std configuration is:',stdrows[beststdrowidx],beststdrowidx,weightrange[beststdrowidx]

plt.figure()
plt.pcolormesh(evalffmatrix)

#plt.figure()
#plt.plot(datashotnumber)
#plt.figure()
#plt.plot(datatimes)


plt.figure()
plt.plot(weightsx, evalff, label='ff values')
#plt.plot(normrows,label='norm')
plt.plot(weightsx, meanrows, label = 'mean')
plt.plot(weightsx, stdrows, label = 'std')
#plt.plot(np.array(datatimes)*1e10,label='times')
plt.legend()
plt.show()
