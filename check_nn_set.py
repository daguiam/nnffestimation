"""
Creates and tests the model for neural network FF estimation



"""


import keras
# Create first network with Keras
from keras.models import Sequential
from keras.layers import Dense
from keras.layers import Conv2D
from keras.models import load_model
import numpy as np
from scipy.stats import norm
import matplotlib.pylab as plt
import pickle


import libdata

from  nn_model import *


if __name__ == "__main__":

    import argparse
    

    parser = argparse.ArgumentParser(description='Loads data, creates nn model, trains and plots the data')
    
    parser.add_argument('-r','--rampup',action="store_true",default=False)
    parser.add_argument('-lc','--lowercutoff',action="store_true",default=False)
    parser.add_argument('-new','--trainnew',action="store_true",default=False)

    parser.add_argument('-a','--antenna',help='epochs',type=int,default=-1)

    
    
    args = parser.parse_args()
    
    antenna = args.antenna
    
    
    rampup = args.rampup
    lowercutoff = args.lowercutoff

    trainnew = args.trainnew
    np.random.seed(2)


    modelfile = 'testmodel.h5'
    # fix random seed for reproducibility
    seed = 7
    np.random.seed(seed)
    # load pima indians datase


    #do_1d()



    data = load_data()
    
    
    if not rampup:
        print 'Removing ramp up data'
        data = libdata.select_data_filter(data,data.flattop)
    if not lowercutoff:
        print 'Removing lower cutoff data'
        data = libdata.select_data_filter(data,np.invert(data.lowercutoff))
    if antenna != -1:
        data = libdata.select_data_filter(data,data.antenna==antenna)
    
    
    
    data_x,data_y = libdata.parse_data_nn(data,kind='1d')
   
    
    test_x,test_y = data_x,data_y

    model = load_model(modelfile)

    

    pred_test = model.predict(test_x)

    testdiff = libdata.unnormalise_ff_data(pred_test)-libdata.unnormalise_ff_data(test_y)
    #print testdiff

    etestmean = np.mean(testdiff)
    eteststd = np.std(testdiff)





    f,axarr = plt.subplots(4,1)

    ax = axarr[0]

    x = data_y
    y = data_y
    x = libdata.unnormalise_ff_data(x)/1e9
    y = libdata.unnormalise_ff_data(y)/1e9
    ax.plot(x,y,c='c',label='Correct')

    x = test_y
    y = pred_test
    x = libdata.unnormalise_ff_data(x)/1e9
    y = libdata.unnormalise_ff_data(y)/1e9
    
    emean = (etestmean)/1e9
    estd = (eteststd)/1e9
    
    ax.scatter(x,y,c='r',label='Pred set [E:%0.3f S:%0.3f]'%(emean,estd))
    #ax.scatter(data_y,data_y,c='c')

  
    
    ax.legend(loc='lower right',fontsize='x-small')


    #print len(train_y),len(test_y)


        
    ax = axarr[1]
    totalpower = data.nbi + data.ecrh+data.icrh
    totalpower = totalpower/1e6
    
    #print totalpower
      
    ax.scatter(totalpower,testdiff/1e9)
        
    ax = axarr[2]
    
    ax.scatter(totalpower,testdiff/1e9,c=totalpower)
    ax.scatter(-data.nbi/1e6,testdiff/1e9,c=totalpower)
       
    ax = axarr[3]
    
    ax.scatter(data.icrh/1e6,testdiff/1e9,c=totalpower)
    ax.scatter(-data.ecrh/1e6,testdiff/1e9,c=totalpower)



    plt.show()
