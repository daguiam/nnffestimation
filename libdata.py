"""
Verifies the data inside the pickle files


"""

import pickle
import numpy as np
import copy


import ricg

from ricg import common

from ricg.lib import rif_augdlib as augd

class dict2obj():
    def __init__(self,d):
        self.__dict__ = d

def check_if_between_times(time,tbegvec,tendvec):
    """ checks to see if time is placed between any pair of tbegvec/tendvec
    """
    for tbeg,tend in zip(tbegvec,tendvec):
        if time > tbeg and time < tend:
            return True
    return False
        

def is_plasma_flattop(IPA, time):
    """ Returns ramp up, flattop and ramp down.
        Assumes >80% flattop <20% etc
        Ramp up - flattop - ramp down
    """
    ipatime = IPA.time
    ipa = IPA.data

    maxcurrent = np.max(common.smooth(ipa,100))
    meancurrent = np.mean(ipa)
    idx = common.closest_index(ipatime,time)
    if ipa[idx] > 0.8*maxcurrent:
        return True #it is flattop
    return False

def data_do_stft(times,signals,freqramps,Fs,window,padding,step,windowtype):
    for i in range(len(times)):



        sig = np.array(signals[i,:])
        time = times
        freqramp = np.array(freqramps[i,:])

        stft_pf,stft_fb,stft = ricg.calc_stft(freqramp,sig,Fs=Fs,window=window,padding=padding,step=step,windowtype=windowtype)


        pk_pf, pk_fb, pk_amp = ricg.calcPeakBeatFrequencies( stft_pf,stft_fb,stft)
    return stft_pf,stft_fb,stft,pk_pf,pk_fb,pk_amp 

def get_discharge_time_features(shotnumber,time):
    """ returns True/False for presence of ELM, Flattop or ICRF ECRF NBI on """

    ICRH = augd.get_powerICRH(shotnumber)
    ECRH = augd.get_powerECRH(shotnumber)
    NBI = augd.get_powerNBI(shotnumber)

    if ICRH == 0:
        powericrh = 0
    else:
        powericrh = ICRH.data[common.closest_index(ICRH.time,time)]
    if ECRH == 0:
        powerecrh = 0
    else:
        powerecrh = ECRH.data[common.closest_index(ECRH.time,time)]
    if NBI == 0:
        powernbi = 0
    else:
        powernbi = NBI.data[common.closest_index(NBI.time,time)]

    elmdata = augd.get_diagSignal('ELM','t_endELM',shotnumber)
    #elmend = augd.get_diagSignal('ELM','t_endELM',shotnumber)

    if elmdata == 0:
        elmexists = False
    else:
        elmexists =  check_if_between_times(time,elmdata.time,elmdata.data)
    elm = elmexists

    IPA = augd.get_currentIpa(shotnumber)
    flattop = is_plasma_flattop(IPA, time)

    return powericrh,powerecrh,powernbi, elm,flattop


def get_data_features_ricg(shotnumber,antenna,time,verbose=False):
    shot = ricg.RIF(shotnumber=shotnumber,
                    antenna=antenna,
                    time=time,
                    )
    shot.gotoTime(time)
    equidiag = shot.Eq.equidiag

    params = ricg.default_params
    r,z,bfield,fce_antenna,fce_sep = shot.getBfieldProfile(shot.time,
                                                           shot.los.r,
                                                           shot.los.z)

    print '#%d Ant %d %0.4fs %s Fwall[%0.2f] Fsep[%0.2f]'%(shotnumber,antenna,time,equidiag,fce_antenna/1e9,fce_sep/1e9)



    r, z, bfield, fce_antenna, fce_sep = shot.getBfieldProfile(shot.time,
                                                        shot.los.r,
                                                        shot.los.z)
    los_ra = shot.los.ra
    los_b = bfield
    
    # Loads and processes the raw data for this sweep
    freqramp, sig, stft_pf, stft_fb, stft = shot.processRaw(
                                                    doCalibrate=True,
                                                    doFilter=True)

    # Calculates the beat frequencies from the stft data
    pk_pf, pk_fb, pk_amp = ricg.calcPeakBeatFrequencies(stft_pf,stft_fb,stft)
    pk_dg = shot.calc_fb2dg(pk_fb)

    # Normalized fb to dg conversion
    fb2dg = shot.calc_fb2dg(1)
    # Estimates the first fringe frequency
    
    
    ffnew, existsLowerCutoff = ricg.calc_ff(freqramp, sig,
                                            fce_antenna, fce_sep, 
                                            shot.Fs,
                                            los_ra,los_b,
                                            fb2dg,
                                            pk_pf, pk_fb, pk_dg, pk_amp,
                                            thresholdRatio=params.ff_threshold,
                                            smoothN=params.ff_smoothN,
                                            verbose=verbose)    
 

    return fce_antenna, fce_sep, ffnew, existsLowerCutoff






def load_test_update_data(filename):

    evalffmatrix = []
    datashotnumber = []
    datatimes =[]
    
    print 'Loading FFs from stored picke'
    loadedtestff = pickle.load(open(filename,'rb'))
    
    for testi in range(len(loadedtestff)):

        testdata = dict2obj(loadedtestff[testi])
        times = testdata.times
        shotnumber = testdata.shotnumber
        antenna = testdata.antenna
        time0 = times[-1]
        fce = testdata.fce

        fce_antenna, fce_sep, ffnew, existsLowerCutoff = get_data_features_ricg(shotnumber,antenna,time0)

        powericrh,powerecrh,powernbi, elm,flattop =   get_discharge_time_features(shotnumber,time0)

        print  'ICRH %0.2f ECRH %0.2f NBI %0.2f ELM %d Flattop %d LowerCutoff %d'%( powericrh/1e6,powerecrh/1e6,powernbi/1e6, elm,flattop,existsLowerCutoff)

        loadedtestff[testi]['fce_sep'] = fce_sep
        loadedtestff[testi]['fce_antenna'] = fce_antenna
        loadedtestff[testi]['ff_algorithm'] = ffnew

        loadedtestff[testi]['lowercutoff'] = existsLowerCutoff
        loadedtestff[testi]['icrh'] = powericrh
        loadedtestff[testi]['ecrh'] = powerecrh
        loadedtestff[testi]['nbi'] = powernbi
        loadedtestff[testi]['elm'] = elm
        loadedtestff[testi]['flattop'] = flattop


        #fce_antenna, fce_sep, ffnew, existsLowerCutoff = get_data_features_ricg(shotnumber,antenna,time0)

        powericrh,powerecrh,powernbi, elm,flattop


        striter = '%d: #%d Antenna %d %0.4f s'%(testi,shotnumber,antenna,time0)
        striter = striter+' FF=['
        for ff in np.array(testdata.ffs):
            striter = striter +'%0.2f, '%(ff/1e9)
        striter = striter+']'
        strfce =  ' Fstored[%0.2f] Eq: Fwall[%0.2f] Fsep[%0.2f]'%(fce/1e9,fce_antenna/1e9,fce_sep/1e9)
        striter = striter + strfce
        print striter

        




    print 'Saving back to pickle'
    pickle.dump(loadedtestff, open(filename,'wb'))


def load_test_data(filename,doStft=False):


    shotnumber_list = []
    antenna_list = []
    time_list = []
    signals_list = []
    freqramps_list = []
    amplitudes_list = []
    stft_pf_list = []
    stft_fb_list = []
    stft_list = []
    pk_pf_list = []
    pk_fb_list = []
    pk_amp_list = []
    ff_list = []
    fce_antenna_list = []
    fce_sep_list = []

    ff_algorithm_list = []
    lowercutoff_list = []
    icrh_list = []
    ecrh_list = []
    nbi_list = []
    elm_list = []
    flattop_list = []
    



    #shotnumber_list = []
    
    print 'Loading FFs from stored picke'
    loadedtestff = pickle.load(open(filename,'rb'))
    
    print len(loadedtestff)
    for testi in range(len(loadedtestff)):

        testdata = dict2obj(loadedtestff[testi])
        testdata.stft_Fs = testdata.stft_Fs.flatten()
      
        Fs = testdata.stft_Fs
        window = testdata.stft_window
        windowtype =testdata.stft_wintype
        padding =testdata.stft_padding
        step = testdata.stft_step
        times = testdata.times
        signals = np.array(testdata.sigs)
        freqramps = np.array(testdata.freqramp)
        shotnumber = testdata.shotnumber
        antenna = testdata.antenna

        padding = 1024
        

    
        time0 = times[-1]
    
        fce = testdata.fce
        fce_antenna = testdata.fce_antenna
        fce_sep = testdata.fce_sep

        striter = '%d: #%d Antenna %d %0.4f s'%(testi,shotnumber,antenna,time0)

        striter = striter+' FF=['
        for ff in np.array(testdata.ffs):
            striter = striter +'%0.2f, '%(ff/1e9)
        striter = striter+']'
        strfce =  ' Eq: Fwall[%0.2f] Fsep[%0.2f]'%(fce_antenna/1e9,fce_sep/1e9)
        striter = striter + strfce
        
        print striter
        #print len(times)
    
        stftpersist = None 
        amplitudes = []
        stft_pf = stft_fb = stft = []
        pk_pf = pk_fb = pk_amp = []

        amplitudes.append(np.abs(signals[0]))

        if doStft:

            stft_pf,stft_fb,stft,pk_pf,pk_fb,pk_amp = data_do_stft(times,signals,freqramps,Fs,window,padding,step,windowtype)
            stft = stft.astype('float32')

        if ff > fce_sep:
            print 'Skipping data because ff[%0.2f] > fce_sep[%0.2f]'%(ff/1e9,fce_sep/1e9)
            continue    
        

        ff_algorithm_list.append(testdata.ff_algorithm)
        lowercutoff_list.append(testdata.lowercutoff)
        icrh_list.append(testdata.icrh)
        ecrh_list.append(testdata.ecrh)
        nbi_list.append(testdata.nbi)
        elm_list.append(testdata.elm)
        flattop_list.append(testdata.flattop)

        
        amplitudes = np.array(amplitudes)

        shotnumber_list.append(shotnumber)
        antenna_list.append(antenna)
        time_list.append(time0)
        signals_list.append(signals)
        freqramps_list.append(freqramps)
        amplitudes_list.append(amplitudes)
        stft_pf_list.append(stft_pf)
        stft_fb_list.append(stft_fb)
        stft_list.append(stft)
        #print stft
        pk_pf_list.append(pk_pf)
        pk_fb_list.append(pk_fb)
        pk_amp_list.append(pk_amp)
        ff_list.append(ff)
        fce_antenna_list.append(fce_antenna)
        fce_sep_list.append(fce_sep)
    
    print 'Creating result dictionary of all data'
    result = dict(shotnumber=np.array(shotnumber_list),
                  antenna=np.array(antenna_list),
                  time=np.array(time_list),
                  signals=np.array(signals_list),
                  freqramps=np.array(freqramps_list),
                  amplitudes=np.array(amplitudes_list),
                  stft_pf=np.array(stft_pf_list),
                  stft_fb=np.array(stft_fb_list),
                  stft=stft_list,
                  pk_pf=np.array(pk_pf_list),
                  pk_fb=np.array(pk_fb_list),
                  pk_amp=np.array(pk_amp_list),
                  ff=np.array(ff_list),
                  fce_antenna=np.array(fce_antenna_list),
                  fce_sep=np.array(fce_sep_list),


                  ff_algorithm = np.array(ff_algorithm_list),
                  lowercutoff = np.array(lowercutoff_list),
                  icrh = np.array(icrh_list),
                  ecrh = np.array(ecrh_list),
                  nbi = np.array(nbi_list),
                  elm = np.array(elm_list),
                  flattop = np.array(flattop_list),

                  idx = np.arange(len(shotnumber_list))
                  )
    
    print 'returning result'
    return result


def load_obj_test_data(filename,doStft=False):
    return dict2obj(load_test_data(filename,doStft))



def select_data_filter(data,filter):
    """ Returns the data points that have a valid filter"""

    if len(filter)==1:
        print 'Filter has only one point'
        return data

    valididx = np.where(filter)[0]
    newdata = copy.copy(data)

    keys = data.__dict__.keys()

    for key in keys:
        vector = data.__dict__[key]
        print key,len(vector)#,vector
        if len(vector) <=1:
            print '%s does not have enough data, skipping'%(key),vector
            continue
        if isinstance(vector,list):
        #print valididx
            newdata.__dict__[key] = [vector[i] for i in valididx]
        else:
            newdata.__dict__[key] = vector[valididx]
            #newdata.__dict__[key].shape

        print key,len(newdata.__dict__[key])
    newdata.__dict__['idx'] = np.arange(len(newdata.shotnumber))
    return newdata



def parse_data_nn(data,kind='1d'):
    """ Parses the data object and returns the data_x,data_y vectors 
    to be stored """

    data_x = []
    data_y = []
    
    # 1D version
    if kind in ['1d','1',1,'1D']:
        """ In 1d parsing the data is concatenated in a single 1d vector
        Data is [ pk_amp,pk_fb,delims]
        """
        for i in data.idx:
            
            times = data.time[i]
            signals = data.signals[i]
            freqramps = data.freqramps[i]
            #Fs = data.Fs[i]
            #window = data.window[i]
            #padding = data.padding[i]
            #step = data.step[i]
            #windowtype = data.windowtype[i]

            #fcedelimiter =
            
            vec_x = []
            vec_y = []
 
            fce_antenna = data.fce_antenna[i]
            fce_sep = data.fce_sep[i] 
           
            #vec_x.append(data.pk_amp[i])
            #vec_x.append(data.pk_fb[i])
            
            pf = data.pk_pf[i]
            pfvalididx =  (pf<fce_antenna )| (pf> fce_sep)
            #print pfvalididx
            normamps = data.pk_amp[i]
            normamps = normamps/max(normamps)

            normamps[pfvalididx]=0

            for x in normamps:
            #for x in data.ampli[i]:
                vec_x.append(x)

            normfb = data.pk_fb[i]
            normfb = normfb/max(normfb)
            
            normfb[pfvalididx] = 0

            for x in normfb:
            #for x in data.pk_fb[i]:
                vec_x.append(x)

            

            fce_antenna = normalise_ff_data(data.fce_antenna[i]) 
            fce_sep = normalise_ff_data(data.fce_sep[i]) 
            #print fce_antenna,fce_sep
            vec_x.append(fce_antenna)
            vec_x.append(fce_sep)


            ff = data.ff[i]
            ff = normalise_ff_data(ff)
            vec_y.append(ff)
            #print i,data.ff[i],ff
            #print i,vec_x,vec_y
            data_x.append(np.array(vec_x).flatten())
            data_y.append(np.array(vec_y).flatten())
            
            

            #stft_pf,stft_fb,stft,pk_pf,pk_fb,pk_amp = data_do_stft(times,signals,freqramps,Fs,window,padding,step,windowtype)
 
        #data_y = np.array(data_y).flatten()
            
        
    data_x = np.array(data_x)
    data_y = np.array(data_y)

    return data_x, data_y




def parse_data_2d(data, fblims=(0,20e6)):
    """ Parses the provided data and returns the 2d data_x and single value
    data_y.
    The 2d data is the stft matrix limited to the relevant data boundaries

    """

    data_x = []
    data_y = []
    
    # 1D version
    if 1:# kind in ['1d','1',1,'1D']:
        """ In 1d parsing the data is concatenated in a single 1d vector
        Data is [ pk_amp,pk_fb,delims]
        """
        for i in data.idx:
            
            times = data.time[i]
            signals = data.signals[i]
            freqramps = data.freqramps[i]
            #Fs = data.Fs[i]
            #window = data.window[i]
            #padding = data.padding[i]
            #step = data.step[i]
            #windowtype = data.windowtype[i]

            #fcedelimiter =
            
            vec_x = []
            vec_y = []
 
            #print data.stft
            stft = data.stft[i]#[0]
            #stft = np.array(stft)
            #print stft[0][0][0][0]
            stft_pf = data.stft_pf[i]
            stft_fb = data.stft_fb[i]
            minfbidx = common.closest_index(stft_fb,fblims[0])
            maxfbidx  = common.closest_index(stft_fb,fblims[1])

            #print stft_pf,stft_fb
            #print i,stft.shape
            stft = stft[minfbidx:maxfbidx,:]
            stft_fb = stft_fb[minfbidx:maxfbidx]
            #print i,stft.shape
            

            fce_antenna = data.fce_antenna[i]
            fce_sep = data.fce_sep[i] 
                       

            fce_antenna = normalise_ff_data(data.fce_antenna[i]) 
            fce_sep = normalise_ff_data(data.fce_sep[i]) 
            #print fce_antenna,fce_sep
            vec_x.append(fce_antenna)
            vec_x.append(fce_sep)


            ff = data.ff[i]
            ff = normalise_ff_data(ff)
            vec_y.append(ff)
            #print i,data.ff[i],ff
            #print i,vec_x,vec_y
            vec_x = stft/np.max(stft)
            
            data_x.append(np.array(vec_x))
            data_y.append(np.array(vec_y).flatten())
            
            

            #stft_pf,stft_fb,stft,pk_pf,pk_fb,pk_amp = data_do_stft(times,signals,freqramps,Fs,window,padding,step,windowtype)
 
        #data_y = np.array(data_y).flatten()
            
    print 'returning 2d data as np array'
    data_x = np.array(data_x)
    data_y = np.array(data_y)

    return data_x, data_y
            
def normalise_ff_data(ff):
    """ Normalises the frequency data to the expected range between 0 and 1
    from 40 to 68 GHz
    """
    #if len(ff) == 0:
    #    return ff
    ffspan = np.max(ff)-np.min(ff)
    fmin = 40e9
    fmax = 68e9
    ffspan = fmax-fmin

    ff = (ff-fmin)/ffspan

    return ff
    

def unnormalise_ff_data(ff):
    """ unormalises the frequency data to the expected range between 0 and 1
    from 40 to 68 GHz
    """
    #if len(ff) == 0:
    #    return ff
    ffspan = np.max(ff)-np.min(ff)
    fmin = 40e9
    fmax = 68e9
    ffspan = fmax-fmin

    ff = ff*ffspan+fmin

    return ff

