# Create first network with Keras
from keras.models import Sequential
from keras.layers import Dense
from keras.models import load_model
import numpy as np
from scipy.stats import norm

import matplotlib.pylab as plt

import pickle

#from common import *
epochs = 50
batches = 32
np.random.seed(12)

# fix random seed for reproducibility
seed = 7
np.random.seed(seed)
# load pima indians dataset
#dataset = np.loadtxt("pima-indians-diabetes.data.txt", delimiter=",")
trainfile = 'data/traindata.dat.npy'
dataset = np.load(trainfile)
# split into input (X) and output (Y) variables
#
trainfile = 'data/traindatapf.dat.npy'
datapf = np.load(trainfile)

trainfile = 'data/traindataff.dat.npy'
dataffidx = np.load(trainfile)

pf = datapf

# Real test value of ff
testff = pf[dataffidx]
modelfile='modelFFeval_epochs%d.h5'%(epochs)


ffspan = max(pf)-min(pf)
singletestff = (testff-min(pf))/ffspan
singletestff = (testff-40e9)/28e9
print singletestff



# Build Y result vector
pfspan = 0.05
outY = []
print pf
print testff
print dataffidx


for i,ff in enumerate(testff):
	result = norm.pdf(pf/1e9,ff/1e9,pfspan)
	result = result/np.max(result)


	#result = np.zeros(len(pf))
	#result[dataffidx[i]]=1


	result = np.ones(2)*singletestff[i]
	#print result
	outY.append(result)

outY = np.array(outY)

#print np.shape(Y),np.shape(outY)
Y = outY
print Y
#Y = singletestff

X = dataset.reshape(len(dataset[:,0,0]),-1)
print np.shape(X)
# Y = dataset[:,-1,:]
# X = dataset[:,0,:]
#
# singlelength = len(dataset[0,:,0])
# # removing Y data....
# singlelength = singlelength-1
#
# if 0:
# 	datamask = [0,1,2,3]
# 	datamask = range(singlelength-1)
# 	datamask = [0,3]
# 	for i in datamask:
# 		X2 = dataset[:,i+1,:]
# 		X = np.concatenate((X,X2),axis=1)
#
#
testcases = len(Y)
#testmask = [2,27,67,70,95,103,175,170,200]
testmask = np.random.random_integers(testcases,size=(9))
# maskneg = range(testcases)
# maskpos = np.delete(maskneg,testmask)
#
# print testcases,len(maskpos),len(testmask)
# #exit()
# testX = X[testmask,:]
# testY = Y[testmask,]
# #X = X[maskpos]
# #Y = Y[maskpos]

testX = X
testY = Y#np.array([Y]).T


dimInput = len(X[0])
#dimOutput = len(Y[0])
dimOutput = len(Y[0])

argmaxY = 0

if argmaxY == 1:
	Ymax = np.argmax(Y,axis=1)
	#print Ymax
	Y = pf[Ymax]/1e9


sizeH1 = dimInput
sizeH2 =  sizeH1
sizeH3 = dimOutput
sizeH4 = 1




# create model
model = Sequential()
model.add(Dense(sizeH1, input_dim=dimInput, init='uniform', activation='relu'))
model.add(Dense(sizeH2, init='uniform', activation='relu'))
#model.add(Dense(sizeH2, init='uniform', activation='relu'))
model.add(Dense(sizeH3, init='uniform', activation='sigmoid'))
#model.add(Dense(sizeH4, init='uniform', activation='sigmoid'))
# Compile model
model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])
#model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['poisson','accuracy'])
# Fit the model
model.fit(X, Y, nb_epoch=epochs, batch_size=batches,verbose=1,shuffle=True,validation_split=0.00)
# evaluate the model
#scores = model.evaluate(X, Y)
#print("%s: %.2f%%" % (model.metrics_names[1], scores[1]*100))

print model.summary()


model.save(modelfile)

del model
model = load_model(modelfile)


predictions = model.predict(testX)
print predictions.shape




if 0:
	predmaxarg = np.argmax(predictions, axis=1)
	predmeanarg = np.argmax(predictions, axis=1)
	#


	pf = datapf
	predictions = predictions.T
	testY = testY.T
	print len(testmask), predictions.shape
	totalplots = len(testmask)
	for i in range(len(testmask)):
		#plt.figure()


		plt.subplot(totalplots, 1, i + 1)

		plt.plot(pf,testY[:,i], linestyle='--', color='k', linewidth = 1)
		if not argmaxY:
			plt.plot(pf,predictions[:,i],color='blue',linewidth = 2)
			print i,pf[np.argmax(testY[:,i])]
			plt.axvline(x=pf[np.argmax(predictions[:,i])],ymin=0,ymax=1,color='r',linestyle='-',label='prediction')
		else:
			plt.axvline(x=predictions[i],ymin=0,ymax=1,color='r',linestyle='-',label='prediction%d'%(testmask[i]))
		plt.axvline(x=pf[np.argmax(testY[:,i])],ymin=0,ymax=1,color='k',linestyle='--',label='FF%d'%(testmask[i]))
		plt.legend(fontsize='x-small')
		plt.xlabel('Probing Frequency [Hz]')
		plt.ylabel('pdf\nFF')

if 1:
		#predictions = predictions.T

		#predictions = predictions*ffspan+min(pf)
		trained = testY[:,0]*28+40
		preds = predictions[:,0]*28+40



		sortidx = np.argsort(trained)
		trainedsort = trained[sortidx]
		predssort = preds[sortidx]

		error = predssort-trainedsort
		print error

		#plt.plot(testff[sortidx], linestyle='-', color='k', linewidth = 1,label='fce')
		plt.plot(trainedsort, linestyle='-', color='b', linewidth = 1,label='train')
		plt.plot(predssort, linestyle='-', color='r', linewidth = 1,label='prediction')

		plt.legend(fontsize='x-small')
		plt.xlabel('Tests')
		plt.ylabel('FF')

print 'plotting'
plt.suptitle('Epochs=%d, Error Mean: %0.2f Std %0.2f'%(epochs,np.mean(error),np.std(error)))
plt.savefig('nnEpochs%d.png'%(epochs))


predictions = [trained,preds]
pickle.dump(predictions,open('preds_epochs%d.pickle'%(epochs),'wb'))


if 1:

	errorsortidx = np.argsort(np.abs(error))[::-1]

	checkcasesidx = errorsortidx[:3]
	print checkcasesidx
	#print error[errorsortidx]
	#print errorsortidx
	for idx in checkcasesidx:
		Xsort = X[sortidx]
		case = Xsort[idx]

		pfsize = len(pf)
		amp = case[0:pfsize]
		fb = case[pfsize:2*pfsize]
		fce = case[2*pfsize:]

		plt.figure()
		plt.plot(pf/1e9,fce*0.1,'k',linewidth=2,label='fce')
		plt.plot(pf/1e9,amp,'r',label='amps')
		plt.plot(pf/1e9,fb,'b',label='fb')
		plt.axvline(x=predssort[idx],ymin=0,ymax=1,color='m',linestyle='-',linewidth=2,label='prediction')
		plt.axvline(x=trainedsort[idx],ymin=0,ymax=1,color='y',linestyle='-',linewidth=2,label='tested')
		plt.legend()

		plt.title('%d error: %0.2f'%(idx,error[idx]))
		plt.ylabel('Probing Frequency [GHz]')


plt.show()
