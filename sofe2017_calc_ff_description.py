import numpy as np

import matplotlib.pyplot as plt
from scipy import signal

import ricg

from ricg import common
import logging

from libnn import colors2 as colors

def freq_hilbert(sig,Fs):
    """ Calcultes the instantaneous frequency based on the hilbert 
    transform
    """
    sigorig = sig
    sig = sig.real
    ts = 1.0/Fs
    hilb = signal.hilbert(sig)
    ampli = np.abs(hilb)
    
    norm = sig/ampli
    hilbnorm = signal.hilbert(norm)
    env = np.abs(hilbnorm)
    phase = np.unwrap(np.angle(hilbnorm))
    print 'phase1',phase
    phase = np.unwrap(np.angle(sigorig))
    print 'phase2',phase
    #exit()

    phasediff = np.append([0],np.diff(phase))
    print 'phasediff',phasediff
    freq = phasediff/(2*np.pi*ts)
    #freq = freq/(2*np.pi)
    return freq


# Implemented smooth moving average algorithm
def smooth(data,N=5):
    return np.convolve(np.ones(N)/N,data,'same')


def closest_index(array,value):
    """ Finds the index of the array element closest to value
    """
    array = np.array(array)
    return (np.abs(np.array(array)-value)).argmin()

def calc_ff(freqramp,sig,
                    fce,fsep,Fs,
                    ra,b,
                    fb2dg,
                    pk_pf, pk_fb, pk_dg, pk_amp,
                    thresholdRatio=0.02,
                    smoothN=15,
                    verbose=False):
    """ Performs an adaptation of  first fringe estimation algorithm 
    described in the DIII-D paper "Improved reflectometer electron 
    density profile measurements on DIII-D" by Wang 2003
    """
    
    logger = logging.getLogger(__name__)
    sigorig = sig
    #sig = sig.real
    
    # interpolate the peaks
    pk_fb = np.interp(freqramp,pk_pf,pk_fb)
    pk_dg = np.interp(freqramp,pk_pf,pk_dg)
    pk_amp = np.interp(freqramp,pk_pf,pk_amp)
    pk_pf = freqramp
    
    #1. Limit range to fce at wall and fce at separatrix
    fceidx = closest_index(freqramp,fce)
    fsepidx = closest_index(freqramp,fsep)
    
    if fsepidx <= fceidx+1:
        #fsepidx = fceidx+3
        return (freqramp[fceidx]+freqramp[fsepidx])/2, False
    
    sigrange = sig[fceidx:fsepidx]
    freqramprange = freqramp[fceidx:fsepidx]
    sigamp = np.abs(sigrange)
    smoothsigamp = common.smooth(np.abs(sig),1)[fceidx:fsepidx]
    smoothsigamp = common.smooth(np.abs(pk_amp),1)[fceidx:fsepidx]



    #2. Rough estimate of fstart by increase in signal amplitude
    # todo maxdx equals to peak beat frequency in the range
    #maxidx = np.argmax(np.diff(smoothsigamp))+1
    #fstartrough = freqramprange[maxidx]
    #logger.debug( 'Rough fstart is %0.3f'%(fstartrough))
    
    maxidx = np.argmax(pk_fb[fceidx:fsepidx])
    
    fstartrough = freqramprange[maxidx]
    if verbose:
        logger.debug( 'Rough fstart is %0.3f'%(fstartrough))
    fstartidx = closest_index(freqramprange,fstartrough)

    #3. Band pass filter around average IF frequency from start to sep
    startsig = sigrange[fstartidx:]
    #use HILBERT
    #freqsig = freq_hilbert(startsig.real,Fs)
    #freqmean = np.mean(freqsig)
    #freqrms = np.sqrt(np.mean(np.power(freqsig,2)))


    #1.5 Test if lower cut off exists
    # In case the amplitude at the fce_antenna > 0.5*Pmax in the range
    # if there is lower cut off, choose the maximum peak fb in the sigrange!
    amp_fce_antenna = np.abs(sig[fceidx])
    amp_fce_antenna = smoothsigamp[0]
    amp_fce_antenna = np.mean(np.abs(sig[:fceidx]))
    meanstartsigamp =  0.5*np.mean(np.abs(startsig))
 
    if amp_fce_antenna >= meanstartsigamp  :
        existsLowerCutoff = True
    else:
        existsLowerCutoff = False
    print 'Amplitudes:', amp_fce_antenna,meanstartsigamp, existsLowerCutoff
    
    if verbose:
        logger.debug( 'Exists lower cutoff: %r'%(existsLowerCutoff))
        logger.debug( 'Amp Fce %0.3f and Mean %0.3f '%(amp_fce_antenna/1e9,meanstartsigamp/1e9))
    
    #3.1 Using freqmean from provided pk_fb
    # Limits search range to between fstart and fsep
    startfb = pk_fb[fstartidx:fsepidx]
    freqmean = np.mean(startfb)
    freqrms = np.std(startfb)

    # Filtering uses the complete broadband signal which is cut afterwards
    sigtofilter = sig
    if freqrms <=0.0:
        freqrms = 1e6
    lowcut = freqmean - freqrms/2
    if lowcut <0:
        lowcut = 0
    highcut = freqmean+freqrms/2
    if highcut <6e6:
        highcut = 6e6
    lowcut = freqmean - freqrms/1
    highcut = freqmean+freqrms/1

    if verbose:
        logger.debug(' D3d Filter Freqmean %0.2f, Freqrms %0.2f.'%(freqmean/1e6,freqrms/1e6))
        logger.debug(' D3d Filter high %0.2f, low %0.2f.'%(highcut/1e6,lowcut/1e6))

    if freqmean >1e6 and freqrms > 0.0:
        try:
            sigfilt = common.filtfilt_bandpass(sigtofilter,lowcut,highcut,Fs=Fs,order=5)
        except:
            if verbose:
                logger.debug( ' trycalcff D3D Error: Freqmean %0.2f, Freqrms %0.2f. not filtering'%(freqmean/1e6,freqrms/1e6))
            sigfilt = sigtofilter
    
    else:
        if verbose:
            logger.debug( ' calcff D3D Error: Freqmean %0.2f, Freqrms %0.2f. not filtering'%(freqmean/1e6,freqrms/1e6))
        sigfilt = sigtofilter

    sigfiltcut = sigfilt[fceidx:fsepidx]
    #4. Find Max power of the smoothed square of the filtered signal
    power = np.abs(np.power(sigfiltcut,2))
    #thresholdRatio = 0.02
    #smoothN = 30
    power = common.smooth(power,smoothN)
    #power = common.smoothweighted(power,pk_fb[fceidx:fsepidx],smoothN)
    
    #4.5 Find the peak at which the power has a maximum derivate, as in 2.
    powerdiff =np.append([0],np.diff(power))
    powerdiff[powerdiff<0] = 0
    
    #power = power*powerdiff
    Pmax = np.max(power)
    Pmin = np.min(power)
    
    #5. First frequency below whose smoothed square avalue is belo threshold
    # starting from the separatrix

    threshold = thresholdRatio*Pmax+Pmin
    
    #thresholdidx = np.argmax(power>threshold)-1
    
    powerlen = len(power)
    powerflip = power[::-1]
    maxpowerflipidx = np.argmax(powerflip)
    maxpowerflipidx = 0
    thresholdidx = np.argmax(powerflip[maxpowerflipidx:]<threshold)
    thresholdidx = maxpowerflipidx + thresholdidx
    thresholdidx = powerlen-thresholdidx-1
    
    fstart = freqramprange[thresholdidx]
    
    
    
    # In case of lower cut off, the ff is at the best ratio of peak fb and 
    # negative beat frequency derivative
    ratio = np.ones(len(freqramprange))
    if existsLowerCutoff:
        print 'Calculating lowert cut off ff'
        fbrange = common.smooth(pk_fb,5)[fceidx:fsepidx]

        fceidx2 = closest_index(freqramp,fce)
        fsepidx2 = closest_index(freqramp,fsep)

        freqsig = freq_hilbert(sig,Fs)
        fbrange = freqsig[fceidx2:fsepidx2]
        #fbrange = pk_fb[fceidx:fsepidx]
        #fbrange = common.smooth(fbrange,10)
        fbrange = fbrange/max(np.abs(fbrange))
        fbdiff = np.append([0],np.diff(fbrange))
        
        ratio = fbrange-power/max(power)
        maxpk_fbidx = np.argmax(fbrange)
        print maxpk_fbidx
        maxpk_fbidx = np.argmax(ratio)
        print maxpk_fbidx        
        fstart_lc = freqramprange[maxpk_fbidx]
        if verbose:

            logger.debug('Calculated ff %0.2f ff due to lc %0.2f'%(fstart/1e9,fstart_lc/1e9))
        fstart = fstart_lc
    
    
    



    
    return fstart, existsLowerCutoff
    

if __name__ == "__main__":
    
    
    shotnumber = 33291
    antenna = 1
    time = 2.45
    time = 3.4
    persistence = 4
    
    shot = ricg.RIF(shotnumber=shotnumber,
                    antenna=antenna,
                    time=time,
                    persistence=persistence,
                    verbose=True)
    shot.gotoTime(time)


   
    for i in range(persistence):
        print i, shot.sweepnr, shot.raw.time, shot.time
        
        
        shot.nextSweep()
        #sweep_iteration(shot)


        #shot.processRaw()
        shot.processProfile()
    
    freqramp = shot.freqramp
    sig = shot.signal
    fce = shot.fce_antenna
    fsep = shot.fce_sep
    Fs = shot.Fs
    ra = shot.los_ra
    b = shot.los_b
    fb2dg = shot.calc_fb2dg(1)

    pk_pf = shot.pk_pf
    pk_fb = shot.pk_fb
    pk_dg = shot.calc_fb2dg(pk_fb)

    pk_amp = shot.pk_amp

    stft_pf = shot.stft_pf
    stft_fb = shot.stft_fb
    stft = shot.stft

         
    
    


   
    
    
    #1. Limit range to fce at wall and fce at separatrix
    fceidx = closest_index(freqramp,fce)
    fsepidx = closest_index(freqramp,fsep)


    sigrange = sig[fceidx:fsepidx]
    freqramprange = freqramp[fceidx:fsepidx]
    sigamp = np.abs(sigrange)
    smoothsigamp = common.smooth(np.abs(sig),10)
    smoothsigamprange = smoothsigamp[fceidx:fsepidx]
    #smoothsigamp = common.smooth(np.abs(pk_amp),1)[fceidx:fsepidx]

    fstartidx = np.argmax(np.diff(smoothsigamprange))+1+fceidx
    

    froughstart = freqramp[fstartidx]



    freqsig = freq_hilbert(sig,Fs)
    freqsig = common.smooth(freqsig,10)

    freqsig = freqsig
    freqrange = freqsig[fceidx:fsepidx]
   
    avgfreq = np.mean(freqrange[fstartidx-fceidx:])
    stdfreq = np.std(freqrange[fstartidx-fceidx:])
    print 'avg freq in range',avgfreq/1e6,stdfreq/1e6

 
    fbandspan = 5e6
    fbandspan = 2e6
    lowcut = avgfreq - fbandspan
    highcut = avgfreq + fbandspan
    lowcut = avgfreq - stdfreq
    highcut = avgfreq + stdfreq

    sigfilt = common.filtfilt_bandpass(sig,lowcut,highcut,Fs=Fs,order=5)

    sigfiltamp = np.abs(sigfilt)
    sigfiltamp = common.smooth(sigfiltamp,10)
    
    
    # calculate the ff
    
    sigfiltcut = sigfilt[fceidx:fsepidx]
    sigfiltcut = sig[fceidx:fsepidx]
    #4. Find Max power of the smoothed square of the filtered signal
    power = np.abs(np.power(sigfiltcut,2))
    #thresholdRatio = 0.02
    #smoothN = 30
    power = common.smooth(power,15)
    #power = common.smoothweighted(power,pk_fb[fceidx:fsepidx],smoothN)
    
    #4.5 Find the peak at which the power has a maximum derivate, as in 2.
    powerdiff =np.append([0],np.diff(power))
    powerdiff[powerdiff<0] = 0
    
    #power = power*powerdiff
    Pmax = np.max(power)
    Pmin = np.min(power)
    
    #5. First frequency below whose smoothed square avalue is belo threshold
    # starting from the separatrix
    thresholdRatio = 0.02
    threshold = thresholdRatio*Pmax+Pmin
    
    #thresholdidx = np.argmax(power>threshold)-1
    
    powerlen = len(power)
    powerflip = power[::-1]
    maxpowerflipidx = np.argmax(powerflip)
    maxpowerflipidx = 0
    thresholdidx = np.argmax(powerflip[maxpowerflipidx:]<threshold)
    thresholdidx = maxpowerflipidx + thresholdidx
    thresholdidx = powerlen-thresholdidx-1
    
    fstart = freqramprange[thresholdidx]
    

    #startsig = sig[fstartidx:fsepidx]

    amp_fce_antenna = np.mean(np.abs(sig[:fceidx]))
    meanstartsigamp =  0.5*np.mean(np.abs(sig[fsepidx:]))
 
    if amp_fce_antenna >= meanstartsigamp  :
        existsLowerCutoff = True
    else:
        existsLowerCutoff = False
    print 'Amplitudes:', amp_fce_antenna,meanstartsigamp, existsLowerCutoff
    

    if existsLowerCutoff:
        print 'Calculating lowert cut off ff'
        #fbrange = common.smooth(pk_fb,5)[fceidx:fsepidx]

        fceidx2 = closest_index(freqramp,fce)
        fsepidx2 = closest_index(freqramp,fsep)

        #freqsig = freq_hilbert(sig,Fs)
        #freqsig = common.smooth(freqsig,10)

        fbrange = freqsig[fceidx2:fsepidx2]
        #fbrange = pk_fb[fceidx:fsepidx]
        #fbrange = common.smooth(fbrange,10)
        fbrange = fbrange/max(np.abs(fbrange))
        fbdiff = np.append([0],np.diff(fbrange))
        
        ratio = fbrange-power/max(power)
        maxpk_fbidx = np.argmax(fbrange)
        print maxpk_fbidx
        maxpk_fbidx = np.argmax(ratio)
        print maxpk_fbidx        
        fstart_lc = freqramprange[maxpk_fbidx]
 

    if existsLowerCutoff:
    
        print fstart,fstart_lc
        fstart = fstart_lc

    ##### PLOTTING 


    if 1:
        ### SCALING SCALE
         freqramp = freqramp/1e9

         fce = fce/1e9
         fsep = fsep/1e9

         pk_pf = pk_pf/1e9
         pk_fb = pk_fb/1e6
         pk_dg = pk_dg*1e9


         avgfreq = avgfreq/1e6
         lowcut = lowcut/1e6
         highcut = highcut/1e6
         freqsig = freqsig/1e6
         freqrange = freqrange/1e6

         freqramprange = freqramprange/1e9

         froughstart = froughstart/1e9
         fstart = fstart/1e9

         stft_pf = stft_pf/1e9
         stft_fb = stft_fb/1e6
         stft = stft

    if 1:
         import matplotlib as mpl
         mpl.rcParams['lines.linewidth'] = 1
         #mpl.rcParams['lines.color'] = 'r'

    lw = 1
    cb = '#1f77b4'
    cr = '#d62728'
    cp = '#9467bd'
    cpi = '#e377c2'
    cg = '#2ca02c'
    co = '#ff7f0e'
    alphaval = 0.5
    alphashade = 0.3

    cor = colors[1]
    cor1 = colors[2]
    cor2 = colors[3]
    corfce = colors[7]
    corfce = 'k'
    corsep = colors[7]

    corbw = colors[9]

    corff= colors[1]
    corerror = colors[0]

    corrough = colors[6]
    coramp = colors[3]


    corfce = 'k'
    corsep = colors[7]
    alphaval = 0.8
    lw = 1.5

    figdpi = 300
    fi = 0
    filenamepng = 'images/sofe2017_calc_ff_%d.png'
    filenamepdf = 'images/sofe2017_calc_ff_%d.pdf'


    hl = []
#    f,axarr = plt.subplots(3,1,sharex=True, figsize=(3.37,3))
    f,axarr = plt.subplots(3,1,sharex=True,figsize=(5,4))
    
    f.add_subplot(111, frameon=False)
# hide tick and tick label of the big axes
    plt.tick_params(labelcolor='none', top='off', bottom='off', left='off', right='off')


    #plt.ylabel("Beat frequency [MHz]")
    plt.xlabel("Probing frequency [GHz]")
    #plt.suptitle('X-mode reflectometry scenarios')


    #plt.xlabel('Probing frequency [GHz]')
    plt.subplots_adjust(left=0.2, bottom=0.15, right=None, top=None, wspace=0.05, hspace=0)



    ax = axarr[0]

    ax.set_xlim([45,54])

    axarr[0].set_ylim([-0.55,0.55])

    axarr[1].set_ylim([-5,25])
    axarr[2].set_ylim([-0.45,0.45])


    #ax.plot(freqramp,sig.imag,label='I')
    ax.set_ylabel('Amplitude [V]')
      
    # plot fce wall
    for ax in axarr:
        h1 = ax.axvline(x=fce,linestyle='--',c=corfce,alpha=alphaval,lw=lw,label='Wall')
        h2 = ax.axvline(x=fsep,linestyle='--',c=corsep,alpha=alphaval,lw=lw,label='Separatrix')


    hl.append(h1)
    hl.append(h2)
    ax = axarr[0]   
    ax.plot(freqramp,sig.real,label='Q',c=cb,lw=1)
    h =ax.plot(freqramp,smoothsigamp,label='Amplitude',c=coramp,alpha=alphaval,lw=1)
    hl.append(h)
    #ax.plot(freqramprange,sigamp)
    ax.plot(freqramprange,smoothsigamprange,c=coramp,lw=1)
    
    #ax.axvline(x=froughstart, c='r')
        
    ax = axarr[1]
    
   # ax.axvline(x=froughstart, c='r')

    ax.set_ylabel(r'$f_b$ [MHz]')

    ax.pcolormesh(stft_pf,stft_fb,stft,cmap='Greens', rasterized=True)




    fi += 1;f.savefig(filenamepng%(fi),dpi=figdpi);f.savefig(filenamepdf%(fi),dpi=figdpi);print filenamepng%(fi)    
    ax=axarr[1]
    for ax in axarr:

        h3 = ax.axvline(x=froughstart, c=corrough,alpha=alphaval,lw=lw,linestyle='--',label='Rough FF')

    hl.append(h3)

    ax=axarr[1]


    ax.plot(freqramp, freqsig, c=cg,alpha=alphashade,lw=lw)
    fi += 1;f.savefig(filenamepng%(fi),dpi=figdpi);f.savefig(filenamepdf%(fi),dpi=figdpi);print filenamepng%(fi)


    froughidx = closest_index(freqramprange,froughstart)
    h = ax.plot(freqramprange[froughidx:], freqrange[froughidx:],alpha=alphaval,c=cg,lw=lw,label='Signal frequency')

    hl.append(h)

    
    fi += 1;f.savefig(filenamepng%(fi),dpi=figdpi);f.savefig(filenamepdf%(fi),dpi=figdpi);print filenamepng%(fi)

    h = ax.axhspan(lowcut,highcut, facecolor=corbw, alpha=alphashade,label='Filter region')

    hl.append(h)
    ax.axhline(y=avgfreq, c=corbw,linestyle='--',lw=lw,alpha=alphaval)

    fi += 1;f.savefig(filenamepng%(fi),dpi=figdpi);f.savefig(filenamepdf%(fi),dpi=figdpi);print filenamepng%(fi)

    #ax.axhline(y=lowcut, c=corbw,lw=lw,alpha=alphashade)
    #ax.axhline(y=highcut, c=corbw,lw=lw,alpha=alphashade)
   
    #ax.set_ylim([-5,25])

    ax = axarr[2]
 

    ax.plot(freqramp,sigfilt.real,lw=1,c=cb)
    #ax.plot(freqramp,sigfilt.imag)
    ax.plot(freqramp,sigfiltamp,lw=1,c=coramp)


    ax.set_ylabel('Amplitude [V]')

    fi += 1;f.savefig(filenamepng%(fi),dpi=figdpi);f.savefig(filenamepdf%(fi),dpi=figdpi);print filenamepng%(fi)
 



    for ax in axarr:

        #ax.axvline(x=froughstart, c=corrough,alpha=alphaval,lw=lw,linestyle='--')
        h = ax.axvline(x=fstart, c=corff,alpha=alphaval,lw=lw,label='Estimated FF')
    fi += 1;f.savefig(filenamepng%(fi),dpi=figdpi);f.savefig(filenamepdf%(fi),dpi=figdpi);print filenamepng%(fi)
        

    hl.append(h)


    ax = axarr[1]

    handles = []

    for i in hl:
        if isinstance(i, list):
            i = i[0]
        handles.append(i)
    l=ax.legend(handles=handles,fontsize='small')
    l.set_zorder(20)
    
    axarr[2].set_zorder(-1)
    fi += 1;f.savefig(filenamepng%(fi),dpi=figdpi);f.savefig(filenamepdf%(fi),dpi=figdpi);print filenamepng%(fi)
   
    plt.show()
