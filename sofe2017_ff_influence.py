import numpy as np

import matplotlib.pyplot as plt
from scipy import signal

import ricg

from ricg import common
from ricg import reflectometry
import logging
from ricg import default_params

from mpl_toolkits.axes_grid1.inset_locator import zoomed_inset_axes
from mpl_toolkits.axes_grid1.inset_locator import mark_inset

from matplotlib.ticker import MultipleLocator, FormatStrFormatter

def sweep_iteration(shot,fixedff=None,verbose=True):
    """ This is the iteration that occurs for each sweep
    """
            
        #freqramp, sig, stft_pf, stft_fb, stft = shot.processRaw(
                                                        #doCalibrate=True,
                                                        #doFilter=True)

    r, z, bfield, fce_antenna, fce_sep = shot.getBfieldProfile(shot.time,
                                                        shot.los.r,
                                                        shot.los.z)
    los_ra = shot.los.ra
    los_b = bfield
    
    
    # Loads and processes the raw data for this sweep
    freqramp, sig, stft_pf, stft_fb, stft = shot.processRaw(
                                                    doCalibrate=True,
                                                    doFilter=True)
    stft_dg = shot.calc_fb2dg(stft_fb)


    # Calculates the beat frequencies from the stft data
    pk_pf, pk_fb, pk_amp = ricg.calcPeakBeatFrequencies(stft_pf,stft_fb,stft)
    pk_dg = shot.calc_fb2dg(pk_fb)


    # Normalized fb to dg conversion
    fb2dg = shot.calc_fb2dg(1)
    # Estimates the first fringe frequency
    
    
    ffnew, existsLowerCutoff = ricg.calc_ff(freqramp, sig,
                                            fce_antenna, fce_sep, 
                                            shot.Fs,
                                            los_ra,los_b,
                                            fb2dg,
                                            pk_pf, pk_fb, pk_dg, pk_amp,
                                            thresholdRatio=default_params.ff_threshold,
                                            smoothN=default_params.ff_smoothN,
                                            verbose=verbose)

    
    print 'Estimated ff',ffnew
    # Other possible estiamtives for FF:
        #ff = (fce_sep-fce_antenna)/2+fce_antenna
        #ff =  fce_antenna + 1.8e9
    if fixedff is not None:
        print 'Using fixed ff!',fixedff,ffnew
        ff = fixedff 
    else:
        print 'UsingNot fixed ff!',fixedff,ffnew

        ff =  ffnew
    #ff = fixedff or ffnew
    
    
    # Calculats the reflection signal from the STFT data and ff frequency
    dp_fc, dp_fb, dp_amp = ricg.calcReflectionSignal(
                                                stft_pf,
                                                stft_fb,
                                                stft,
                                                ff,
                                                default_params.pf_lastbandfreq)
    dp_dg = shot.calc_fb2dg(dp_fb)

    # Interpolates the group delay data to a fixed number of points
    dpi_fc, dpi_dg = ricg.calcReflectionInterpolate(dp_fc,
                                                dp_dg,
                                                dp_amp,
                                                npoints=default_params.out_datapoints)
    dpi_fb = shot.calc_dg2fb(dpi_dg)
    shot.dpi_fc = dpi_fc
    shot.dpi_dg = dpi_dg
    shot.dpi_fb = dpi_fb

    fc = dpi_fc
    dg = dpi_dg


    # Estimates the residual density at the first fringe reflection and the
    # correct ff at fce0
    los_fce = reflectometry.calc_fce(los_b)
    #ne_ff, realdvacuum,fce0 = reflectometry.calcEstimateResidualDensityFF(fc, dg,
    #                            los_ra, los_b,
    #                            ne_ff_min=1e16, 
    #                            ne_ff_max=0.5e18,
    #                            ne_ff_tries=150,ff=40e9)
    
    
#    ne_ff, dvacuum,fce0 = reflectometry.calcEstimateResidualDensityFF(fc, dg,
#                                los_ra, los_b,
#                                ne_ff_min=1e16, 
#                                ne_ff_max=0.5e18,
#                                ne_ff_tries=150,ff=ff)

    ne_ff = 1e17
    # Removes the vacuum distance delay from the group delay data based on
    # the estimated residual density
    dpr_fc, dpr_dg, dvacuum, fce0, dgvacuum = reflectometry.removeVacuumDistance(
                                                dpi_fc,
                                                dpi_dg,
                                                los_ra,
                                                los_b,
                                                ne_ff=ne_ff, ff=ff)
    dpr_fb = shot.calc_dg2fb(dpr_dg)
    print 'fce0',fce0

    # Inverts the density profile based on the calculated group delay data
    ra, ne, br = reflectometry.calcInvertProfile(dpr_fc,
                                    dpr_dg,
                                    los_ra,
                                    los_b,
                                    dvacuum=dvacuum,
                                    #dvacuum=realdvacuum,
                                    fce0=fce0)


    # Calculate real r, z coordinates from ra
    re,thetae,ze = shot.los.calcRa2cyl(ra)
    # Calculate rhop from r, z coordinates
    rhop = shot.Eq.get_kkRZ2Rhop(shot.time,re,ze)


    ValidateData = ricg.DataValidation(verbose=True,)

    # Validation of the fce0 being above the fce at the antenna
    valid, desc = ValidateData.validateFce0AboveFceAntenna(fce0, fce_antenna)
    # Validation of the calculated group delay being non negative
    valid, desc = ValidateData.validatePlasmaPropagationDelay(dpr_fc, dpr_dg)


    # Validation of the radial monotonicity of the density profile
    valid, desc = ValidateData.validateDensiyProfileMonotonicity(ra,ne)

    # Validation of the radial propagation delay higher than in vacuum

    valid, desc = ValidateData.validateProfilePositionWithGroupDelay(
                                        dpr_fc, dpr_dg, ra-dvacuum, ne)
    
    
    # Validates all the tests run for this sweep
    dataValidated = ValidateData.validate()


    shot.ne_ff = ne_ff
    shot.freq = freqramp
    shot.signal = sig

    shot.freq = freqramp
    shot.signal = sig

    shot.bfield = bfield
    shot.fce_antenna = fce_antenna
    shot.fce_sep = fce_sep
    shot.stft_pf = stft_pf
    shot.stft_fb = stft_fb
    shot.stft = stft
    shot.stft_dg = stft_dg

    shot.ff = ff

    shot.fc = fc
    shot.dg = dg

    shot.dp_fc = dp_fc
    shot.dp_fb = dp_fb
    shot.dp_dg = dp_dg
    shot.dp_amp = dp_amp

    shot.dpi_fc = dpi_fc
    shot.dpi_dg = dpi_dg
    shot.dpi_fb = dpi_fb

    shot.dpr_fc = dpr_fc
    shot.dpr_dg = dpr_dg
    shot.dpr_fb = dpr_fb

    shot.dvacuum = dvacuum
    shot.dgvacuum = dgvacuum
    
    shot.fce0 = fce0
    shot.ra = ra
    shot.ne = ne

    shot.re = re
    shot.ze = ze

    shot.fc = fc
    shot.dg = dg

    shot.dp_fc = dp_fc
    shot.dp_fb = dp_fb
    shot.dp_dg = dp_dg
    shot.dp_amp = dp_amp
    
    shot.dpi_fc = dpi_fc
    shot.dpi_dg = dpi_dg
    shot.dpi_fb = dpi_fb

    shot.dvacuum = dvacuum
    shot.fce0 = fce0
    shot.dgvacuum = dgvacuum
    shot.ra = ra
    shot.ne = ne

    shot.re = re
    shot.ze = ze

    shot.rhop = rhop

    shot.los_ra = los_ra
    shot.los_b = los_b
    shot.los_fce = los_fce

    shot.ra_sep = np.interp(fce_sep,los_fce,los_ra)

    fforig = ff
    
        



def freq_hilbert(sig,Fs):
    """ Calcultes the instantaneous frequency based on the hilbert 
    transform
    """
    sigorig = sig
    sig = sig.real
    ts = 1.0/Fs
    hilb = signal.hilbert(sig)
    ampli = np.abs(hilb)
    
    norm = sig/ampli
    hilbnorm = signal.hilbert(norm)
    env = np.abs(hilbnorm)
    phase = np.unwrap(np.angle(hilbnorm))
    print 'phase1',phase
    #phase = np.unwrap(np.angle(sigorig))
    print 'phase2',phase
    #exit()
    phasediff = np.append([0],np.diff(phase))
    print 'phasediff',phasediff
    freq = phasediff/(2*np.pi*ts)
    #freq = freq/(2*np.pi)
    return freq


# Implemented smooth moving average algorithm
def smooth(data,N=5):
    return np.convolve(np.ones(N)/N,data,'same')


def closest_index(array,value):
    """ Finds the index of the array element closest to value
    """
    array = np.array(array)
    return (np.abs(np.array(array)-value)).argmin()





        
    

if __name__ == "__main__":
    import copy
    
    shotnumber = 33291
    antenna = 1
    time = 2.45
    #time = 3.4
    persistence = 1
    
    shot = ricg.RIF(shotnumber=shotnumber,
                    antenna=antenna,
                    time=time,
                    persistence=persistence,
                    verbose=True)
    shot.gotoTime(time)


   
    for i in range(persistence):
        print i, shot.sweepnr, shot.raw.time, shot.time
        
        
        shot.nextSweep()
        shot.processProfile()
        sweep_iteration(shot,fixedff=None)


        #shot.processRaw()
        
    
    
        

    freqramp = shot.freqramp
    sig = shot.signal
    fce = shot.fce_antenna
    fsep = shot.fce_sep
    ff = shot.ff

    Fs = shot.Fs
    ra = shot.los_ra
    b = shot.los_b
    fb2dg = shot.calc_fb2dg(1)

    pk_pf = shot.pk_pf
    pk_fb = shot.pk_fb
    pk_dg = shot.calc_fb2dg(pk_fb)

    pk_amp = shot.pk_amp

    fc = shot.fc
    dg = shot.dg

    stft_pf = shot.stft_pf
    stft_fb = shot.stft_fb
    stft = shot.stft
    stft_dg = shot.stft_dg

    los_b = shot.los_b
    los_ra = shot.los_ra
    los_fce = shot.los_fce

    ra_sep = shot.ra_sep

    ra = shot.ra
    ne = shot.ne
    
    shot1 = copy.copy(shot)
    shot2 = copy.copy(shot)
    fforig = ff


    ffspans = 0.15e9
    ffspans = 0.1e9
    ffspans = 0.2e9
    ff1 = fforig-ffspans

    sweep_iteration(shot1, fixedff=ff1)

    #shot1.processProfile()
    ra1 = shot1.ra
    ne1 = shot1.ne

    ff2 = fforig+ffspans
    sweep_iteration(shot2, fixedff=ff2)

    #shot2.processProfile()
    ra2 = shot2.ra
    ne2 = shot2.ne

    print ff,ff1,ff2

    ##### PLOTTING 


    if 1:
        ### SCALING SCALE
         freqramp = freqramp/1e9

         fce = fce/1e9
         fsep = fsep/1e9
         ff = ff/1e9

         pk_pf = pk_pf/1e9
         pk_fb = pk_fb/1e6
         pk_dg = pk_dg*1e9


         stft_pf = stft_pf/1e9
         stft_fb = stft_fb/1e6
         stft_dg = stft_dg*1e9
         stft = stft
         
         fc = fc/1e9
         dg = dg*1e9

         los_ra = los_ra*100
         los_fce = los_fce/1e9

         ra_sep = ra_sep*100

         ra = ra*100
         ne = ne/1e19
         ra1 = ra1*100
         ne1 = ne1/1e19
         ra2 = ra2*100
         ne2 = ne2/1e19

         ff1 = ff1/1e9
         ff2 = ff2/1e9

    if 1:
         import matplotlib as mpl
         mpl.rcParams['lines.linewidth'] = 1
         #mpl.rcParams['lines.color'] = 'r'
    

    raff = np.interp(ff,los_fce,los_ra)
    raff1 = np.interp(ff1,los_fce,los_ra)
    raff2 = np.interp(ff2,los_fce,los_ra)

    print 'ff',ff

    print 'neff',shot.ne_ff

    colors = ['#1f77b4','#ff7f0e','#2ca02c','#d62728','#9467bd','#8c564b','#e377c2','#7f7f7f','#bcbd22','#17becf']

    cor = colors[1]
    cor1 = colors[2]
    cor2 = colors[3]
    corfce = colors[7]
    corfce = 'k'
    corsep = colors[7]

    lw = 1
    cb = '#1f77b4'
    cr = '#d62728'
    cp = '#9467bd'
    cpi = '#e377c2'
    cg = '#2ca02c'
    co = '#ff7f0e'
    alphaval = 0.9
    alphashade = 0.1

    figdpi = 300
    fi = 0
    filenamepng = 'images/sofe2017_ff_influence_%d.png'
    filenamepdf = 'images/sofe2017_ff_influence_%d.pdf'


#    f,axarr = plt.subplots(3,1,sharex=True, figsize=(3.37,3))
    f,axarr = plt.subplots(3,1,sharex=True,figsize=(3,3))
    
    axraw = axarr[0]
    axstft = axarr[1]
    axlos = axarr[2]

    f.add_subplot(111, frameon=False)
# hide tick and tick label of the big axes
    plt.tick_params(labelcolor='none', top='off', bottom='off', left='off', right='off')

    #plt.ylabel("Beat frequency [MHz]")
    #plt.xlabel("Probing frequency [GHz]")
    
    plt.xlabel("Frequency [GHz]")
    
    #plt.suptitle('X-mode reflectometry scenarios')
    #plt.xlabel('Probing frequency [GHz]')
    plt.subplots_adjust(left=0.2, bottom=0.15, right=None, top=None, wspace=0.05, hspace=0)

    axstft.set_ylim([0,9])
    axlos.set_ylim([0,9])


    ax = axraw
    #ax.set_xlim([freqramp.min(),freqramp.max()])
    ax.set_xlim([45,65])

    ax.set_ylabel('Amplitude [V]')
    ax.plot(freqramp,sig.real)
    
    #for tk in ax.get_yticklabels():
    #    tk.set_visible(True)
    #axstft.axis('off')
    #axlos.axis('off')

    #axstft.get_yticklabels().set_visible(False)

    fi += 1;f.savefig(filenamepng%(fi),dpi=figdpi);f.savefig(filenamepdf%(fi),dpi=figdpi);print filenamepng%(fi)
    #plt.show()
    


    ax = axstft
    ax.set_ylabel(r'$\tau_g$ [ns]')
 
    ax.pcolormesh(stft_pf,stft_dg,stft,cmap='Blues',rasterized=True)

    fi += 1;f.savefig(filenamepng%(fi),dpi=figdpi);f.savefig(filenamepdf%(fi),dpi=figdpi);print filenamepng%(fi)


#    ax.plot(fc,dg,c=colors[0])

#    fi += 1;f.savefig(filenamepng%(fi),dpi=figdpi);f.savefig(filenamepdf%(fi),dpi=figdpi);print filenamepng%(fi)

 
    
    ax = axlos

    ax.set_ylabel('Radius [cm]')

    ax.plot(los_fce,los_ra)

    fi += 1;f.savefig(filenamepng%(fi),dpi=figdpi);f.savefig(filenamepdf%(fi),dpi=figdpi);print filenamepng%(fi)


    # plot fce wall
    for ax in axarr:
        ax.axvline(x=fce,linestyle='--',c=corfce,alpha=alphaval,lw=lw,label='Wall')
    fi += 1;f.savefig(filenamepng%(fi),dpi=figdpi);f.savefig(filenamepdf%(fi),dpi=figdpi);print filenamepng%(fi)


    # plot fce sep

    for ax in axarr:
        ax.axvline(x=fsep,linestyle='--',c=corsep,alpha=alphaval,lw=lw,label='Separatrix')
    fi += 1;f.savefig(filenamepng%(fi),dpi=figdpi);f.savefig(filenamepdf%(fi),dpi=figdpi);print filenamepng%(fi)


    # plot ff
    axstft.plot(fc,dg,c=colors[0])

    for ax in axarr:
        ax.axvline(x=ff,c=cor,alpha=alphaval,lw=lw)
    axlos.hlines([raff],[0],[ff],colors=cor,alpha=alphaval,lw=lw)
   
    fi += 1;f.savefig(filenamepng%(fi),dpi=figdpi);f.savefig(filenamepdf%(fi),dpi=figdpi);print filenamepng%(fi)
    # plot ff1

    for ax in axarr:
        ax.axvline(x=ff1,c=cor1,alpha=alphaval,lw=lw)
    axlos.hlines([raff1],[0],[ff1],colors=cor1,alpha=alphaval,lw=lw)
    
    fi += 1;f.savefig(filenamepng%(fi),dpi=figdpi);f.savefig(filenamepdf%(fi),dpi=figdpi);print filenamepng%(fi)
    # plot ff2

    for ax in axarr:
        ax.axvline(x=ff2,c=cor2,alpha=alphaval,lw=lw)
    axlos.hlines([raff2],[0],[ff2],colors=cor2,alpha=alphaval,lw=lw)
      
    fi += 1;f.savefig(filenamepng%(fi),dpi=figdpi);f.savefig(filenamepdf%(fi),dpi=figdpi);print filenamepng%(fi)

    #axlos.hlines([raff1],[0],[ff1],colors=cor1,alpha=alphaval,lw=lw)
    #axlos.hlines([raff2],[0],[ff2],colors=cor2,alpha=alphaval,lw=lw)
    

    #ax.set_ylabel('Inverted B profile [cm]')
    #ax.set_ylabel('Distance [cm]')
    


    #ax.legend()

    if 1:
        zoom = 2.0
        #zoom = 0.5
        axins = zoomed_inset_axes(ax, zoom, loc=9)  # zoom 
        
        axins.plot(los_fce,los_ra)
        
        axins.set_ylim([3,6])
        axins.set_ylim([3.1,5.8])
        axins.set_yticks([raff1,raff,raff2])
        axins.set_xticks([ff1,ff,ff2])
        #axins.set_xticks([ff1,ff,ff2],rotation=45)
        #axins.set_xlim([fce,fsep])
        axins.set_xlim([45.8,47])

        ha = ['right', 'center', 'left']
        ha =  'center'

        plt.setp( axins.xaxis.get_majorticklabels(), rotation=55 )
        plt.setp( axins.xaxis.get_majorticklabels(), ha=ha )
        #plt.setp( axins.xaxis.get_majorticklabels(), rotation=70 )
        axins.yaxis.set_major_formatter(FormatStrFormatter('%.2f'))
        axins.xaxis.set_major_formatter(FormatStrFormatter('%.1f'))

        axins.hlines([raff],[0],[ff],colors=cor,alpha=alphaval,lw=lw)
        axins.hlines([raff1],[0],[ff1],colors=cor1,alpha=alphaval,lw=lw)
        axins.hlines([raff2],[0],[ff2],colors=cor2,alpha=alphaval,lw=lw)
        
        #axins.vlines([ff],[0],[raff],colors=cor,alpha=alphaval,lw=lw)
        #axins.vlines([ff1],[0],[raff1],colors=cor1,alpha=alphaval,lw=lw)
        #axins.vlines([ff2],[0],[raff2],colors=cor2,alpha=alphaval,lw=lw)
        #axins.axvline(x=fce,linestyle='--',c='k',alpha=alphaval,lw=lw)
        #axins.axvline(x=fsep,linestyle='--',c='k',alpha=alphaval,lw=lw)
        axins.axvline(x=ff,c=cor,alpha=alphaval,lw=lw)  
        axins.axvline(x=ff1,c=cor1,alpha=alphaval,lw=lw)  
        axins.axvline(x=ff2,c=cor2,alpha=alphaval,lw=lw)  
    
        mark_inset(ax, axins, loc1=2, loc2=3, fc="none", ec="0.5")


    fi += 1;f.savefig(filenamepng%(fi),dpi=figdpi);f.savefig(filenamepdf%(fi),dpi=figdpi);print filenamepng%(fi)

    if 0:
      for ax in axarr:
        ax.axvline(x=fce,linestyle='--',c=corfce,alpha=alphaval,lw=lw,label='Wall')
        ax.axvline(x=fsep,linestyle='--',c=corsep,alpha=alphaval,lw=lw,label='Separatrix')
        ax.axvline(x=ff,c=cor,alpha=alphaval,lw=lw)
#        ax.axvline(x=froughstart, c='r',alpha=alphaval,lw=lw)
#        ax.axvline(x=fstart, c='DarkRed',alpha=alphaval,lw=lw)
        if 1:
            ax.axvline(x=ff1,c=cor1,alpha=alphaval,lw=lw)
            ax.axvline(x=ff2,c=cor2,alpha=alphaval,lw=lw)

    

    f,axarr = plt.subplots(1,1,figsize=(2,3))
    plt.subplots_adjust(left=0.15, bottom=0.17, right=None, top=None, wspace=0.05, hspace=0)

    ax = axarr
    ax.set_xlim([0,10])
    ax.set_xlabel('Radius [cm]')
    ax.set_ylabel(r'$n_e$ [$\times10^{19} m^{-3}$]')

    ax.plot(ra,ne,c=cor)
    fi += 1;f.savefig(filenamepng%(fi),dpi=figdpi);f.savefig(filenamepdf%(fi),dpi=figdpi);print filenamepng%(fi)
   
    ax.plot(ra1,ne1,c=cor1)
    fi += 1;f.savefig(filenamepng%(fi),dpi=figdpi);f.savefig(filenamepdf%(fi),dpi=figdpi);print filenamepng%(fi)
 
    ax.plot(ra2,ne2,c=cor2)
    fi += 1;f.savefig(filenamepng%(fi),dpi=figdpi);f.savefig(filenamepdf%(fi),dpi=figdpi);print filenamepng%(fi)

    





    f,axarr = plt.subplots(1,1,figsize=(4,3))
    plt.subplots_adjust(left=0.15, bottom=0.17, right=0.83, top=None, wspace=0.05, hspace=0)

    ax = axarr
   
    #ax.set_xlim([0,10])
    ax.set_xlabel('Radius [cm]')
    ax.set_ylabel(r'B [T]')

    
    ax.plot(los_ra,los_b)

    
    ax1 = ax.twinx()
    ax1.plot(los_ra,los_fce)
    ax1.set_ylabel(r'Cyclotronic frequency [GHz]')
    fi += 1;f.savefig(filenamepng%(fi),dpi=figdpi);f.savefig(filenamepdf%(fi),dpi=figdpi);print filenamepng%(fi)

    ax.axvline(x=0,c=corfce,linestyle='--',alpha=alphaval,lw=lw,label='Wall')
    fi += 1;f.savefig(filenamepng%(fi),dpi=figdpi);f.savefig(filenamepdf%(fi),dpi=figdpi);print filenamepng%(fi)

    ax.axvline(x=ra_sep,c=corsep,linestyle='--',alpha=alphaval,lw=lw,label='Separatrix')
   

    fi += 1;f.savefig(filenamepng%(fi),dpi=figdpi);f.savefig(filenamepdf%(fi),dpi=figdpi);print filenamepng%(fi)





    

    






    plt.show()
